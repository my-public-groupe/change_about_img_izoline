<?php

namespace App\Console\Commands;

use App\Models\Categories;
use App\Models\Manufacturers;
use App\Models\Photos;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Csv\Reader;
use League\Csv\Statement;

class ImportProducts extends Command
{
    protected $signature = 'import:products';
    protected $description = 'Import products';
    private $path = 'uploaded/';
    private $filename_ro = 'sync_all_RO.csv';
    private $filename_ru = 'sync_all_RU.csv';

    public function handle()
    {
        ini_set('max_execution_time', 3600);

        $csv_ro = Reader::createFromPath(public_path($this->path . $this->filename_ro));
        $csv_ro->setHeaderOffset(0);
        $csv_ro->setDelimiter(';');

        $csv_ru = Reader::createFromPath(public_path($this->path . $this->filename_ru));
        $csv_ru->setHeaderOffset(0);
        $csv_ru->setDelimiter(';');

        if (count($csv_ro)) {
            $csv_products_ro = $csv_ro->getRecords();

            foreach ($csv_products_ro as $offset => $csv_product_ro) {
                $csv_product_ro = (object)$csv_product_ro;
                $csv_product_ru = (object)$this->getProductRu($csv_ru, $csv_product_ro->id);

                try {
                    $product = Product::updateOrCreate(
                        ['id_1c' => $csv_product_ro->id],
                        [
                            'id_1c' => $csv_product_ro->id,
                            'sku' => $csv_product_ro->sku,
                            'name' => $csv_product_ru->name ?? '',
                            'name_ro' => $csv_product_ro->name,
                            'description_short' => $csv_product_ru->short_description ?? '',
                            'description_short_ro' => $csv_product_ro->short_description,
                            'description' => $csv_product_ru->description ?? '',
                            'description_ro' => $csv_product_ro->description,
                            'enabled' => $csv_product_ro->catalog_visibility == 'visible',
                            'price' => (float)$csv_product_ro->regular_price,
                            'sale_price' => (float)$csv_product_ro->sale_price,
                            'slug' => Str::slug($csv_product_ro->{'Ссылка'}),
                        ]
                    );

                    //Бренды
                    $brand_name = $csv_product_ro->attributes_value2;
                    if (!empty($brand_name)) {
                        $this->updateBrands($product, $brand_name);
                    }

                    //Фото
                    $photo_url = $csv_product_ro->image_id;
                    if (!empty($photo_url)) {
                         $this->updatePhotos($product, $photo_url);
                    }

                    //Категории
                    $this->updateCategories($product, $csv_product_ro, $csv_product_ru);//Характеристики
                    $this->updateCharacteristics($product, $csv_product_ro, $csv_product_ru);
                } catch (\Exception $e) {
                    Log::error('Import: ' . $e->getMessage());
                    continue;
                }
            }
        }
    }

    private function updateCategories($product, $csv_ro, $csv_ru)
    {
        $arr_ro = array_map('trim', explode(">", $csv_ro->category ?? []));
        $arr_ru = array_map('trim', explode(">", $csv_ru->category ?? []));
        if(count($arr_ro) == 0 || count($arr_ru) == 0) return;

        $parent_name_ro = $arr_ro[0];
        $parent_name_ru = $arr_ru[0];

        $slug = Str::slug(str_replace('+', '-plus', $arr_ro[0]));
        $parent_category = Categories::updateOrCreate(
            ['name_ro' => $parent_name_ro],
            [
                'name' => $parent_name_ru,
                'name_ro' => $parent_name_ro,
                'slug' => $slug,
                'enabled' => 1
            ]
        );

        if (!empty($arr_ro[1]) && !empty($arr_ru[1])) {
            $child_name_ro = $arr_ro[1];
            $child_name_ru = $arr_ru[1];

            $slug = Str::slug(str_replace('+', '-plus', $arr_ro[1]));
            $child_category = Categories::updateOrCreate(
                ['name_ro' => $child_name_ro],
                [
                    'name' => $child_name_ru,
                    'name_ro' => $child_name_ro,
                    'slug' => $slug,
                    'enabled' => 1
                ]
            );

            $child_category->parents()->sync($parent_category->id, true);

            $product->category()->sync($child_category->id, true);
        }
    }

    private function updateBrands($product, $brand_name)
    {
        $brand = Manufacturers::updateOrCreate(
            ['name' => $brand_name],
            [
                'name' => $brand_name,
                'name_ro' => $brand_name,
                'slug' => Str::slug($brand_name),
                'enabled' => 1,
                'sort' => 0
            ]
        );

        $product->manufacturer_id = $brand->id;
        $product->save();
    }

    private function updatePhotos($product, $url)
    {
        $filename = basename(parse_url($url, PHP_URL_PATH));

        $response = Http::withOptions(['verify' => false])
            ->get($url);

        if ($response->status() === 200) {
            $imageContent = $response->body();
            Storage::disk('uploaded')->put($filename, $imageContent);

            Photos::updateOrCreate(
                ['source' => $filename],
                [
                    'source' => $filename,
                    'table_id' => $product->id,
                    'table' => 'products',
                    'sort' => 0
                ]);
        }
    }

    private function updateCharacteristics($product, $csv_ro, $csv_ru)
    {
        $characteristics = [];
        $characteristics_ro = [];

        for ($i = 1; $i <= 11; $i++){
            $attr_name = 'attributes_name'.$i;
            $attr_value = 'attributes_value'.$i;
            $characteristics[$csv_ru->$attr_name] = $csv_ru->$attr_value;
            $characteristics[$csv_ro->$attr_name] = $csv_ro->$attr_value;
        }

        $product->characteristics = $characteristics;
        $product->characteristics_ro = $characteristics_ro;
        $product->save();
    }

    private function getProductRu($csv_ru, $id_1c)
    {
        $stmt = (new Statement())->where(function ($record, $offset, $iterator) use ($id_1c) {
            return $record['id'] == $id_1c;
        });

        $record_ru = $stmt->process($csv_ru);

        return $record_ru->first();
    }
}
