<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

trait CommonTrait
{
    public function getCreatedAtAttribute($date)
    {
        return Date::parse($date)->format('d.m.Y');
    }

    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Date::parse($date)->format('d.m.Y');
    }

    public function setUpdatedAtAttribute($date)
    {
        $this->attributes['updated_at'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    public function setSlugAttribute($value)
    {
        if ($value == "") {
            // grab the title and slugify it
            $this->attributes['slug'] = Str::slug($this->name);
        } else {
            $this->attributes['slug'] = Str::slug($value);
        }
    }

    public function setNameAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['name'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if (! in_array($key, array_keys(config('app.locales')))) continue;
                if ($key == config('app.base_locale')) {
                    $this->attributes['name'] = $value;
                    continue;
                }
                $this->attributes['name_' . $key] = $value;
            }
        }
    }

    public function setDescriptionAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['description'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if (! in_array($key, array_keys(config('app.locales')))) continue;
                if ($key == config('app.base_locale')) {
                    $this->attributes['description'] = $value;
                    continue;
                }
                $this->attributes['description_' . $key] = $value;
            }
        }
    }

    public function setDescriptionShortAttribute($values) {

        if (!is_array($values)) {
            $this->attributes['description_short'] = $values;
        } else {
            foreach ($values as $key => $value) {
                if (! in_array($key, array_keys(config('app.locales')))) continue;
                if ($key == config('app.base_locale')) {
                    $this->attributes['description_short'] = $value;
                    continue;
                }
                $this->attributes['description_short_' . $key] = $value;
            }
        }
    }

    public function getDescriptionAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_' . $locale])) {
            return $this->attributes['description_' . $locale];
        }
        return $this->attributes['description'];
    }

    public function getDescriptionShortAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['description_short_' . $locale])) {
            return $this->attributes['description_short_' . $locale];
        }

        return $this->attributes['description_short'];
    }

    public function getNameAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['name_' . $locale])) {
            return $this->attributes['name_' . $locale];
        }

        return  $this->attributes['name'];
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photos','table_id')->where('table', $this->getTable())->orderBy('sort');
    }

    public function scopeEnabled($query)
    {
        return $query->where('enabled', true);
    }

    public function scopeDisabled($query)
    {
        return $query->where('enabled', false);
    }

    public function mainphoto($index = 0)
    {
        $fileName = 'img/no_photo.png';

        if (isset($this->photos[$index]) && File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
            $fileName =  $this->photos[$index]->source;
            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function originalphoto($index = 0)
    {
        $fileName = 'img/no_photo.png';

        if (isset($this->photos[$index]) && File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
            $fileName =  $this->photos[$index]->source;
            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function thumbphoto($index = 0)
    {
        $fileName = "no_photo.png";
        if (isset($this->photos[$index])) {
            $fileName = $this->photos[$index]->source;
        }
        return config('photos.thumbs_url') . $fileName;
    }

    public function scopeOrderByName($query)
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale')) {
            return $query->orderBy('name_' . $locale);
        }

        return $query->orderBy('name');
    }

    public function scopeSortable($query)
    {
        $sort = session('sort', 'no_sort');

        if ($sort == 'price_asc') {
            return $query->orderBy('price');
        }

        if ($sort == 'price_desc') {
            return $query->orderBy('price', 'desc');
        }

        if ($sort == 'new_sort') {
            return $query->orderBy('new', 'desc');
        }

        return $query;
    }
}
