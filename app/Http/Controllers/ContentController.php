<?php

namespace App\Http\Controllers;

use App\Models\CategoriesMaterials;
use App\Models\Lists;

use App\Http\Requests;
use App\Models\Content;
use Illuminate\Support\Facades\Lang;

class ContentController extends Controller
{
    public function getBySlug($slug)
    {
        $data = Content::where('slug', $slug)->firstOrFail();

        return view('content')->with(compact('data'));
    }

    public function getContacts()
    {
        return view('contacts');
    }

    public function getLocales()
    {
        return Lang::get('common');
    }

    public function aboutUs()
    {
        return view('about');
    }

    public function conditions()
    {
        return view('conditions');
    }

    public function getVideos()
    {
        $videos = Lists::find(Lists::ID_VIDEOS)->children;
        $video_categories = CategoriesMaterials::enabled()->pluck('slug', 'name');

        $category_slug = null;

        return view('videos')->with(compact(
            'videos',
            'video_categories',
            'category_slug'
        ));
    }

    public function getVideo($category_slug)
    {
        $videos = Lists::find(Lists::ID_VIDEOS)
            ->children()
            ->whereHas('video_category', function ($query) use ($category_slug) {
            $query->where('slug', $category_slug);
        })->get();

        $video_category = CategoriesMaterials::firstWhere('slug', $category_slug);

        $video_categories = CategoriesMaterials::enabled()->pluck('slug', 'name');

        return view('videos')->with(compact(
            'videos',
            'video_categories',
            'video_category',
            'category_slug'
        ));
    }

    public function documentation()
    {
        $docs = Lists::find(Lists::ID_DOCS)->children;

        return view('documentation')->with(compact('docs'));
    }

    public function compare()
    {
        return view('compare');
    }
}
