<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Lists;
use App\Models\Order;
use App\Models\OrderCalendar;
use App\Models\Photos;
use App\Models\Product;
use App\Models\Region;
use App\Models\User;
use App\Models\UserNotifications;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DataTables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {

    }

    public static function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $paginator = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $paginator->withPath(Route::current()->uri());

        return $paginator;
    }

    public function UploadPhotos(Request $request, $table, $item_id)
    {
        $files_location = base_path() . "/" . Config::get('photos.images_dir');
        $photos = $request->file('photos');
        $valid_thumbs   = array();

        if (!empty($photos)) {
            foreach ($photos as $file) {
                $extension = $file->getClientOriginalExtension();
                $new_filename = uniqid() . "." . $extension;
                $orig_image = Image::make($file);
                $image = $orig_image;
                $image->resize(config('photos.width'), config('photos.height'), function ($constraint) {
                    $constraint->aspectRatio();  //сохранять пропорции
                    $constraint->upsize();       //если фото меньше, то не увеличивать
                });

                $image->save($files_location . $new_filename);

                //thumb
                $all_thumbs = Config::get('photos.thumbs');

                foreach($all_thumbs as $thumb) {
                    if (count($valid_thumbs) > 0 && !in_array($thumb['path'], $valid_thumbs)) continue;
                    $thumbPath = $files_location . $thumb['path'] . "/";
                    if (!File::exists($thumbPath)) {
                        File::makeDirectory($thumbPath);
                    }
                    $image->resize($thumb['width'], $thumb['height'], function ($constraint) {
                        $constraint->aspectRatio();  //сохранять пропорции
                        $constraint->upsize();       //если фото меньше, то не увеличивать
                    });
                    $image->save($thumbPath . $new_filename);
                }

                unset($image);

                $photo              = new Photos;
                $photo->source      = $new_filename;
                $photo->table       = $table;

                if ($item_id != 0) {
                    $photo->table_id = $item_id;
                } else {
                    $photo->token = Session::getId();
                }

                $photo->save();
                $photo_id           = $photo->id;
                $photo->sort        = $photo_id;
                $photo->save();
            }
        }
    }

    public function UpdatePhotos(Request $request, $id)
    {
        $pc = new Admin\PhotosController;
        $pc->UpdatePhotos($request, $id);
    }

    public static function datatablesCommon($query)
    {
        return Datatables::eloquent($query)
            ->editColumn('name_en', function ($item) {
                return view('admin.partials.datatable-title', compact('item'))->render();
            })
            ->addColumn('actions', function ($item) {
                return view('admin.partials.datatable-actions', compact('item'))->render();
            })
            ->rawColumns(['name_en', 'actions']);
    }

    public function getCurrencySymbol()
    {
        $currency = session()->get('currency');

        return strtolower($currency['name']);
    }

    public static function getModelFromString($modelName = '', $nameSpace = 'App')
    {
        if (empty($nameSpace) || is_null($nameSpace) || $nameSpace === "") {
            $modelNameWithNameSpace = "App" . '\\' . $modelName;
            return app($modelNameWithNameSpace);
        }

        if (is_array($nameSpace)) {
            $nameSpace = implode('\\', $nameSpace);
            $modelNameWithNameSpace = $nameSpace . '\\' . $modelName;
            return app($modelNameWithNameSpace);
        } elseif (!is_array($nameSpace)) {
            $modelNameWithNameSpace = $nameSpace . '\\' . $modelName;
            return app($modelNameWithNameSpace);
        }
    }

    public static function isMobile()
    {
        if(!empty($_SERVER['HTTP_USER_AGENT'])){
            if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){
                return true;
            }
        }

        return false;
    }

    public static function hasWebp()
    {
        return !empty($_SERVER['HTTP_ACCEPT']) && Str::contains($_SERVER['HTTP_ACCEPT'], 'webp');
    }
}
