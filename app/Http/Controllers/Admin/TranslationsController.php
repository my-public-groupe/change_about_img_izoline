<?php

namespace App\Http\Controllers\Admin;

use App\Models\Translations;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Yaml\Yaml;

class TranslationsController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit()
    {
        $locales = config('app.locales');
        foreach ($locales as $locale => $name) {
            $data['languages'][$locale] = [
                'name' => $name,
                'content' => Yaml::dump(Translations::getLocaleArrayFromFile('common', $locale)),
            ];
        }
        return view('admin.translations.edit')->with($data);
    }

    public function save()
    {
        $languages = Request::get('languages', []);
        foreach ($languages as $key => $content) {
            try {
                Translations::updateFromYaml('common', $content, $key);
            } catch (\Symfony\Component\Yaml\Exception\ParseException $e) {
                return redirect('admin/translations')->withErrors(['message' => 'Ошибка синтаксиса. YAML error']);
            }
        }

        // save for Vue translations
        foreach ($languages as $locale => $content) {
            $jsonContent = json_encode(Translations::getLocaleArrayFromFile('common', $locale));
            $filePath = public_path("locales/{$locale}.json");
            File::put($filePath, $jsonContent);
        }

        // redirect
        Session::flash('message', 'Сохранено');
        return redirect('admin/translations');
    }

}
