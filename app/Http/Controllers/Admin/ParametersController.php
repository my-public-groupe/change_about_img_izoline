<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Parameters;
use App\Models\ParametersProducts;
use App\Models\ParametersValues;
use function GuzzleHttp\Psr7\parse_response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ParametersController extends Controller
{
    public function index()
    {
        $data = Parameters::all();
        return view('admin.parameters.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.parameters.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Parameters();
        } else {
            $data = Parameters::find($id);
        }

        $data->name  = $request->name;
        $data->type  = 1;
        $data->is_filter = $request->is_filter == "1";
        $data->sort = $request->sort ?? 0;

        $data->save();

        if (isset($request->values)) {
            foreach ($request->values['id'] as $key => $value_id) {
                $ru = $request->values['ru'][$key];
                $ro = $request->values['ro'][$key];

                if ($ru == "" || $ro == "") continue;

                if ($value_id > 0) {
                    $parameter_value = ParametersValues::find($value_id);
                } else {
                    $parameter_value = new ParametersValues();
                }
                $parameter_value->value = $ru;
                $parameter_value->value_ro = $ro;
                $parameter_value->sort     = $key;

                $data->values()->save($parameter_value);
            }
        }



        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.parameters.edit', $data->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Parameters::where('id', $id)->with('values')->first();

        return view('admin.parameters.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Parameters::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    public function getCategoryParameters(Request $request)
    {
        $category_id = $request->get('category_id', 0);
        $params = Categories::find($category_id)
                    ->parameters()
                    ->with(['parameter' => function($query){
                        $query->with('values');
                    }])
                    ->get();

        $data = [];
        foreach ($params as $param) {
            $data[] = [
                'id' => $param->parameter->id,
                'name' => $param->parameter->name,
                'type' => $param->parameter->type,
                'values' => $param->parameter->values->pluck('value', 'id')->toArray(),
            ];
        }

        return response()->json(['success' => 'true', 'data' => $data]);
    }

    public function removeParameter(Request $request)
    {
        $table = $request->get('table', '');
        $parameters_id = $request->get('parameters_id', 0);
        $table_id = $request->get('table_id', 0);

        ParametersProducts::where('table', $table)
            ->where('table_id', $table_id)
            ->where('parameters_id', $parameters_id)->delete();

        return response()->json(['success' => 'true']);
    }

    public function removeParameterValue(Request $request)
    {
        $param_value_id = $request->get('param_value_id', 0);

        ParametersProducts::where('value_id', $param_value_id)->delete();

        ParametersValues::find($param_value_id)->delete();

        return response()->json(['success' => 'true']);
    }
}
