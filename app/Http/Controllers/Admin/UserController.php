<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\UserCelebration;
use App\Models\UserNotifications;
use App\Notifications\UserRegistered;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('admin.users.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.users.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'phone' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    public function edit($id)
    {
        $data['data'] = User::find($id);
        $data['order_history'] = $this->getOrderHistory($data['data']);

        return view('admin.users.edit', $data);
    }

    public function save(Request $request, $id)
    {
        if (!isset($id)) {
            $data = new User();
        }else{
            $data = User::find($id);
        }

        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->telegram = $request->telegram;
        $data->viber = $request->viber;
        $data->facebook = $request->facebook;
        $data->instagram = $request->instagram;

        $out = [];
        $other_phones = $request->other_phones;

        if(!empty($other_phones['phone'])) {
            foreach ($other_phones['phone'] as $k => $other_phone) {
                $out[] = [
                    'phone' => $other_phone,
                    'comment' => $other_phones['comment'][$k]
                ];
            }

            $data->other_phones = $out;
        }

        $out = [];
        $addresses = $request->addresses;

        if(!empty($addresses['address'])) {
            foreach ($addresses['address'] as $address) {
                $out[] = [
                    'address' => $address
                ];
            }

            $data->addresses = $out;
        }

        $data->rights = $request->rights;

        if (!empty($request->open_password)) {
            $data->password         = bcrypt($request->open_password);
            $data->open_password    = $request->open_password;
        }

        $data->masterclass_agree = (bool)$request->masterclass_agree;

        if(empty($request->email)){
            $phone = str_replace(["+373", "(", ")", " "], "", $request->phone);
            $data->email = "0$phone@noemail.com";
        }
        if(empty($request->open_password)){
            $random_password = Str::random(6);

            $data->password         = bcrypt($random_password);
            $data->open_password    = $random_password;
        }

        $data->save();

        if(!isset($id)){
            $locale = app()->getLocale();
            $data->notify(new UserRegistered($data, $request->open_password, $locale));
        }

        if(isset($id) && !empty($request->celebrations['id'])){
            foreach ($request->celebrations['id'] as $k => $c_id){
                $date = Carbon::createFromFormat('d.m.Y', $request->celebrations['date'][$k])->format('Y-m-d');

                $celebration = UserCelebration::find($c_id);
                $celebration->date = $date;
                $celebration->description = $request->celebrations['description'][$k];
                $celebration->repeat = $request->celebrations['repeat'][$k];
                $celebration->save();

                //Оповещения
                $celebration->createAdminNotifications();
            }
        }

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.users.edit', $data->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'phone' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();

    }

    public function ajaxData(Request $request)
    {
        $query = User::query();

        if ($request->filled('rights')) {
            $query->where('rights', $request->rights);
        }

        $data = self::datatablesCommon($query)
            ->editColumn('name', function ($item) {
                return view('admin.partials.datatable-title', compact('item'))->render();
            })
            ->addColumn('phones_address', function ($item) {
                $phone_1 = str_replace(["(", ")", " "], "", $item->phone);
                $phone_1_raw = $item->phone;
                $phone_2 = str_replace(["(", ")", " "], "", $item->other_phones[0]['phone'] ?? '');
                $phone_2_raw = $item->other_phones[0] ?? '';
                $address = $item->addresses[0]['address'] ?? '';

                return view('admin.partials.datatable-phones', compact(
                    'item',
                    'phone_1',
                    'phone_1_raw',
                    'phone_2',
                    'phone_2_raw',
                    'address'
                ));
            });

        $search = \request()->search['value'];
        if (!empty($search)) {
            $search_phone = preg_replace('/[^0-9]/', '', $search);

            $data = $data->filter(function ($query2) use ($search_phone, $search) {
                $query2->where(function ($query3) use ($search_phone, $search) {
                    $query3->whereRaw("id like ?", "%$search%");
                    $query3->orWhereRaw("name like ?", "%$search%");
                    $query3->orWhereRaw("email like ?", "%$search%");
                    $query3->orWhereRaw("addresses like ?", "%$search%");
                    $query3->orWhereRaw("phone like ?", "%$search%");
                    $query3->orWhereRaw("REPLACE(REPLACE(REPLACE(REPLACE(phone, ' ', ''), '(', ''), ')', ''), '+', '') like ?", "%$search_phone%");
                    $query3->orWhereRaw("other_phones like ?", "%$search%");
                    $query3->orWhereRaw("REPLACE(REPLACE(REPLACE(REPLACE(other_phones, ' ', ''), '(', ''), ')', ''), '+', '') like ?", "%$search_phone%");
                    $query3->orWhereRaw("telegram like ?", "%$search%");
                    $query3->orWhereRaw("REPLACE(REPLACE(REPLACE(REPLACE(telegram, ' ', ''), '(', ''), ')', ''), '+', '') like ?", "%$search_phone%");
                    $query3->orWhereRaw("viber like ?", "%$search%");
                    $query3->orWhereRaw("REPLACE(REPLACE(REPLACE(REPLACE(viber, ' ', ''), '(', ''), ')', ''), '+', '') like ?", "%$search_phone%");
                    $query3->orWhereRaw("facebook like ?", "%$search%");
                    $query3->orWhereRaw("instagram like ?", "%$search%");
                    $query3->orWhereRaw("DATE_FORMAT(created_at, '%d.%m.%Y') like ?", "%$search%");
                });
            });
        }

        $data = $data->make(true);

        return $data;
    }

    public function addPhone(Request $request)
    {
        $user_id = $request->user_id;
        $phone = $request->phone;
        $comment = $request->comment;

        $user = User::find($user_id);
        $other_phones = $user->getOtherPhones();
        $other_phones[] = [
            'phone' => $phone,
            'comment' => $comment
        ];

        $user->other_phones = $other_phones;
        $user->save();

        Session::flash('message', 'Телефон добавлен!');

        return redirect()->back();
    }

    public function removePhone(Request $request)
    {
        $user_id = $request->user_id;
        $phone = $request->phone;

        $user = User::find($user_id);
        $results = collect($user->getOtherPhones())->filter(function ($item) use ($phone) {
            return $item['phone'] != $phone;
        })->toArray();

        $user->other_phones = $results;

        $user->save();
    }

    public function addAddress(Request $request)
    {
        $user_id = $request->user_id;
        $address = $request->address;

        $user = User::find($user_id);
        $addresses = $user->getAddresses();
        $addresses[] = [
            'address' => $address
        ];

        $user->addresses = $addresses;
        $user->save();

        Session::flash('message', 'Адрес добавлен!');

        return redirect()->back();
    }

    public function removeAddress(Request $request)
    {
        $user_id = $request->user_id;
        $address = $request->address;

        $user = User::find($user_id);
        $results = collect($user->getAddresses())->filter(function ($item) use ($address) {
            return $item['address'] != $address;
        })->toArray();

        $user->addresses = $results;

        $user->save();
    }

    public function addNotification(Request $request)
    {
        $celebration = UserCelebration::find($request->celebration_id);

        //Оповещения
        $celebration->createAdminNotifications();

        $mes = 'Оповещения созданы! <a href="admin/users/celebrations" class="btn btn-warning btn-mini">Посмотреть оповещения</a>';
        Session::flash('message', $mes);

        return redirect()->back();
    }

    public function getCelebrationsPage(Request $request)
    {
        $months = config('custom.months');
        $years[] = Carbon::now()->year;
        $years[] = Carbon::now()->addYear()->year;

        return view('admin.users.celebrations')->with(compact('months', 'years'));
    }

    public static function datatablesCommonCelebrations($data)
    {
        return Datatables::collection($data)
            ->editColumn('description', function ($item){
                $description = $item['celebration_description'];
                return view('admin.partials.datatable-description', compact('description'));
            })
            ->editColumn('alert_dates', function ($item) {
                $alert_dates = $item['alert_dates'];
                return view('admin.partials.datatable-alert-dates', compact('alert_dates'));
            });
    }

    public function getCelebrationsAjax()
    {
        $celebrations = UserCelebration::with('user')->get();

        $data = collect();

        if(!empty($celebrations)){
            foreach ($celebrations as $celebration){
                $celebration_date_carbon = Carbon::parse($celebration->date);
                $celebration_date_timestamp = $celebration_date_carbon->timestamp;
                $celebration_date_month = $celebration_date_carbon->month;
                $celebration_date_year = $celebration_date_carbon->year;

                $user = $celebration->user;

                $data[] = [
                    'id' => $celebration->id,
                    'user_id' => $celebration->user_id,
                    'user_name' => !empty($user->name) ? $user->name : $user->email,
                    'user_email' => $user->email,
                    'user_phone' => $user->phone,
                    'celebration_date' => $celebration_date_carbon->format('d.m.Y'),
                    'celebration_date_month' => $celebration_date_month,
                    'celebration_date_year' => $celebration_date_year,
                    'celebration_date_sort' => $celebration_date_timestamp,
                    'celebration_description' => Str::limit($celebration->description, 150),
                    'alert_dates' => $user->getAlerts($celebration->id),
                    'mk' => $user->masterclass_agree
                ];
            }
        }

        if(\request()->filled('month')){
            $data = $data->where('celebration_date_month', \request()->month);
        }

        if(\request()->filled('year')){
            $data = $data->where('celebration_date_year', \request()->year);
        }

        $data = $data->toArray();

        return self::datatablesCommonCelebrations($data)
            ->make(true);
    }

    public function removeNotification(Request $request)
    {
        UserNotifications::destroy($request->id);

        return ['success' => true];
    }

    public function addEvent(Request $request)
    {
        $user_id = $request->user_id;
        $date = Carbon::createFromFormat('d.m.Y', $request->date)->format('Y-m-d');
        $description = $request->description;
        $repeat = $request->repeat;

        $user = User::find($user_id);

        $celebration = $user->celebrations()->updateOrCreate(
            ['date' => $date],
            ['date' => $date, 'description' => $description, 'repeat' => $repeat]
        );

        //Оповещения
        $celebration->createAdminNotifications();

        Session::flash('message', 'Событие добавлено!');

        return redirect()->back();
    }

    public function removeEvent(Request $request)
    {
        $celebration = UserCelebration::find($request->id);
        $celebration->delete();
        $celebration->notifs()->delete();
    }

    public function getOrderHistory($data)
    {
        try {
            $user_id = $data->id;

            $orders = Order::where('status', '!=', -1)
                ->where('user_id', $user_id)
                ->get();

            return $orders->map(function ($item) {
                return [
                    'id' => $item->id,
                    'date' => $item->created_at,
                    'sum' => number_format($item->getTotalAmount(), 2, ".", " "),
                    'currency' => 'MDL'
                ];
            })->toArray();
        } catch (\Exception $e) {
            return [];
        }
    }

    public function loginAsUser($id)
    {
        $user = User::find($id);

        Auth::login($user);

        \session(['no_order_email' => true]);

        return redirect()->route('cabinet');
    }

    public static function createNotifications($celebrations, $user_id)
    {
        $notif_events_days = config('custom.notif_events_days');
        if (!empty($notif_events_days)) {
            if (!empty($celebrations['date'])) {
                foreach ($celebrations['date'] as $k => $celebration_date){
                    for ($i = 0; $i < count($notif_events_days); $i++) {
                        $date_fmt = Carbon::createFromFormat('d.m.Y', $celebration_date)->format('Y-m-d');
                        $date_run = Carbon::createFromFormat('d.m.Y', $celebration_date)
                            ->subDays($notif_events_days[$i])
                            ->format('Y-m-d');

                        DB::table('users_notifications')->updateOrInsert(
                            [
                                'user_id' => $user_id,
                                'original_date' => $date_fmt,
                                'date_run' => $date_run
                            ],
                            [
                                'user_id' => $user_id,
                                'original_date' => $date_fmt,
                                'date_run' => $date_run,
                                'freq' => $celebrations['repeat'][$k]
                            ]);
                    }
                }
            }
        }
    }
}
