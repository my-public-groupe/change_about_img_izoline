<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CollectionController extends Controller
{
    public function index()
    {
        return view('admin.collections.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = Collection::query();

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.collections.edit', $this->needs());
    }

    public function edit($id)
    {
        $data = Collection::find($id);
        return view('admin.collections.edit', $this->needs())->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        $model = Collection::updateOrCreate(['id' => $id],
            $request->except(
                [
                    'products',
                    'photos',
                    'file_upload',
                    'meta_description',
                    'meta_keywords',
                    'title'
                ]));

        if ($request->has('products')) {
            $model->products()->sync($request->products);
        } else {
            $model->products()->detach();
        }

        $model->slug = Str::slug($request->name['ru']);
        $model->save();

        $model->saveMeta($request);

        $this->UpdatePhotos($request, $model->id);

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect()->route('admin.collections.edit', $model->id);
    }


    public function needs()
    {
        $products = Product::get()->pluck('name_en', 'id')->toArray();

        return compact('products');
    }

}
