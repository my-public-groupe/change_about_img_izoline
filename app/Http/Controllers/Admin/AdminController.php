<?php

namespace App\Http\Controllers\Admin;

use App\Models\Note;
use App\Models\Settings;
use App\Models\User;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    public function getLogin(){
        return view('admin.login');
    }

    public function postLogin(Request $request){
        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email',         // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        $this->validate($request, $rules);

        $userdata = array(
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {
            // authorization successful!

            return redirect('admin');

        } else {
            // validation not successful, send back to form
            return redirect()->back()->withErrors(trans('auth.failed'));
        }

    }

    public function clearCache()
    {
        Cache::flush();
        Artisan::call('view:clear');
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    public function loginAs($user_id)
    {
        $user = User::findOrFail($user_id);

        Auth::login($user);

        return redirect()->route('get-account');
    }

    public function settingsPage()
    {
        return view('admin.settings.index');
    }

    public function saveSettings(Request $request)
    {
        $settings = $request->settings;

        if(!empty($settings)) {
            foreach ($settings as $s => $v) {
                Settings::set($s, $v);
            }
        }

        Session::flash('message', trans('common.saved'));

        return back();
    }
}
