<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ConvertImages;
use App\Models\Categories;
use App\Models\Lists;
use App\Models\Parameters;
use App\Models\Product;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \request('id');
        $parent = Categories::find($id);

        return view('admin.categories.index')->with(compact('id', 'parent'));
    }

    public static function datatablesCommon($query)
    {
        return Datatables::eloquent($query)
            ->editColumn('name', function ($item) {
                return view('admin.partials.datatable-title', compact('item'))->render();
            })
            ->addColumn('actions', function ($item) {
                return view('admin.partials.datatable-actions', compact('item'))->render();
            })
            ->rawColumns(['name', 'actions']);
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $id = \request('id');

        if ($id > 0){
            $query = Categories::query()
                ->select('categories.*')
                ->join('categories_xref', 'categories_xref.child_id', '=', 'categories.id')
                ->where('categories_xref.parent_id', $id);
        }else{
            $query = Categories::query();
        }

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.categories.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*' => 'required',
            'slug'   => 'required|unique:categories,id,{$id}'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        $data = Categories::updateOrCreate(['id' => $id],
            $request->except(
                [
                    'file_upload',
                    'photos',
                    'parent',
                    'show_menu'
                ]));

        // checkboxes
        $checkboxes = [
            'show_menu'
        ];

        foreach ($checkboxes as $checkbox) {
            $data->setAttribute($checkbox, $request->has($checkbox));
        }

        $data->save();

        $data->saveMeta($request);

        $this->UpdatePhotos($request, $data->id);

        //categories
        if ($request->parent) {
            $data->parents()->sync($request->parent, true);
        } else {
            $data->parents()->detach();
        }

        // parameters
        if ($request->has('parameters')) {
            $data->parameters()->sync($request->parameters, true);
        }else{
            $data->parameters()->detach();
        }

        ConvertImages::dispatch($data->id, 'categories');

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect()->route('admin.categories.edit', $data->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Categories::find($id);
        $parents = $data->parents->pluck('id')->toArray();
        $category_parameters = $data->parameters()->orderBy('sort')->get();

        return view('admin.categories.edit')->with(compact('data', 'parents', 'category_parameters'))->with($this->needs($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*' => 'required',
            'slug'   => [
                'required',
                Rule::unique('categories')->ignore($id, 'id')
            ]
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Categories::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function needs($id = null)
    {
        $categories = Categories::query();
        if (isset($id)) {
            $categories->where('id', '!=', $id);
        }
        $categories = $categories->pluck('name', 'id')->toArray();

        $parameters = Parameters::orderBy('name')->pluck('name', 'id')->toArray();

        return compact('categories', 'parameters');
    }
}
