<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoriesNews;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

class NewsController extends Controller
{
    public function index()
    {
        $data = News::all()->reverse();
        return view('admin.news.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.news.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = [
            'name'          => 'required',
            'category_id'   => 'required',
            'slug'          => 'required|unique:news'
        ];

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id){
        $fields = $request->except([
            'date',
            'file_upload',
            'photos',
        ]);

        if (!isset($id)) {
            $data = News::create();
            $data->created_at = Carbon::now();
        }else{
            $data = News::find($id);
        }

        $data->update($fields);

        // checkboxes
        $checkboxes = [];

        foreach ($checkboxes as $checkbox) {
            $data->setAttribute($checkbox, $request->has($checkbox) ? true : false);
        }

        $data->save();

        $this->UpdatePhotos($request, $data->id);

        //categories
        if ($request->category_id) {
            $data->category()->sync($request->category_id, true);
        } else {
            $data->category()->detach();
        }

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect()->route('admin.news.edit', $data->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data     = News::find($id);
        return view('admin.news.edit')->with(compact('data'))->with($this->needs($data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'          => 'required',
            'category_id'   => 'required',
            'slug'          => 'required|unique:news,id,{$id}',
        ];

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        News::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function needs($data = null)
    {
        $categories = CategoriesNews::enabled()->pluck('name','id')->toArray();
        $news_categories = $data?->category->pluck('id')->toArray();

        return compact('categories', 'news_categories');
    }

}
