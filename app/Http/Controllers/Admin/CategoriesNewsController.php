<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoriesNews;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CategoriesNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories_news.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = CategoriesNews::query();
        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.categories_news.edit')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories_news'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new CategoriesNews();
            $data->created_at = Carbon::now();
        } else {
            $data = CategoriesNews::find($id);
        }

        $data->name              = $request->name;

        $data->slug = $request->slug;

        //$data->description       = $request->description;
        //$data->title             = $request->title;
        //$data->meta_keywords     = $request->meta_keywords;
        //$data->meta_description  = $request->meta_description;
        //$data->top               = $request->top;
        //$data->sort              = $request->sort;
        $data->save();

        //$this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect()->route('admin.categories_news.edit', $data->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data    = CategoriesNews::find($id);
        return view('admin.categories_news.edit')->with(compact('data'))->with($this->needs());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'slug'          => 'required|unique:categories_news,id,{$id}'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        CategoriesNews::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function needs()
    {
        $categories = CategoriesNews::pluck('name','id')->toArray();
        return compact('categories');
    }
}
