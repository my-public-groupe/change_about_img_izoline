<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ConvertImages;
use App\Models\Categories;
use App\Models\ListThickness;
use App\Models\Manufacturers;
use App\Models\Parameters;
use App\Models\ParametersProducts;
use App\Models\Product;
use App\Notifications\ProductStatusChanged;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use PhpParser\Node\Param;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::enabled()
            ->whereDoesntHave('parents')
            ->pluck('name', 'id');

        return view('admin.products.index')->with(compact('categories'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData(Request $request)
    {
        $query = Product::query()->orderBy('id', 'desc');

        if ($request->has('status')) {
            $query->where('status', $request->status);
        }

        return self::datatablesCommon($query)
            ->addColumn('mainphoto', function ($item) {
                return $item->mainphoto();
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        $products = Product::enabled()
            ->pluck('name', 'id');

        return view('admin.products.edit')
            ->with(compact('products'))
            ->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name.*' => 'required',
            'product_type' => 'required',
            'measure_type' => 'required',
            'round_by' => 'required',
            'width' => 'required',
            'length' => 'required',
            'thickness' => 'required_if:product_type,2,3',
            'price_square' => 'required_if:product_type,2',
            'slug' => [
                'required',
                Rule::unique('products')
            ]
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request);
    }

    private function save(Request $request, $id = null)
    {
        $fields = $request->except([
            'file_upload', 'photos', 'parameters',
            'values', 'value_en', 'value_id',
            'category_id',
            'question', 'answer',
            'question_ro', 'answer_ro',
            'manufacturer_id',
            'recommended'
        ]);

        // store
        if (!isset($id)) {
            $data = Product::create();
            $data->user_id = Auth::id();
        } else {
            $data = Product::find($id);
            $data->updated_at = Carbon::now();
        }

        $data->update($fields);

        // checkboxes
        $checkboxes = [
            'enabled'
        ];

        foreach ($checkboxes as $checkbox) {
            $data->setAttribute($checkbox, $request->has($checkbox));
        }

        $data->manufacturer_id = null;
        if($request->filled('manufacturer_id')){
            $data->manufacturer_id = $request->manufacturer_id;
        }

        $this->updateFaq($request, $data);

        $data->save();

        $this->UpdatePhotos($request, $data->id);

        $data->saveMeta($request);

        //list thickness
        $this->saveListThickness($request, $data);

        //categories
        if ($request->category_id) {
            $data->category()->sync($request->category_id, true);
        } else {
            if($request->filled('parent_category_id')){
                $data->category()->sync($request->parent_category_id, true);
            }
        }

        $data->recommended()->detach();
        if($request->filled('recommended')){
            $data->recommended()->sync($request->recommended);
        }

        // parameters
        if ($request->has('parameters') && $request->has('value_id')) {
            $data->parameters()->detach();

            foreach ($request->parameters as $parameter_id) {
                if (!isset($request->value_id[$parameter_id])) continue;

                $values = $request->value_id[$parameter_id];

                foreach ($values as $v) {
                    ParametersProducts::updateOrCreate(
                        [
                            'parameter_id' => $parameter_id,
                            'product_id' => $data->id,
                            'value_id' => $v,
                        ],
                        [
                            'parameter_id' => $parameter_id,
                            'product_id' => $data->id,
                            'value_id' => $v,
                        ]
                    );
                }
            }
        }

        ConvertImages::dispatch($data->id);

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect(route('admin.products.edit', $data->id));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Product::find($id);
        $category = $data->category;

        $products = Product::enabled()
            ->where('id', '!=', $id)
            ->pluck('name', 'id');

        if(\request()->filled('copy')){
            $data->slug .= ' copy';
        }

        return view('admin.products.edit')->with(compact(
            'data',
            'products',
            'category'))->with($this->needs($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name.*' => 'required',
            'product_type' => 'required',
            'measure_type' => 'required',
            'round_by' => 'required',
            'width' => 'required',
            'length' => 'required',
            'thickness' => 'required_if:product_type,2,3',
            'price_square' => 'required_if:product_type,2',
            'slug' => [
                'required',
                Rule::unique('products')->ignore($id)
            ]
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        return $this->save($request, $id);
    }

    private function needs($id = null)
    {
        $sel_parameters = [];
        $parameters = collect();

        if(!isset($id)) {
            $category_id = \request()->category_id;
            $category = Categories::find($category_id);
            $categories = $category->children->pluck('name', 'id')->toArray();
        }else{
            $product = Product::find($id);

            if(isset($product->category[0]->parents[0])) {
                $category_id = $product->category[0]->parents[0]->id;

                $category = Categories::find($category_id);
                $categories = $category->children->pluck('name', 'id')->toArray();
            }else{
                $categories = [];
            }

            $parameters = $product->category[0]->parameters;

            $product_parameters = $product->parameters;
            if ($product_parameters->isNotEmpty()) {
                foreach ($product_parameters as $pp) {
                    $sel_parameters[$pp->id][] = $pp->pivot->value_id;
                }
            }
        }

        $brands = Manufacturers::enabled()
            ->pluck('name', 'id')
            ->toArray();

        return compact(
            'categories',
            'parameters',
            'sel_parameters',
            'brands');
    }

    private function updateFaq($request, $product)
    {
        $question = $request->question;
        $answer = $request->answer;
        $question_ro = $request->question_ro;
        $answer_ro = $request->answer_ro;
        $faq = [];
        $faq_ro = [];

        if(empty($question) || empty($answer)){
            $product->faq = [];
        }
        if(empty($question_ro) || empty($answer_ro)){
            $product->faq_ro = [];
        }

        if(!empty($question) && !empty($answer)){
            foreach ($question as $i => $value){
                $faq[] = [
                    'question' => $value,
                    'answer' => $this->replaceNewLines($answer[$i]) ?? ''
                ];
            }

            foreach ($question_ro as $k => $value_ro){
                if(empty($answer_ro[$k])) $answer_ro[$k] = '';

                $faq_ro[] = [
                    'question' => $value_ro,
                    'answer' => $this->replaceNewLines($answer_ro[$k])
                ];
            }

            $product->faq = $faq;
            $product->faq_ro = $faq_ro;
        }
    }

    private function saveListThickness($request, $data)
    {
        try {
            $data->list_thickness()->detach();

            if (isset($request->list_thickness['thickness'])) {
                foreach ($request->list_thickness['thickness'] as $k => $thickness) {

                    if($data->product_type == Product::TYPE_LIST_XPS){
                        $price = ListThickness::calcPrice($request->list_thickness['price_cubic'][$k], $request->list_thickness['surface'][$k]);
                    }else{
                        $price = ListThickness::calcPrice($data->price_cubic, $request->list_thickness['surface'][$k]);
                    }

                    $data->list_thickness()->syncWithoutDetaching([
                        [
                            'thickness' => $thickness ?? 0,
                            'surface' => $request->list_thickness['surface'][$k] ?? null,
                            'weight' => $request->list_thickness['weight'][$k] ?? null,
                            'num_in_box' => $request->list_thickness['num_in_box'][$k] ?? null,
                            'price' => $price,
                            'price_cubic' => $request->list_thickness['price_cubic'][$k] ?? null
                        ]
                    ]);
                }
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    private function replaceNewLines($text)
    {
        return str_replace("\n", "<br>", $text);
    }
}
