<?php

namespace App\Http\Controllers;

use App\Models\CategoriesNews;
use App\Models\News;
use App\Http\Requests;


class NewsController extends Controller
{
    public function getNews($category, $slug)
    {
        $data = News::enabled()->whereHas('category', function ($query) use ($category) {
            $query->where('slug', $category);
        })->where('slug', $slug)->firstOrFail();

        $data->increment('views');

        $other_news = News::enabled()
            ->where('id', '!=', $data->id)
            ->oldest('sort')
            ->latest('created_at')
            ->get()
            ->map(function ($item) {
                return $item->getNewsInfo();
            });

        $data = $data->getNewsInfo();

        return view('blog.post')->with(compact('data', 'other_news'));
    }

    public function getNewsList()
    {
        $page = \request()->get('page', 1);

        $news = News::enabled()
            ->with('photos')
            ->oldest('sort')
            ->latest('created_at')
            ->get()
            ->map(function ($item) {
                return $item->getNewsInfo();
            });

        $news = self::paginate($news, 6, $page);

        $news_categories = CategoriesNews::enabled()->get();

        $popular_news = News::enabled()
            ->oldest('views')
            ->limit(5)
            ->get();

        return view('blog.list')->with(compact('news', 'news_categories', 'popular_news'));
    }

    public function getCategoryNewsList($cur_category)
    {
        $page = \request()->get('page', 1);

        $news = News::enabled()->whereHas('category', function ($query) use ($cur_category) {
            $query->where('slug', $cur_category);
        })->with('photos')->oldest('sort')->latest('created_at')->get()
            ->map(function ($item) {
                return $item->getNewsInfo();
            });

        $news = self::paginate($news, 6, $page)
            ->withPath(route('get-category-posts', $cur_category));

        $news_categories = CategoriesNews::enabled()->get();

        $popular_news = News::enabled()
            ->oldest('views')
            ->get();

        $category = CategoriesNews::where('slug', $cur_category)->first();

        return view('blog.list')->with(compact('news', 'news_categories', 'popular_news', 'category'));
    }
}
