<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Settings;
use App\Notifications\CheckoutManager;
use App\Notifications\OrderCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class CartController extends Controller
{
    public function getCart()
    {
        $recommended = CatalogController::getRecommended();

        return view('cart.index', compact(
            'recommended'
        ));
    }

    public function getCartDelivery()
    {
        return view('cart.delivery');
    }

    public function getCartPayment()
    {
        return view('cart.payment');
    }

    public function getCartSuccess()
    {
        $order = Order::where('id', session('order')['id'])
            ->firstOrFail();

        return view('cart.success', compact('order'));
    }

    public function getCartSuccessManager()
    {
        return view('cart.success-manager');
    }

    public function getCartProducts()
    {
        $products = Cart::getProductsList();

        return response()->json($products);
    }

    public function postCart(Request $request)
    {
        $cart = [];
        foreach ($request->get('product_ids') as $key => $pid) {
            $cart[$pid] = $request->count[$key];
        }

        // saving to session changed cart
        session(['cart' => $cart]);

        return redirect(route('get-checkout'));
    }

    public function getPayment()
    {
        return view('cart.payment');
    }

    public function addToCart(Request $request)
    {
        $product_id = $request->id;
        $quantity = $request->q;
        $thickness = $request->thickness ?? null;

        Cart::addToCart($product_id, $quantity, $thickness);

        return response()->json([
            'products' => Cart::getProductsList(),
            'quantity' => Cart::getAllQuantity(),
            'amount' => Cart::getAllAmount()
        ]);
    }

    public function changeCartQuantity(Request $request)
    {
        $product_id = $request->id;
        $quantity = $request->q;
        $thickness = $request->thickness;

        if ($quantity == 0) {
            Cart::removeProduct($product_id, $thickness);
        } else {
            Cart::changeQuantity($product_id, $quantity, $thickness);
        }

        return $this->getCartQuantity();
    }

    public function getCartQuantity()
    {
        $quantity = Cart::getAllQuantity();

        return response()->json([
            'amount' => Cart::getAllAmount(),
            'quantity' => $quantity,
            'weight' => Cart::getAllWeight(),
            'surface' => Cart::getAllSurface()
        ]);
    }

    public function getCartInfo()
    {
        return response()->json([
            'products' => Cart::getProductsList(),
            'quantity' => Cart::getAllQuantity(),
            'amount' => Cart::getAllAmount(),
            'weight' => Cart::getAllWeight(),
            'surface' => Cart::getAllSurface()
        ]);
    }

    public function postCheckout(Request $request)
    {
        $products = Cart::getProductsList();

        $fields = $request->all();

        // adding order
        $order = new Order();

        $order_data = [
            'products' => $products,
            'ip' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'orderInfo' => $fields,
            'amount' => Cart::getAllAmount(),
            'lang' => app()->getLocale(),
            'currency' => session('currency')
        ];

        $order->data = $order_data;
        $order->user_id = auth()->check() ? auth()->id() : null;
        $order->status = Order::STATUS_NEW;
        $order->save();

        $order_info = [
            'id' => $order->id,
            'subtotal' => $order->getSubTotalAmount(),
            'amount' => $order->getTotalAmount()
        ];

        session(['order' => $order_info]);

        Cart::clear();

        //письмо админу
        Notification::route('mail', config('custom.admin_email'))
            ->notify(new OrderCreated($order));

        //письмо пользователю
        Notification::route('mail', $fields['email'])
            ->notify(new OrderCreated($order));

        return redirect()->route('cart.success');
    }

    public function postCheckoutManager(Request $request)
    {
        $form['name'] = $request->name;
        $form['phone'] = $request->phone;
        $cart['cart_products'] = Cart::getProductsList();
        $cart['cart_amount'] = Cart::getTotalAmount();

        Cart::clear();

        //письмо менеджеру
        $manager_email = Settings::get('manager_email');

        if(!empty($manager_email)) {
            Notification::route('mail', $manager_email)
                ->notify(new CheckoutManager($form, $cart));
        }

        return redirect()->route('cart.success-manager');
    }

    public function checkoutPage()
    {
        $products = Cart::getProductsList();
        $amount = Cart::getAllAmount();

        return view('cart.checkout')->with(compact(
            'products',
            'amount'
        ));
    }

    public function getCheckoutSuccess()
    {
        $order = Order::where('user_uid', auth()->id())
            ->where('order_tag', request('order'))
            ->latest()
            ->firstOrFail();

        return view('cart.success', compact('order'));
    }
}
