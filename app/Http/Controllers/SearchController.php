<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    public function getSearch(Request $request)
    {
        $searchword = $request->searchword;
        $category_id = $request->category_id;

        $productsQuery = Product::query();

        if (!empty($category_id)) {
            $productsQuery->whereHas('category', function ($query) use ($category_id) {
                $query->whereHas('parents', function ($query2) use ($category_id) {
                    $query2->where('categories_xref.parent_id', $category_id);
                });
            });
        }

        $productsQuery = $productsQuery->searchByKeyword($searchword);

        $totalProducts = $productsQuery->count();

        $products = $productsQuery->enabled()->paginate(20);

        Session::now('searchword', $searchword);
        Session::now('category_id', $category_id);

        return view('search-result',
            compact(
                'category_id',
                'totalProducts',
                'products'
            )
        );
    }
}
