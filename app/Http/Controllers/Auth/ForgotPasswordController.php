<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\MailNotification;
use App\Models\PasswordResets;
use App\Models\User;
use App\Notifications\NewPasswordRequest;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return !Auth::check() ? view('auth.passwords.email') : redirect()->route('index');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $reset_password_email = $request->email;

        //смотрим если есть такой e-mail
        $user = User::firstWhere('email', $reset_password_email);

        if($user){
            $token = Password::createToken($user);

            $locale = app()->getLocale();
            $user->notify(new NewPasswordRequest($token, $locale));

            return ['success' => true, 'data' => [trans('common.recover_password_text_modal')]];
        }else{
            return ['success' => false, 'data' => [trans('common.email_user_not_found')]];
        }
    }
}
