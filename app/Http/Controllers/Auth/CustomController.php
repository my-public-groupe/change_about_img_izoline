<?php

namespace App\Http\Controllers\Auth;

use App\Models\Design;
use App\Models\Dessert;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class CustomController extends Controller
{
    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $old_password = $request->old_password;

        if(!Hash::check($old_password, $user->password)){
            return ['success' => false, 'data' => [trans('common.wrong_password')]];
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return ['success' => true];
    }

    public function emailConfirm($token)
    {
        $user = User::firstWhere('token', $token);
        if($user){
            $user->email = $user->new_email;

            $user->new_email = '';
            $user->token = '';

            $user->save();
        }else{
            abort(404);
        }

        return redirect()->route('cabinet');
    }

    public function importUsers()
    {
        $spam = [
            "@abbuzz",
            "@refabl",
            "@kinomaxru",
            "@molodejka",
            "@lmail",
            "@regmail",
            "@got",
            "@join",
            "@see",
            "@tree",
            "@her",
            "www",
            "test",
            "meet",
            "facebook.com"
        ];

        $wp_users = DB::table('wp_users')
            ->get();

        foreach ($wp_users as $wp_user) {
            $user_login = $wp_user->user_login;
            $user_nicename = $wp_user->user_nicename;
            $display_name = $wp_user->display_name;
            $user_email = $wp_user->user_email;
            $user_registered = $wp_user->user_registered;

            if (Str::contains($user_login, $spam) ||
                Str::contains($user_nicename, $spam) ||
                Str::contains($user_email, $spam)) {
                continue;
            }

            $password = Str::random(6);

            $user = User::firstWhere('email', $user_email);

            if(!$user) {
                User::create([
                    'name' => $display_name,
                    'email' => $user_email,
                    'password' => Hash::make($password),
                    'open_password' => $password,
                    'created_at' => $user_registered,
                    'wordpress' => true
                ]);
            }
        }

        $orders_csv = public_path('uploaded/orders.csv');
        $orders = $this->readCSV($orders_csv, array('delimiter' => ','));
        foreach ($orders as $k => $order){
            if($k == 0) continue;

            try {
                $phone = $order[24];
            } catch (\Exception $e) {
                continue;
            }

            $email = $order[25];
            $name = $order[26];

            $password = Str::random(6);

            $user = User::firstWhere('email', $email);

            if(!$user) {
                if(empty($email)){
                    $email = $phone . '@noemail.com';
                }

                try {
                    User::create([
                        'name' => $name,
                        'email' => $email,
                        'phone' => $phone,
                        'password' => Hash::make($password),
                        'open_password' => $password,
                        'created_at' => Carbon::createFromDate(2021),
                        'wordpress' => true
                    ]);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }

        $other_phones = [
            '067509778',
            '069419583',
            '079524650',
            '068573717',
            '068161726',
            '068530077',
            '079991214',
            '079999828',
            '078131405',
            '069220721',
            '069001250',
            '069890000',
            '060388969',
            '068444488',
            '068278816'
        ];

        foreach ($other_phones as $other_phone){
            $user = User::firstWhere('phone', $other_phone);
            if(!$user){
                $password = Str::random(6);

                User::create([
                    'email' => $other_phone . '@noemail.com',
                    'phone' => $other_phone,
                    'password' => Hash::make($password),
                    'open_password' => $password,
                    'created_at' => Carbon::createFromDate(2021),
                    'wordpress' => true
                ]);
            }
        }

        $other_emails = [
            'lesya_smotr@yahoo.com'
        ];

        foreach ($other_emails as $other_email){
            $user = User::firstWhere('email', $other_email);

            if(!$user){
                $password = Str::random(6);

                User::create([
                    'email' => $other_email,
                    'password' => Hash::make($password),
                    'open_password' => $password,
                    'created_at' => Carbon::createFromDate(2021),
                    'wordpress' => true
                ]);
            }
        }
    }

    function importOrders()
    {
        $orders_csv = public_path('uploaded/orders.csv');
        $orders = $this->readCSV($orders_csv, array('delimiter' => ','));

        $fields = $orders[0];

        foreach ($orders as $k => $order_csv){
            if($k == 0) continue;
            if(empty($order_csv[9])) continue;

            $order_id = $order_csv[0];

            try {
                $created_at = Carbon::createFromFormat('d.m.Y H:i', $order_csv[3] . ' ' . $order_csv[4]);
            } catch (\Exception $e) {
                $created_at = Carbon::createFromDate(2021);
            }

            $phone = $order_csv[24] ?? '';

            $order_total = $order_csv[9];
            $email = $order_csv[25] ?? '';

            foreach ($fields as $j => $field){
                $data['orderInfo'][$field] = $order_csv[$j] ?? '';
            }

            $data['amount'] = $order_total;

            $order = Order::firstWhere('id', $order_id);

            if(!$order) {
                $order = Order::create([
                    'id' => $order_id,
                    'data' => $data,
                    'status' => 5,
                    'created_at' => $created_at,
                    'wordpress' => true
                ]);
            }

            if(!empty($phone) || !empty($email)) {
                $user = User::where('phone', $phone)->orWhere('email', $email)->first();
                if ($user) {
                    $order->user_id = $user->id;
                    $order->save();
                }
            }
        }

        $other_orders = [
            [
                'Name' => 'Эд',
                'Phone' => '067509778',
                'Date' => '05.08.2021',
                'Comment' => 'Дочке 6 лет'
            ],
            [
                'Name' => 'Nastea Lozan',
                'Phone' => '069419583',
                'Address' => 'Ion Neculce 27',
                'Date' => '05.08.2021',
                'Comment' => 'Дочке 4 года, Mira'
            ],
            [
                'Name' => 'Леся Циховская',
                'Email' => 'lesya_smotr@yahoo.com',
                'Address' => 'Друмул скиноасей 1/4',
                'Date' => '08.08.2021',
                'Comment' => 'Дочке 4 года, Mira'
            ],
            [
                'Name' => 'Елена Мокруха',
                'Phone' => '079524650',
                'Address' => 'sos Muncesti 176, ap 90',
                'Comment' => '5 лет в 22-м году будет, Alisa'
            ],
            [
                'Phone' => '068573717',
                'Address' => 'Columna 104',
                'Comment' => 'turte dulci'
            ],
            [
                'Phone' => '068161726',
                'Address' => 'voluntarilor',
                'Date' => '10.09.2021',
                'Comment' => 'Turta dulce'
            ],
            [
                'Name' => 'Daniel',
                'Phone' => '068530077',
                'Comment' => 'Avion',
                'Date' => '14.09.2021',
            ],
            [
                'Phone' => '079991214',
                'Address' => 'Iulia 5/2',
                'Comment' => '1165 lei пряники',
                'Date' => '05.10.2021',
            ],
            [
                'Phone' => '079999828',
                'Address' => 'Armeneasca 44/2',
                'Comment' => '360 lei пряники',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '078131405',
                'Address' => 'Maria Cebotari 37',
                'Comment' => '600 turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '069220721',
                'Address' => 'Alba Iulia 89',
                'Comment' => 'turte dulci haloween',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '069001250',
                'Address' => 'Traian 23/1, 5 scara',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '069890000',
                'Address' => 'Bodoni 27',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '060388969',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Phone' => '068444488',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Name' => 'Ana Popova',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
            [
                'Name' => 'Tatiana',
                'Phone' => '068278816',
                'Comment' => 'turte dulci',
                'Date' => '29.10.2021',
            ],
        ];

        foreach ($other_orders as $other_order){
            $date = $other_order['Date'] ?? '29.07.2021';

            $created_at = Carbon::createFromFormat('d.m.Y', $date);

            $order = Order::create([
                'data' => ['orderInfo' => $other_order],
                'status' => 5,
                'created_at' => $created_at,
                'wordpress' => true
            ]);

            if(isset($other_order['Phone'])){
                $user = User::firstWhere('phone', $other_order['Phone']);
                if($user){
                    $order->user_id = $user->id;
                    $order->save();
                }
            }
        }
    }

    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');

        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);

        return $line_of_text;
    }
}
