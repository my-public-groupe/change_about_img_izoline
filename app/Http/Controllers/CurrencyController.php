<?php

namespace App\Http\Controllers;

class CurrencyController extends Controller{
    public function changeCurrency($currency_name)
    {
        $currencies = config('custom.currencies');

        $index = array_search($currency_name, array_column($currencies, 'name'));

        if($index !== false){
           session()->put('currency', $currencies[$index]);
        }

        return redirect()->to(url()->previous());
    }
}
