<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Parameters;
use App\Models\Product;

class ProductsController extends Controller
{
    public function getProduct($product_slug)
    {
        $product = Product::enabled()
            ->where('slug', $product_slug)
            ->firstOrFail();

        $product->currency_price = $product->getCurrencyPrice();
        $product->currency_price_valute = $product->getCurrencyPriceValute();
        $product->currency_type_price = $product->getCurrencyTypePrice();

        $product->param_num_in_box = $product->getParamNumInBox();

        $thickness_list = $product->getThicknessList();

        // добавляем просмотров
        $product->increment('views');

        $child_category = $product->category[0];

        $parent_category = collect();
        $categories = collect();
        if(isset($child_category->parents[0])) {
            $parent_category = $child_category->parents[0];
            $categories = $parent_category->children;
        }

        $related = $product->getRelated();

        return view('catalog.product')->with(compact(
            'parent_category',
                'child_category',
                'categories',
                'product',
                'thickness_list',
                'related'
            )
        );
    }
}
