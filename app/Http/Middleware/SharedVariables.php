<?php

namespace App\Http\Middleware;

use App\Models\Categories;
use App\Models\Collection;
use App\Models\Content;
use Closure;
use Illuminate\Http\Request;

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currency_index = 0;

        if (!session()->has('currency')) {
            session()->put('currency', config('custom.currencies')[$currency_index]);
        }

        $this->shareLocaleTimestamp();

        $site_categories = Categories::enabled()
            ->whereDoesntHave('parents')
            ->get();

        $footer_categories = Categories::enabled()
            ->get();

        $footer_content = Content::enabled()
            ->get();

        view()->share('site_categories', $site_categories);
        view()->share('footer_categories', $footer_categories);
        view()->share('footer_content', $footer_content);

        return $next($request);
    }

    public function shareLocaleTimestamp()
    {
        $locale = app()->getLocale();
        $locale_path = public_path("locales/$locale.json");
        $locale_timestamp = filemtime($locale_path);

        view()->share('locale_timestamp', $locale_timestamp);
    }
}
