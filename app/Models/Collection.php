<?php

namespace App\Models;

use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use CommonTrait, MetaTrait;

    protected $guarded = [];

    const TOP_COLLECTION = 1;
    const BEST_SELLER = 4;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_collection', 'collection_id', 'product_id')
            ->oldest('sort');
    }
}
