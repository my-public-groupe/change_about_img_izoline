<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    const RIGHTS_USER = 0;
    const RIGHTS_ADMIN = 1;
    const RIGHTS_PARTNER = 2;

    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    protected $casts = [
        'params' => 'collection',
        'other_phones' => 'collection',
        'addresses' => 'collection'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function celebrations()
    {
        return $this->hasMany('App\Models\UserCelebration', 'user_id');
    }

    public function getCelebrationsFormatted()
    {
        $celebration_fmt = collect();

        if($this->celebrations->isNotEmpty()){
            $celebration_fmt = $this->celebrations->map(function ($celebration){
                $celebration->date = Carbon::parse($celebration->date)->format('d.m.Y');
                return $celebration;
            });
        }

        return $celebration_fmt;
    }

    public function getPhoneAttribute()
    {
        return $this->attributes['phone'] ?? '';
    }

    public function getOtherPhones()
    {
        return $this->other_phones;
    }

    public function getAddresses()
    {
        return $this->addresses;
    }

    public function getAlerts($celebration_id)
    {
        return UserNotifications::where('celebration_id', $celebration_id)
            ->get()
            ->toArray();
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    public function getFullName()
    {
        $lastname = $this->lastname ?? '';

        return "{$this->name} {$lastname}";
    }

    public function getShortname()
    {
        $lastname = $this->lastname[0] ?? '';

        return "{$this->name} {$lastname}.";
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id')->latest();
    }

    public function isUser()
    {
        // this looks if is user in rights column in your users table
        if ($this->rights == self::RIGHTS_USER) return true;
        return false;
    }

    public function isPartner()
    {
        // this looks if is manager in rights column in your users table
        if ($this->rights == self::RIGHTS_PARTNER) return true;
        return false;
    }

    public function isAdmin()
    {
        // this looks if is admin in rights column in your users table
        if ($this->rights == self::RIGHTS_ADMIN) return true;
        return false;
    }

    public function isWordPress()
    {
        return $this->wordpress == 1;
    }

    public function addAdditionalPhone($phone)
    {
        $other_phones = $this->other_phones;
        $other_phones[] = ['phone' => $phone, 'comment' => ''];
        $this->other_phones = $other_phones;

        $this->save();
    }

    public function addAdditionalAddress($address)
    {
        $other_addresses = $this->addresses;
        $other_addresses[] = ['address' => $address];
        $this->addresses = $other_addresses;

        $this->save();
    }

    public function getTelegramLink()
    {
        $tg = ltrim($this->telegram, "0");
        $tg = str_replace(['+373', '(', ')', '@', ' '], '', $tg);

        if(ctype_digit($tg)){
            return "https://t.me/+373$tg";
        }

        if(ctype_alnum($tg)){
            return "https://t.me/$tg";
        }

        if(Str::contains($this->telegram, 'http') || Str::contains($this->telegram, 't.me')){
            return $this->telegram;
        }

        return "https://t.me/+373$tg";
    }

    public static function isCartAdmin()
    {
        return request()->has('no_email') || request()->has('no-email') || !empty(session('no_order_email'));
    }

    public function getUserInfoString()
    {
        $out[] = "<b>Имя</b>: $this->name";
        $out[] = "<b>Email</b>: $this->email";
        $out[] = "<b>Телефон</b>: $this->phone";

        if(!empty($this->getAddresses())){
            foreach ($this->getAddresses() as $address){
                $out[] = "<b>Адрес</b>: {$address['address']}";
            }
        }

        if(!empty($this->telegram)){
            $out[] = "<b>Telegram</b>: $this->telegram";
        }
        if(!empty($this->viber)){
            $out[] = "<b>Viber</b>: $this->viber";
        }
        if(!empty($this->facebook)){
            $out[] = "<b>Facebook</b>: $this->facebook";
        }
        if(!empty($this->instagram)){
            $out[] = "<b>Instagram</b>: $this->instagram";
        }

        return implode('<br>', $out);
    }
}
