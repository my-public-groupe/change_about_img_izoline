<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;
use App\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class Categories extends Model
{
    use CommonTrait, MetaTrait;

    protected $with = ['parents', 'photos'];

    protected $guarded = ['id'];

    public function children()
    {
	    return $this->belongsToMany(
	        'App\Models\Categories', 'categories_xref', 'parent_id', 'child_id')->orderBy('sort');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'products_categories');
    }

    public function parents()
    {
	    return $this->belongsToMany('App\Models\Categories', 'categories_xref',  'child_id', 'parent_id');
    }

    public function parameters()
    {
        return $this->belongsToMany('App\Models\Parameters', 'parameters_categories', 'category_id', 'parameter_id')->orderBy('sort');
    }

    public function mainphoto($index = 0)
    {
        $fileName = 'no_photo.png';

        if (isset($this->photos[$index])) {

            if(File::exists(public_path('uploaded/' . $this->photos[$index]->source))) {
                $fileName = $this->photos[$index]->source;
            }

            if(Controller::hasWebp()){
                if(File::exists(public_path('uploaded/' . File::name($fileName) . '.webp'))){
                    $fileName = File::name($fileName) . '.webp';
                }
            }

            return config('photos.images_url') . $fileName;
        }

        return $fileName;
    }

    public function getFullPathAttribute()
    {
        $path_arr = [];
        if (isset($this->parents{0})) {
            $path_arr[] = $this->parents{0}->name;
            $path_arr[] = $this->name;
        } else {
            $path_arr[] = [$this->name];
        }

        return $path_arr;
    }

    public function setSlugAttribute($values)
    {
        if (!is_array($values)) {
            $this->attributes['slug'] = Str::slug($values);
        } else {
            foreach ($values as $key => $value) {
                if ($key == "ru") {
                    $this->attributes['slug'] = Str::slug($value);
                    continue;
                }
                $this->attributes['slug_' . $key] = Str::slug($value) ?? '';
            }
        }
    }

    public function scopeMenu($query)
    {
        return $query->where('show_menu', true);
    }

    public function hasPhotos()
    {
        return count($this->photos) > 0;
    }

    public function getActive()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug
        ];
    }
}
