<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const PAYMENT_TYPE_CASH = 0;
    const PAYMENT_TYPE_CARD = 1;

    const STATUS_DELETED = -2;
    const STATUS_CANCELED = -1;
    const STATUS_NEW = 0;
    const STATUS_DELIVERING = 1;
    const STATUS_DELIVERED = 2;

    protected $casts = [
        'data' => 'collection',
    ];

    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function products()
    {
        //return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    public function getName()
    {
        return $this->id;
    }

    public function getCurrencyText()
    {
        return trans('common.lei');
    }

    public function getCartItems()
    {
        return [];
    }

    public function getSubTotalAmount()
    {
        return $this->data['amount'];
    }

    public function getTotalAmount()
    {
        return $this->data['amount'];
    }

    public function getDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y');
    }

    public function getTotalQuantity()
    {
        return $this->products()->sum('quantity');
    }

    public function getProductIds()
    {
        return [];
    }

    public function getAmountTextAttribute()
    {
        return number_format($this->amount, 2) . ' ' . $this->getCurrencyText();
    }

    public function getStatusTextAttribute()
    {
        return trans('common.order_status_' . $this->status);
    }

    public function isPaymentCash()
    {
        return $this->data['orderInfo']['payment'] == 'cash';
    }
}
