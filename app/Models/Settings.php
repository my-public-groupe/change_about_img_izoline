<?php

namespace App\Models;

class Settings extends BaseModel
{
    protected $guarded = [];

    public $timestamps = false;

    protected $primaryKey = 'name';

    /**
     * Return setting value or default value by key.
     *
     * @param string $key
     * @param null $default_value
     * @return string|null
     */
    public static function get($key, $default_value = null)
    {
        if (Settings::hasByKey($key)) {
            $setting = Settings::getByKey($key);
        } else {
            $setting = $default_value;
        }

        if (is_null($setting)) {
            $setting = $default_value;
        }

        return $setting;
    }

    protected static function getByKey($key)
    {
        if (cache()->has($key)) {
            $setting = cache($key);
        } else {
            $setting = Settings::where('name', $key)->first();
            if (!is_null($setting)) {
                $setting = $setting->value;
            }
            cache([$key => $setting]);
        }

        return $setting;
    }

    protected static function hasByKey($key)
    {
        if (cache()->has($key)) {
            $setting = cache($key);
        } else {
            $setting = Settings::where('name', $key)->first();
        }
        return ($setting === null) ? false : true;
    }

    /**
     * Set the setting by key and value.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return void
     */
    public static function set($key, $value)
    {
        Settings::updateOrCreate(['name' => $key], ['value' => $value]);

        if (cache()->has($key)) {
            cache()->forget($key);
        }
    }

}
