<?php

namespace App\Models;


use Illuminate\Support\Facades\Lang;

class Parameters extends BaseModel
{
    const PARAM_SIZES = 1;
    public $timestamps = false;

    protected $casts = [
        'params' => 'collection',
    ];

    protected $guarded = [];

    public function values()
    {
        return $this->hasMany('App\Models\ParametersValues')->orderBy('sort');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'parameters_products', 'parameter_id', 'product_id');
    }

    public function getSelectedValue()
    {
        $locale = Lang::locale();
        if ($locale == config('app.base_locale')) {
            $field = 'value';
        } else {
            $field = 'value_' . $locale;
        }

        if ($this->type == Parameters::TYPE_INPUT) {
            return $this->pivot->value;
        }

        if ($this->type == Parameters::TYPE_SELECT) {
            $values = $this->values->pluck($field, 'id')->toArray();
            if (isset($values[$this->pivot->value_id])) {
                return $values[$this->pivot->value_id];
            }
        }

        if ($this->type == Parameters::TYPE_STRING) {
            return $this->pivot->$field;
        }

        return '';
    }

    public function scopeIsFilter($query)
    {
        return $query->where('is_filter', 1);
    }
}
