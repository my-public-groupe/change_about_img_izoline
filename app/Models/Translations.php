<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Symfony\Component\Yaml\Yaml;

class Translations extends Model
{
    public $timestamps  =  false;

    public function getValueAttribute()
    {
        $locale = Lang::locale();
        if ($locale != "ru"){
            if (isset($this->attributes['value_' . $locale]))
                return $this->attributes['value_' . $locale];
        }
        return $this->attributes['value'];
    }

    public function setValueAttribute($values)
    {
        foreach($values as $key=>$value){
            if ($key == "ru") {
                $this->attributes['value'] = $value;
                continue;
            }
            $this->attributes['value_' . $key] = $value;
        }
    }

    /**
     * Language folder path.
     *
     * @return string
     */
    public static function languageDir($code)
    {
        return base_path("lang/" . $code . "/");
    }

    public static function getLocaleArrayFromFile($filename, $code)
    {
        $arr = File::getRequire(self::languageDir($code) . $filename . '.php');
        return $arr;
    }

    /**
     * Update language file from yaml.
     *
     * @var text
     */
    public static function updateFromYaml($filename, $yaml, $code)
    {
        $content = '<?php return ' . var_export(Yaml::parse($yaml), true) . ' ?>';
        File::put(self::languageDir($code) . $filename . ".php", $content);
    }

}
