<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class ParametersValues extends Model
{
    public $timestamps  =   false;

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'parameters_products', 'value_id', 'product_id');
    }

    public function getValueAttribute()
    {
        $locale = Lang::locale();
        if ($locale == config('app.base_locale')) {
            return $this->attributes['value'];
        } else {
            return $this->attributes['value_' . $locale];
        }
    }

    public static function getCharacteristicsValues($values_ids)
    {
        $values = self::whereIn('id', $values_ids)->get();

        foreach ($values as $v){
            $out[] = $v->value;
        }

        return empty($out) ? '' : implode(", ", $out);
    }
}
