<?php

namespace App\Models;

class Cart
{

    /**
     * @param int $productId
     * @param int $quantity
     */
    public static function addToCart($productId, $quantity, $thickness = null)
    {
        $cart = session('cart', []);

        if (empty($thickness)) {
            $model = Product::find($productId);

            try {
                $thickness = $model->getThicknessList()[0]['value'] ?? 0;
            } catch (\Exception $e) {
                $thickness = 0;
            }
        }

        $key = $productId . '_' . $thickness;

        $new_q = isset($cart[$key]) ? (int)$cart[$key]['quantity'] + (int)$quantity : (int)$quantity;

        $cart[$key] = [
            'quantity' => $new_q,
            'thickness' => (int)$thickness
        ];

        session(['cart' => $cart]);
    }

    /**
     * @param int $productId
     * @param int $quantity
     */
    public static function changeQuantity($productId, $quantity, $thickness = null)
    {
        $cart = session('cart', []);

        if (!empty($thickness)) {
            $key = $productId . '_' . $thickness;
        } else {
            $key = $productId;
        }

        if (isset($cart[$key])) {
            $cart[$key]['quantity'] = (int)$quantity;
        }

        session(['cart' => $cart]);
    }

    /**
     * @param int $productId
     */
    public static function removeProduct($productId, $thickness = null)
    {
        $cart = session('cart', []);

        if (!empty($thickness)) {
            $key = $productId . '_' . $thickness;
        } else {
            $key = $productId;
        }

        if (isset($cart[$key])) {
            unset($cart[$key]);
        }

        session(['cart' => $cart]);
    }

    /**
     * @return int
     */
    public static function getAllQuantity()
    {
        $cart = session('cart', []);

        // count all number of products
        $allQuantity = 0;

        foreach ($cart as $product_id => $c) {
            $allQuantity += (int)$c['quantity'];
        }

        return $allQuantity;
    }

    /**
     * @return mixed
     */
    public static function getProductsList()
    {
        $cart = session('cart', []);

        $products = [];
        foreach ($cart as $key => $c) {
            $key_parts = explode('_', $key);
            $product_id = $key_parts[0];
            $thickness = count($key_parts) > 1 ? $key_parts[1] : null;

            $product = Product::find($product_id);

            if(!$product){
                continue;
            }

            $products[] = [
                'id' => $product_id,
                'hash' => hash('crc32', $product_id . '_' . $thickness),
                'name' => $product->name,
                'product_number' => $product->sku ?? '',
                'price' => $product->getCurrencyPrice($thickness),
                'currency_price_valute' => $product->getCurrencyPriceValute(),
                'currency_type_price' => $product->getCurrencyTypePrice(),
                'has_sale' => $product->hasSale(),
                'quantity' => $c['quantity'] ?? 1,
                'thickness' => $thickness,
                'photo' => $product->mainphoto(),
                'slug' => $product->getRoute() ?? '',
                'weight' => $product->getWeight(),
                'area' => $product->area,
                'surface' => $product->surface,
                'product_type' => $product->product_type,
                'num_in_box' => $product->num_in_box,
                'num_in_box_thickness' => $product->getNumInBoxThickness($thickness),
                'num_in_box_surface' => $product->getNumInBoxSurface($thickness),
                'round_by' => $product->round_by,
                'placeholder_text' => $product->placeholder_text
            ];
        }

        return $products;
    }

    public static function getAllAmount($order = null)
    {
        if ($order) {
            $products = $order->data['products'];
        } else {
            $products = self::getProductsList();
        }

        $allAmount = 0;

        foreach ($products as $product){
            $model = Product::find($product['id']);

            if(!empty($product['thickness'])) {
                $price = $model->getPriceThickness($product['thickness']);
            }else{
                $price = $model->getThicknessList()[0]['price'] ?? 0;
            }

            $allAmount += (float)$price * (int)$product['quantity'];
        }

        return $allAmount;
    }

    public static function getTotalAmount()
    {
        return self::getAllAmount() + self::getDeliveryCost();
    }

    public static function getDeliveryCost()
    {
        return 0;
    }

    public static function getDeliveryLimit()
    {
        return 0;
    }

    public static function getAllWeight($order = null)
    {
        if ($order) {
            $products = $order->data['products'];
        } else {
            $products = self::getProductsList();
        }

        $allWeight = 0;

        foreach ($products as $product){
            $model = Product::find($product['id']);

            if(!empty($product['thickness'])) {
                $weight = $model->getWeightThickness($product['thickness']);
            }else{
                $weight = $model->getThicknessList()[0]['weight'] ?? 0;
            }

            $allWeight += $weight * $product['quantity'];
        }

        return $allWeight;
    }

    public static function getAllSurface($order = null)
    {
        if ($order) {
            $products = $order->data['products'];
        } else {
            $products = self::getProductsList();
        }

        $allSurface = 0;

        foreach ($products as $product){
            $surface = $product['surface'] * $product['quantity'];
            if(!empty($product['num_in_box_surface'])){
                $surface = $product['num_in_box_surface'] * $product['quantity'];
            }

            $allSurface += $surface;
        }

        return $allSurface;
    }

    public static function clear()
    {
        // cleaning cart
        session(['cart' => []]);
    }
}
