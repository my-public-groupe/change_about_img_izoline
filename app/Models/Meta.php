<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Meta extends Model
{
    public $timestamps  =   false;
    protected $table    =   "meta";

    public function getTitleAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['title_' . $locale])) {
            return $this->attributes['title_' . $locale];
        }

        return $this->attributes['title'];
    }

    public function getMetaKeywordsAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['meta_keywords_' . $locale])) {
            return $this->attributes['meta_keywords_' . $locale];
        }

        return $this->attributes['meta_keywords'];
    }

    public function getMetaDescriptionAttribute()
    {
        $locale = Lang::locale();
        if ($locale != config('app.base_locale') && isset($this->attributes['meta_description_' . $locale])) {
            return $this->attributes['meta_description_' . $locale];
        }

        return $this->attributes['meta_description'];
    }
}
