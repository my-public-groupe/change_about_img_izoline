<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class MailNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $text;
    public $btn_title;
    public $btn_route;
    public $locale;
    public $view;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $text, $btn_title, $btn_route, $locale = '', $view = 'emails.main-template')
    {
        $this->title = $title;
        $this->text = $text;
        $this->btn_title = $btn_title;
        $this->btn_route = $btn_route;
        $this->view = $view;

        if ($locale != '') {
            $this->locale = $locale;
        } else {
            $this->locale = config('app.fallback_locale');
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        App::setLocale($this->locale);

        $title = $this->title;
        $text = $this->text;
        $btn_title = $this->btn_title;
        $route = $this->btn_route;

        return $this->view($this->view)
            ->subject($title)
            ->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME'))
            ->with(compact('title', 'text', 'btn_title', 'route'));
    }
}
