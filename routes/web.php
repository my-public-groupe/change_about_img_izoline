<?php


/*
 *
 * FRONTEND
 *
 */

\UniSharp\LaravelFilemanager\Lfm::routes();

Auth::routes();

// Localization
Route::get('/js/lang.js', function () {
    $lang = app()->getLocale();

    $files = glob(base_path('lang/' . $lang . '/common.php'));
    $strings = [];

    foreach ($files as $file) {
        $name = basename($file, '.php');
        $strings[$name] = require $file;
    }

    header('Content-Type: text/javascript');
    echo('window.lang = ' . json_encode(app()->getLocale()) . ';');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);

Route::get('/home', function () {
    return redirect()->route('index');
});

Route::get('search-result', 'SearchController@getSearch')->name('get-search-result');

#Страницы каталога
Route::get('catalog', 'CatalogController@getCatalog')->name('catalog.get-catalog');
Route::get('catalog/product/{product_slug}', 'ProductsController@getProduct')->name('catalog.get-product');
Route::get('catalog/{parent_category}', 'CatalogController@getParentCategory')->name('catalog.get-parent-category');
Route::get('catalog/{parent_category}/{child_category}', 'CatalogController@getChildrenCategory')->name('catalog.get-child-category');

#Корзина, оформление заказа
Route::post('cart/checkout', 'CartController@postCheckout')->name('cart.checkout');
Route::get('cart/success', 'CartController@getCartSuccess')->name('cart.success');
Route::post('cart/add', 'CartController@addToCart')->name('cart.add');
Route::get('cart/get-cart-quantity', 'CartController@getCartQuantity')->name('cart.get-cart-quantity');
Route::post('cart/get-cart-info', 'CartController@getCartInfo')->name('cart.get-cart-info');
Route::post('cart/get-products', 'CartController@getCartProducts')->name('cart.get-products');
Route::post('cart/change-quantity', 'CartController@changeCartQuantity')->name('cart.change-quantity');
Route::get('cart', 'CartController@getCart')->name('cart.index');
Route::get('checkout', 'CartController@checkoutPage')->name('cart.checkout-page');
Route::post('checkout-manager', 'CartController@postCheckoutManager')->name('cart.checkout-manager');
Route::get('cart/success-manager', 'CartController@getCartSuccessManager')->name('cart.success-manager');

Route::post('send-ask-form', ['uses' => 'MailController@sendAskForm', 'as' => 'send-ask-form']);
Route::post('send-cheap-form', ['uses' => 'MailController@sendCheapForm', 'as' => 'send-cheap-form']);
Route::post('send-consultation-form', ['uses' => 'MailController@sendConsultationForm', 'as' => 'send-consultation-form']);
Route::post('send-contacts-form', ['uses' => 'MailController@sendContactsForm', 'as' => 'send-contacts-form']);

Route::get('wishlist', 'HomeController@getWishlist')->name('get-wishlist');
Route::post('wishlist/add', 'HomeController@addWishlist')->name('add-wishlist');
Route::post('wishlist/remove', 'HomeController@removeWishlist')->name('remove-wishlist');

Route::get('compare', 'HomeController@getCompare')->name('get-compare');
Route::post('compare/add', 'HomeController@addCompare')->name('add-compare');
Route::post('compare/remove', 'HomeController@removeCompare')->name('remove-compare');

Route::get('logout', ['uses' => 'Auth\AuthController@logout', 'as' => 'logout']);

Route::get('contacts', 'ContentController@getContacts')->name('get-contacts');

Route::get('conditions', 'ContentController@conditions')->name('conditions');
Route::get('videos', 'ContentController@getVideos')->name('videos');
Route::get('videos/{category}', 'ContentController@getVideo')->name('get-videos');
Route::get('documentation', 'ContentController@documentation')->name('documentation');
Route::get('success', function () {
    return view('cart.success');
})->name('get-success');

//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
//!!!!этот роут должен быть всегда последним!!!!!
Route::get('{slug}', ['uses' => 'ContentController@getBySlug', 'as' => 'content']);
