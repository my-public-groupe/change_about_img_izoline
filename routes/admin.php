<?php

/*
 *
 *  admin panel
 *
 */

Route::get('admin/login', 'Admin\AdminController@getLogin');

Route::get('logout', 'Auth\AuthController@logout');

Route::post('admin/login', 'Admin\AdminController@postLogin');

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('login-as/{user_id}', ['uses' => 'Admin\AdminController@loginAs', 'as' => 'login-as']);

    Route::get('/', ['uses' => 'Admin\AdminController@index', 'as' => 'admin.index']);

    Route::get('json/changevisibility', 'Admin\JsonController@getChangeVisibility');

    Route::get('json/delete', 'Admin\JsonController@getDeleteModel');

    Route::resource('lists', 'Admin\ListsController');

    Route::resource('collections', 'Admin\CollectionController');
    Route::get('get-collections', 'Admin\CollectionController@ajaxData');

    Route::get('get-users', 'Admin\UserController@ajaxData');
    Route::resource('users', 'Admin\UserController');

    Route::resource('content', 'Admin\ContentController');

    Route::resource('categories', 'Admin\CategoriesController');
    Route::get('get-categories', 'Admin\CategoriesController@ajaxData');

    Route::resource('categories_news', 'Admin\CategoriesNewsController');
    Route::get('get-categories-news', 'Admin\CategoriesNewsController@ajaxData');

    Route::resource('categories_materials', 'Admin\CategoriesMaterialsController');
    Route::get('get-categories-materials', 'Admin\CategoriesMaterialsController@ajaxData');

    Route::resource('news', 'Admin\NewsController');

    Route::resource('products', 'Admin\ProductsController');

    Route::get('get-products', 'Admin\ProductsController@ajaxData');

    Route::resource('manufacturers', 'Admin\ManufacturersController');

    Route::get('get-manufacturers', 'Admin\ManufacturersController@ajaxData');

    Route::get('clear-cache', 'Admin\AdminController@clearCache');

    Route::get('translations', 'Admin\TranslationsController@edit');

    Route::post('translations', 'Admin\TranslationsController@save');

    Route::resource('parameters',       'Admin\ParametersController');

    Route::get('json/remove-parameter', 'Admin\ParametersController@removeParameter');

    Route::get('json/remove-parameter-value', 'Admin\ParametersController@removeParameterValue');

    Route::get('json/get-category-parameters',  'Admin\ParametersController@getCategoryParameters');

    Route::get('orders/settings', 'Admin\OrdersController@settings');

    Route::post('orders/save-settings', 'Admin\OrdersController@saveSettings')->name('save-orders-settings');

    Route::resource('orders', 'Admin\OrdersController');

    Route::get('get-orders', 'Admin\OrdersController@ajaxData');

    Route::post('change-order-status', 'Admin\OrdersController@changeStatus')->name('change-order-status');

    Route::get('get-users', 'Admin\UserController@ajaxData');

    Route::get('login-as-user/{id}', 'Admin\UserController@loginAsUser')->name('login-as-user');

    Route::get('settings', 'Admin\AdminController@settingsPage');
    Route::post('settings', 'Admin\AdminController@saveSettings')->name('save-settings');

    Route::post('json/remove-pdf', 'Admin\ListsController@removePdf')->name('remove-pdf');
});

Route::group(['middleware' => ['admin']], function () {
    Route::any('photos/upload', 'Admin\PhotosController@upload');
    Route::get('photos/getphotos', 'Admin\PhotosController@getJSONPhotos');
    Route::get('photos/changesort', 'Admin\PhotosController@changesort');
    Route::get('photos/delete/{id}', 'Admin\PhotosController@destroy');
});
