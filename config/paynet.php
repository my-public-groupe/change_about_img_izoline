<?php

return [
    'merchant_code' => env('MERCHANT_CODE'),
    'merchant_sec_key' => env('MERCHANT_SEC_KEY'),
    'merchant_user' => env('MERCHANT_USER'),
    'merchant_user_pass' => env('MERCHANT_USER_PASS'),
    'merchant_mode' => env('MERCHANT_MODE'),
    'link_success' => env('MERCHANT_LINK_SUCCESS'),
    'link_cancel' => env('MERCHANT_LINK_CANCEL'),
];
