<?php return array (
  'title' => 'Izoline magazinul de materiale termo și hidroizolante din Chișinău / Moldova',
  'meta_keywords' => '',
  'meta_description' => 'Izoline - magazinul de materiale termo și hidroizolante de înaltă calitate de la branduri renumite la prețuri rezonabile. Importator direct de la producători',
  'home' => 'Acasă',
  'address' => 'Adresa magazinului',
  'form_address' => 'Adresa',
  'main_lang' => 'Română',
  'option_lang' => 'Русский',
  'shop' => 'Magazin',
  'contacts' => 'Contacte',
  'waterproof' => 'Hidroizolarea',
  'thermo_insulation' => 'Izolație termică',
  'materials' => 'Materiale',
  'protective_materials' => 'Filme protectoare',
  'accessories' => 'Accesorii',
  'fasteners' => 'Elemente de fixare',
  'telephone' => 'Telefon',
  'need_help' => 'Ai nevoie de ajutor de specialitate?',
  'get_consultation' => 'Solicitați o consultație gratuită',
  'get_call' => 'Solicitați un apel',
  'our_telephones' => 'Telefoanele noastre',
  'roll_materials' => 'Materiale bituminoase și polimerice în role',
  'pvc_membranes' => 'Membrane din PVC',
  'primers_mastics' => 'Primeri și masticuri',
  'xps' => 'Polistiren extrudat cu spumă (XPS)',
  'stone_wool' => 'Vată minerală',
  'our_goods' => 'Produsele noastre',
  'our_shop' => 'Magazinul nostru',
  'about_us' => 'Despre noi',
  'confidence' => 'Politica de confidențialitate',
  'documentation' => 'Documentația tehnică',
  'helpful_videos' => 'Videoclipuri utile',
  'recommended' => 'Vă recomandăm',
  'sale' => 'Reduceri',
  'filters' => 'Filtre',
  'apply' => 'Aplică',
  'recommended_goods' => 'Produse recomandate',
  'categories_goods' => 'Categorii de produse',
  'show_all_categories' => 'Vezi toate categoriile',
  'cart' => 'Coș',
  'product' => 'Produs',
  'price' => 'Preț',
  'quantity' => 'Cantitate',
  'total' => 'Total',
  'cart_totals' => 'DESPRE COMANDĂ:',
  'total_volume' => 'Volum total',
  'total_weight' => 'Greutate totală',
  'total_price' => 'Preț total',
  'do_order' => 'Plasează comanda',
  'do_manager_order' => 'Trimite la manager',
  'total_volume_units' => 'm3',
  'total_weight_units' => 'kg',
  'total_price_units' => 'lei',
  'checkout' => 'Finalizarea comenzii',
  'your_order' => 'Comanda Dumneavoastră',
  'shipping' => 'Livrare',
  'order' => 'Comandă',
  'order_details' => 'Detalii comandă',
  'name' => 'Nume',
  'phone' => 'Telefon',
  'email' => 'Adresă de email',
  'need_shipping' => 'Este necesară livrarea?',
  'street' => 'Stradă',
  'apt' => 'Nr.',
  'city' => 'Oraș',
  'dist' => 'Sector',
  'order_comment' => 'Comentariu (opțional)',
  'contact_us_form' => 'Contactați-ne',
  'contact_us_form_text' => 'Trimiteți-ne întrebarea dvs. și cu siguranță vă vom contacta',
  'your_message' => 'Mesajul tău',
  'send' => 'Trimite',
  'our_address' => 'Adresa noastră',
  'store_address' => 'Strada Muncești, 801B, Chișinău MD-2029 Republica Moldova',
  'phone_numbers' => 'Numere de telefon:',
  'working_time' => 'Program de lucru',
  'working_time_common_days' => 'Luni - Vineri:',
  'working_time_weekends' => 'Sâmbătă - Duminică:',
  'work' => 'Locuri de muncă',
  'work_text' => 'Dacă sunteți interesat să lucrați în compania noastră, scrieți-ne la adresa de e-mail',
  'about_videos' => 'Despre secțiune',
  'about_videos_text' => 'Aici veți găsi materiale video utile care vă vor ajuta să aflați despre diferite tipuri de materiale izolatoare, utilizarea lor și modul de instalare.',
  'categories' => 'Categorii',
  'description' => 'Descriere',
  'specification' => 'Specificații',
  'faq' => 'Întrebări frecvente',
  'your_question' => 'Întrebarea DVS',
  'have_question' => 'Aveți o întrebare?',
  'features_title_1' => 'Produse de calitate',
  'features_title_2' => 'Prețuri competitive',
  'features_title_3' => 'Întârziere la plată',
  'features_title_4' => 'Expediere și livrare rapide',
  'features_title_5' => 'Returnarea produselor',
  'features_text_1' => 'Lucrăm doar cu producători verificați pentru a garanta clienților noștri calitatea superioară a produselor noastre.',
  'features_text_2' => 'Oferim prețuri rezonabile pentru produsele noastre, astfel încât clienții noștri să poată obține cea mai bună valoare pentru preț.',
  'features_text_3' => 'Suntem pregătiți să oferim condiții convenabile de plată partenerilor noștri.',
  'features_text_4' => 'Oferim expediere rapidă a produselor din depozit și livrare în orice regiune a Republicii Moldova.',
  'features_text_5' => 'Ați rămas cu material neutilizat? Îl puteți returna fără probleme, cu condiția să păstrați aspectul și ambalajul produsului.',
  'site_phone1' => '0681 55 111',
  'site_phone2' => '0681 77 111',
  'site_phone3' => '0683 66 333',
  'site_phone_contacts_1' => '+373 68 155111',
  'site_phone_contacts_2' => '+373 68 177111',
  'site_email' => 'office@izoline.md',
  'site_address' => 'Chisinau, str. Muncesti 801b',
  'payment' => 'Plată',
  'order_success' => 'Comandă #:order_id',
  'order_success_text' => 'Vă mulțumim pentru comandă! Unul dintre managerii noștri vă va contacta în cel mai scurt timp pentru a clarifica toate detaliile.',
  'new_order_status' => 'Noua stare a comenzii',
  'order_status_changed' => 'Starea comenzii dumneavoastră a fost modificată în <b>:new_status_text</b>',
  'order_status_0' => 'Nouă',
  'order_status_1' => 'În curs de livrare',
  'order_status_2' => 'Livrată',
  'card' => 'Achitare cu Card la livrare',
  'cash' => 'Achitare cu Cash la livrare',
  'mail_new_message' => 'Mesaj nou',
  'message' => 'Mesaj',
  'compare' => 'Compară produse',
  'available' => 'Disponibil',
  'add_to_cart' => 'În coș',
  'add_to_cart_long' => 'Aflați suma',
  'product_id' => 'Cod produs',
  'remove' => 'Șterge',
  'wishlist' => 'Favorite',
  'add_wishlist' => 'La Favorite',
  'added_wishlist' => 'Produsul a fost adăugat în wishlist',
  'all_categories' => 'Toate categoriile',
  'search' => 'Căutare...',
  'search_result' => 'Rezultatele căutării ":searchword"',
  'search_total' => 'Am fost găsite :search_total produse',
  'no_products' => 'Nu a fost găsit niciun produs',
  'all_rights_reserved' => 'Toate drepturile rezervate',
  'question' => 'Întrebare',
  'mail_ask_form' => 'Întrebare de pe site',
  'success' => 'Succes',
  'ask_form_sent' => 'Cererea Dvs. a fost trimisă cu succes. Managerul nostru vă va contacta în scurt timp.',
  'contact_form_sent' => 'Mesajul Dvs. a fost trimis cu succes. Managerul nostru vă va contacta în scurt timp.',
  'measure_type_4' => 'cutie',
  'measure_type_2' => 'placă',
  'measure_type_3' => 'rola',
  'measure_type_1' => 'unitate',
  'price_square' => 'm<sup>2</sup>',
  'price_cubic' => 'm<sup>3</sup>',
  'price_box' => 'ambalaj',
  'price_piece' => 'buc.',
  'availability' => 'Disponibil',
  'in_stock' => 'În stoc',
  'out_of_stock' => 'La comandă',
  'lei' => 'lei',
  'calc_qty' => 'Calculează cantitatea',
  'calc' => 'Calculează',
  'i_need' => 'Am nevoie',
  'calc_result' => 'Se va primi',
  'lists' => 'folii',
  'boxes' => 'cutii',
  'rolls' => 'role',
  'select_qty' => 'Introduceți cantitatea',
  'add_compare' => 'Compară',
  'added_compare' => 'Produsul a fost adaugat în comparație',
  'found_cheaper' => 'Ați găsit mai ieftin?',
  'found_cheaper_text' => 'Vrem să ne asigurăm că obțineți un produs de calitate la cel mai bun preț posibil în magazinul nostru. Dacă găsiți același produs sau un produs similar la un preț mai mic decât al nostru, vă rugăm să ne dați detalii mai jos sau să ne contactați telefonic. Vom încerca să vă facem o ofertă mai bună.',
  'write_message' => 'Scrie-ne un mesaj',
  'product_shop_link' => 'Link la produsul sau numele magazinului',
  'comment' => 'Comentariu',
  'mail_cheap_form' => 'Cerere de reducere a prețului',
  'consultation' => 'Consultație',
  'mail_consultation' => 'Cerere de consultație',
  'mail_checkout_manager' => 'Cerere de comandă',
  'order_success_manager' => 'Cererea fost trimisă cu succes!',
  'order_success_manager_text' => 'Managerul nostru vă va contacta în scurt timp pentru a confirma detaliile comenzii',
  'sort_date_desc' => 'Ultimele adăugate',
  'sort_price_desc' => 'Preț mare',
  'sort_price_asc' => 'Preț mic',
  'all' => 'Toate',
  'added_to_cart' => 'Produsul a fost adăugat în cos',
  'order_from_site' => 'Comandă nouă de pe site',
  'please_wait' => 'Așteptați...',
  'width' => 'Lățime, mm',
  'length' => 'Lungime, mm',
  'thickness' => 'Grosime, mm',
  'returns' => 'Retururi de achiziție',
  'delivery_terms' => 'Termeni de livrare',
  'return_terms' => 'Termeni de returnare',
  'pickup' => 'Ridicare personală',
  'close' => 'Închide',
  'answers' => 'Întrebări frecvente ale clienților noștri',
  'faq-title-1' => 'Cum pot determina câte unități de produs am nevoie?',
  'faq-text-1' => '',
  'faq-title-2' => 'Cum pot ridica eu însumi produsul?',
  'faq-text-2' => '',
  'faq-title-3' => 'Pot solicita livrarea?',
  'faq-text-3' => '',
  'faq-title-4' => 'Cum pot returna sau înlocui un produs?',
  'faq-text-4' => '',
  'faq-title-5' => 'Cum pot plasa și plăti comanda mea?',
  'faq-text-5' => '',
  'faq-title-6' => 'Am găsit produsul mai ieftin. Cum pot face alegerea corectă?',
  'faq-text-6' => '',
  'faq-title-7' => 'Ce trebuie să fac dacă nu pot găsi produsul de care am nevoie pe site?',
  'faq-text-7' => '',
  'faq-title-8' => 'Produsul are garanție?',
  'faq-text-8' => '',
  'about-part-1' => '<strong>"IZOLINE"</strong> - magazinul online al companiei SOLAR-CONSTRUCT SRL - furnizor direct de materiale termo și hidroizolante pentru acoperișurile plate, pereți și fundații din Moldova. Ne specializăm în vânzarea de produse izolante moderne, ecologice și de înaltă calitate de la mărci cunoscute precum "<strong>PNP</strong>", "<strong>ROCKWOOL</strong>", "<strong>BELTEP</strong>", "<strong>FDT Flachdach Technologie GmbH</strong>", "<strong>ELEVATE</strong>", "<strong>SWEETONDALE</strong>". Scopul nostru este să oferim clienților noștri o calitate superioară a produselor la prețuri rezonabile.',
  'about-part-2' => 'Oferim o gamă largă de materiale izolante, accesorii și unelte adecvate pentru orice proiect de construcție și de reparații. Atât clienții noștri en-gros, cât și cei en-detail pot conta pe calitatea superioară a produselor noastre și pe nivelul adecvat de competență al consultanților noștri.<a class="u-header-collapse__nav-link" href="/about-us"><strong> Aflați mai mult > </strong></a>',
  'build_route' => 'Construiește traseul:',
  'photo' => 'Foto',
  'working_time_saturday' => 'Sâmbătă:',
  'working_time_sunday' => 'Duminică:',
  'weekend' => 'Zi liberă',
  'self_payment' => 'Ridicare din magazin',
  'paid_delivery' => 'Livrare cu plată',
  'placeholder_list' => 'Plăcile se vând numai în ',
  'placeholder_box' => 'Plăcile se vând numai în ',
  'placeholder_list_second' => 'ambalaje de {x} bucăți.',
  'placeholder_box_second' => 'ambalaje de {x}',
  404 => 'Din păcate, linkul pe care l-ați urmat este incorect sau această pagină a fost eliminată.<br>Folosiți meniul site-ului și veți găsi cu siguranță ceea ce vă trebuie!',
  'tile-delivery-infographic' => 'Livrare în toată Moldova',
  'delivery-infographic' => 'În Chișinău ~ 300 lei.',
  'tile-return-infographic' => 'Retururi ușoare de produse',
  'return-infographic' => 'În termen de 14 zile',
  'tile-certificate-infographic' => 'Articol certificat',
  'certificate-infographic' => 'Numai produse de calitate',
  'cookies_text' => 'Acest site web folosește cookie-uri pentru a vă îmbunătăți experiența. Continuând să utilizați site-ul, sunteți de acord cu prelucrarea cookie-urilor. ',
  'cookies_text_second' => 'Puteți citi mai multe despre politica noastră de confidențialitate pe pagina - ',
  'good' => 'Excelent!',
) ?>