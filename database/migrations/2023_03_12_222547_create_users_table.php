<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('token', 255);
            $table->string('new_email', 255);
            $table->string('password');
            $table->string('open_password')->nullable();
            $table->string('description_short')->nullable();
            $table->text('description')->nullable();
            $table->string('phone', 20)->nullable();
            $table->text('other_phones');
            $table->string('telegram', 255);
            $table->string('viber', 255);
            $table->string('facebook', 255);
            $table->string('instagram', 255);
            $table->boolean('enabled')->default(true);
            $table->tinyInteger('rights')->default(0);
            $table->text('params')->nullable();
            $table->text('addresses');
            $table->boolean('masterclass_agree');
            $table->rememberToken();
            $table->boolean('promo');
            $table->boolean('wordpress');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
