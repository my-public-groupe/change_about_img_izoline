<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('name', 255);
            $table->string('name_ro', 255);
            $table->string('name_en', 255);
            $table->text('description')->nullable();
            $table->text('description_ro')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_short')->nullable();
            $table->text('description_short_ro')->nullable();
            $table->text('description_short_en')->nullable();
            $table->string('product_number', 255);
            $table->text('params')->nullable();
            $table->double('price', 15, 2)->nullable();
            $table->double('sale_price', 15, 2)->nullable();
            $table->double('price_ron', 15, 2)->nullable();
            $table->double('sale_price_ron', 15, 2)->nullable();
            $table->double('price_eur', 15, 2)->nullable();
            $table->double('sale_price_eur', 15, 2)->nullable();
            $table->string('slug')->nullable()->unique();
            $table->text('recommended')->nullable();
            $table->boolean('enabled')->default(true);
            $table->integer('views')->default(0);
            $table->integer('sort')->default(0);
            $table->boolean('availability')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
