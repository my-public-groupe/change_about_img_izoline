<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_values', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value')->nullable();
            $table->string('value_ro')->nullable();
            $table->string('value_en')->nullable();
            $table->text('reserve');
            $table->integer('sort')->default(0);
            $table->unsignedInteger('parameters_id')->index('parameter_values_parameter_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters_values');
    }
};
