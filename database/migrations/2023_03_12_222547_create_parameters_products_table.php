<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parameter_id')->index('parameters_products_parameter_id_foreign');
            $table->unsignedInteger('product_id')->index('parameters_products_product_id_foreign');
            $table->string('value')->index();
            $table->string('value_ro')->index();
            $table->string('value_en')->index();
            $table->integer('value_id')->index();
            $table->integer('sort')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters_products');
    }
};
