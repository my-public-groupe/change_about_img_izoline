<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('name_ro', 255);
            $table->string('name_en', 255);
            $table->boolean('enabled')->default(true);
            $table->integer('sort')->default(0);
            $table->string('slug', 255)->nullable()->default('NULL')->unique('categories_slug_unique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_news');
    }
};
