<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_collection', function (Blueprint $table) {
            $table->foreign(['collection_id'])->references(['id'])->on('collections')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['product_id'])->references(['id'])->on('products')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_collection', function (Blueprint $table) {
            $table->dropForeign('product_collection_collection_id_foreign');
            $table->dropForeign('product_collection_product_id_foreign');
        });
    }
};
