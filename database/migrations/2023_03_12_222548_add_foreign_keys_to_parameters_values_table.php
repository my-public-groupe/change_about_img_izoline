<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters_values', function (Blueprint $table) {
            $table->foreign(['parameters_id'], 'parameter_values_parameter_id_foreign')->references(['id'])->on('parameters')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters_values', function (Blueprint $table) {
            $table->dropForeign('parameter_values_parameter_id_foreign');
        });
    }
};
