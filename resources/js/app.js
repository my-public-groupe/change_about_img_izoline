import {createApp} from 'vue'
import {store} from './store';


window.route = require('./route');

const routePlugin = {
    install(Vue, options) {
        Vue.prototype.$route = window.route; // we use $ because it's the Vue convention
    }
};

import {createI18n} from 'vue-i18n'

const i18n = createI18n({
    locale: '' + window.lang,
    silentTranslationWarn: true
});

export async function loadLanguageAsync(lang, locale_timestamp) {
    i18n.global.setLocaleMessage(lang, await store.dispatch('fetchLocaleMessages', {lang, locale_timestamp}));
}

// get locales
loadLanguageAsync('' + window.lang, window.locale_timestamp)

import Cart from "./components/cart/Cart";
import CartList from "./components/cart/CartList";
import QtyCalculator from "./components/QtyCalculator";

import NotificationPopup from "./components/cart/NotificationPopup";

const app = createApp({});

app.component('cart', Cart);
app.component('cart-list', CartList);
app.component('notification-popup', NotificationPopup);
app.component('qty-calculator', QtyCalculator);

app.use(store);
app.use(i18n);

app.config.globalProperties.$route = function (link) {
    return window.baseUrl + link;
}

app.mount("#app");
