<h1>@lang('common.mail_cheap_form')</h1>
<br>
<b>@lang('common.name'):</b> {{ $data['name'] ?? '' }}<br/>
<b>@lang('common.phone'):</b> {{ $data['phone'] ?? '' }}<br/>
<b>@lang('common.product_shop_link'):</b> {{ $data['shop_link'] ?? '' }}<br/>
<b>@lang('common.price'):</b> {{ $data['price'] ?? '' }}<br/>
<b>@lang('common.comment'):</b> {{ $data['comment'] ?? '' }}
<br/>
<br/>
<b>URL:</b> <a href="{{ $data['referer'] }}">{{ $data['referer'] }}</a>
