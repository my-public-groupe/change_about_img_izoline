<h1>@lang('common.mail_consultation')</h1>
<br>
<b>@lang('common.name'):</b> {{ $data['name'] ?? '' }}<br/>
<b>@lang('common.phone'):</b> {{ $data['phone'] ?? '' }}<br/>
<b>Email:</b> {{ $data['email'] ?? '' }}<br/><br/>
<b>URL:</b> <a href="{{ $data['referer'] }}">{{ $data['referer'] }}</a>
