<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <title>Izoline</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <style type="text/css">
        body {
            text-align: center;
            margin: 0 auto;
            width: 650px;
            font-family: 'Open Sans', sans-serif;
            background-color: #e2e2e2;
            display: block;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        li {
            display: inline-block;
            text-decoration: unset;
        }

        a {
            text-decoration: none;
        }

        p {
            margin: 15px 0;
        }

        h5 {
            color: #444;
            text-align: left;
            font-weight: 400;
        }

        .text-center {
            text-align: center
        }

        .main-bg-light {
            background-color: #fafafa;
        }

        .title {
            color: #444444;
            font-size: 22px;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-bottom: 0;
            text-transform: uppercase;
            display: inline-block;
            line-height: 1;
        }

        table {
            margin-top: 30px
        }

        table.top-0 {
            margin-top: 0;
        }

        table.order-detail {
            border: 1px solid #ddd;
            border-collapse: collapse;
        }

        table.order-detail tr:nth-child(even) {
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
        }

        table.order-detail tr:nth-child(odd) {
            border-bottom: 1px solid #ddd;
        }

        .pad-left-right-space {
            border: unset !important;
        }

        .pad-left-right-space td {
            padding: 5px 15px;
        }

        .pad-left-right-space td p {
            margin: 0;
        }

        .pad-left-right-space td b {
            font-size: 15px;
            font-family: 'Roboto', sans-serif;
        }

        .order-detail th {
            font-size: 16px;
            padding: 15px;
            text-align: center;
            background: #fafafa;
        }

        .footer-social-icon tr td img {
            margin-left: 5px;
            margin-right: 5px;
        }
    </style>
</head>

<body style="margin: 20px auto;">
<table align="center" border="0" cellpadding="0" cellspacing="0"
       style="padding: 0 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;">
    <tbody>
    <tr>
        <td>
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="text-align: left;"
                   width="100%">
                <tr>
                    <td style="text-align: center;">
                        <img src="{{ url('images/logo.png') }}" alt="" style="width: 150px; margin-bottom: 30px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="font-size: 14px; text-align: center;"><b>@lang('common.order_success', ['order_id' => $data['order']->id])</b></p>
                        <p style="font-size: 14px; text-align: center;">@lang('common.order_success_text')</p>
                    </td>
                </tr>
            </table>

            <table class="order-detail" border="0" cellpadding="0" cellspacing="0" align="left"
                   style="width: 100%;    margin-bottom: 50px;">
                <tr align="left">
                    <th>@lang('common.product')</th>
                    <th>@lang('common.quantity')</th>
                    <th>@lang('common.price') </th>
                </tr>

                @foreach($data['data']['products'] as $product)
                    <tr>
                        <td>
                            <p>
                                {{ $product['name'] }}
                                @if(!empty($product['thickness']))
                                    <br>
                                    <small>({{ $product['thickness'] }} mm)</small>
                                @endif
                            </p>
                            <img src="{{ url($product['photo']) }}" width="80">
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                            <p>{{ $product['quantity'] }}</p>
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                            <p><b>{{ $product['price'] }} {{ $data['data']['currency']['label'] }}</b></p>
                        </td>
                    </tr>
                @endforeach

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.name'):</p>
                    </td>
                    <td colspan="2" align="right">
                        <b>{{ $data['data']['orderInfo']['name'] }}</b>
                    </td>
                </tr>

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.phone'):</p>
                    </td>
                    <td colspan="2" align="right">
                        <b>{{ $data['data']['orderInfo']['phone'] }}</b>
                    </td>
                </tr>

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">Email:</p>
                    </td>
                    <td colspan="2" align="right">
                        <b>{{ $data['data']['orderInfo']['email'] }}</b>
                    </td>
                </tr>

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.form_address'):</p>
                    </td>
                    <td colspan="2" align="right">
                        <b>{{ $data['data']['orderInfo']['address'] }}</b>
                    </td>
                </tr>

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.comment'):</p>
                    </td>
                    <td colspan="2" align="right">
                        <b>{{ $data['data']['orderInfo']['comment'] }}</b>
                    </td>
                </tr>

                <tr class="pad-left-right-space">
                    <td colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.payment'):</p>
                    </td>
                    <td colspan="2" align="right">
                        @if($data['data']['orderInfo']['payment'] == 'self_payment')
                            <b>@lang('common.self_payment')</b>
                        @elseif($data['data']['orderInfo']['payment'] == 'paid_delivery')
                            <b>@lang('common.paid_delivery')</b>
                        @endif
                    </td>
                </tr>

                <tr class="pad-left-right-space ">
                    <td class="m-b-5" colspan="2" align="left">
                        <p style="font-size: 14px;">@lang('common.total'):</p>
                    </td>
                    <td class="m-b-5" colspan="2" align="right">
                        <b>{{ $data['data']['amount'] }} {{ $data['data']['currency']['label'] }}</b>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
    </tbody>
</table>
<table class="main-bg-light text-center top-0" align="center" border="0" cellpadding="0" cellspacing="0"
       width="100%">
    <tr>
        <td style="padding: 30px;">
            <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">
                <tr>
                    <td>
                        <p style="font-size:13px; margin:0;">{{ date('Y') }} - <a href="https://izoline.md">izoline.md</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
