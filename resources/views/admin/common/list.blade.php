@extends('admin.body')
@section('title', $title)
@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> {{ $title }}</small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            @if(!isset($no_create))
                <a class="btn btn-success @if(isset($modalbox) && $modalbox) modalbox @endif"
                   href="{{ URL::to('admin/' . $model . '/create') }}" title="Создать элемент {{ $title }}">
                    <i class="ace-icon fa fa-plus-square-o  bigger-120"></i>
                    Создать
                </a>
            @endif

            @if(isset($show_calendar))
                <a class="btn btn-success"
                   href="{{ URL::to('admin/' . $model . '/calendar') }}">
                    <i class="ace-icon fa fa-calendar bigger-120"></i>
                    Календарь
                </a>
            @endif

            @yield('listbuttons')

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header"> {{ $desc }} </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover table-responsive dataTable">
                <thead>
                <tr>
                    <th class="center">
                        <label class="pos-rel"><input type="checkbox" class="ace"/><span class="lbl"></span></label>
                    </th>
                    <th align="center">ID</th>
                    @foreach($fields as $field)
                        <th align="center">{{ $field }}</th>
                    @endforeach
                    @if(!isset($no_visibility))
                        <th align="center"><i class="ace-icon fa fa-eye-slash bigger-130"></i></th>
                    @endif
                    <th align="center"><i class="menu-icon fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white">
                @foreach($data as $d)
                    <tr class="">
                        <td class="center">
                            <label class="pos-rel"><input type="checkbox" class="ace" data-id="{{ $d->id }}"/><span
                                    class="lbl"></span></label>
                        </td>
                        <td align="center">
                            {{ $d->id }}
                        </td>
                        <?php $i = 0; ?>
                        @foreach($fields as $key => $field)
                            <?php $i++; ?>
                                @if($i == 1)
                                    <td align="center">
                                        @if(!isset($no_edit))
                                            <a title="Редактирование"
                                               href="{{ URL::to('admin/'.$model.'/'.$d->id.'/edit') }}"
                                               class="@if(isset($modalbox) && $modalbox) modalbox @endif">{{ $d->$key }}</a>
                                        @endif
                                        @if(isset($view_column))
                                            <a title="Просмотр"
                                               href="{{ URL::to('admin/'.$model.'/'.$d->id) }}">{{ $d->$key }}</a>
                                        @endif

                                        @if (isset($d->photos) && $d->photos->count() > 0)
                                            <a href="{{ $d->mainphoto() }}" class="fancybox"><i
                                                    class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                    @continue
                                @elseif($i == 4 && isset($changeOrderStatus) && $changeOrderStatus == true)
                                    <td align="center">
                                        {{ Form::select("status", config('custom.order_status'), (isset($d->status) ? $d->status : old('status')), ['data-order-id' => $d->id]) }}
                                    </td>
                                    @continue
                                @endif
                            <td align="center">
                                {{ $d->$key }}
                            </td>
                        @endforeach
                        @if(!isset($no_visibility))
                            <td align="center">
                                <div class="action-buttons">
                                    <a href="javascript:void(0);" class="{{ $d->enabled ? 'visible' : 'unvisible' }}"
                                       data-id="{{ $d->id }}" data-model="{{ $model }}">
                                        <i class="ace-icon fa fa-eye bigger-130"></i>
                                    </a>
                                </div>
                            </td>
                        @endif
                        <td align="center">
                            <div class="action-buttons">
                                @if(!isset($no_edit))
                                    <a href="{{ URL::to('admin/'.$model.'/'.$d->id.'/edit') }}"
                                       class="yellow @if(isset($modalbox) && $modalbox) modalbox @endif"><i
                                            class="ace-icon fa fa-pencil bigger-130"></i></a>
                                @endif
                                @if(!isset($no_delete))
                                    {{ Form::open(array('url' => 'admin/' . $model . '/' . $d->id, 'class' => 'pull-right')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::button('<i class="ace-icon fa fa-trash-o bigger-130"></i>', ['type' => 'submit', 'class' => 'red deletebutton']) }}
                                    {{ Form::close() }}
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    @include('admin.partials.datatable-init')

    @include('admin.partials.visibility')

    <script>
        $('#dynamic-table').on('change', $('select[name=status]'), function (e) {
            let order_id = $(e.target).data('order-id');
            let new_status = $(e.target).find('option:selected').val();

            $.post('{{route('admin.change-order-status')}}', {'order_id': order_id, 'new_status': new_status}, function (){
                toastr.success(`Статус заказа №${order_id} изменен`);
            });
        });

        $('#dynamic-table').on('draw.dt', function (e, datatable) {
            $(this).next().addClass('datatable-footer');
        });

        $(function () {
            $('a.fancybox').fancybox();

            @if($model == 'orders')
                $(".deletebutton").click(function (e) {
                    e.preventDefault();

                    if (confirm("Удалить запись?")) {
                        $(this).parents("tr").remove();
                        var url = $(this).parents("form").attr("action");
                        $.post(url, $(this).parents("form").serialize());
                    }
                })
            @endif
        });
    </script>

@endsection
