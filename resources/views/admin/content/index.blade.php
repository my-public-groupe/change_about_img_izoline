@extends('admin.common.list',
    [
        'title'       =>  'Страницы',
        'desc'        =>  'Список страниц сайта',
        'model'       =>  'content',
        'fields'      =>  ['name' => 'Наименование', 'slug' => 'Ссылка', 'created_at' => 'Создан'],
        'data'        =>  $data,
        'no_visibility' => true,
    ]
)
