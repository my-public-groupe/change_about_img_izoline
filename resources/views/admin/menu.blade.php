
<ul class="nav nav-list">
    <li class="">
        <a href="{{ url('admin') }}">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Главная </span>
        </a>
    </li>

    <li>
        <a href="{{ url('admin/categories') }}">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Категории </span>
        </a>
    </li>

    <li>
        <a href="{{ url('admin/manufacturers') }}">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Бренды </span>
        </a>
    </li>

    <li class="">
        <a href="{{ url('admin/categories_materials') }}">
            <i class="menu-icon fa fa-list"></i>
            <span class="menu-text"> Группы материалов</span>
        </a>
    </li>

    <li>
        <a href="{{ url('admin/products') }}">
            <i class="menu-icon fa fa-gift"></i>
            <span class="menu-text"> Товары </span>
        </a>
    </li>

    <li class="">
        <a href="{{ url('admin/content') }}">
            <i class="menu-icon fa fa-file-text-o"></i>
            <span class="menu-text"> Страницы </span>
        </a>
    </li>

    <li>
        <a href="{{ url('admin/orders') }}">
            <i class="menu-icon fa fa-shopping-cart"></i>
            <span class="menu-text"> Заказы </span>
        </a>
    </li>

    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-cogs"></i>
            <span class="menu-text"> Администр.</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
               <a href="{{ url('admin/parameters') }}">
                  <i class="menu-icon fa fa-tasks"></i>
                  <span class="menu-text"> Параметры </span>
               </a>
            </li>

            <li>
                <a href="{{ url('admin/translations') }}">
                    <i class="menu-icon fa fa-globe"></i>
                    <span class="menu-text"> Переводы </span>
                </a>
            </li>
            <li>
                <a href="{{ url('admin/lists') }}">
                    <i class="menu-icon fa fa-list-ol"></i>
                    <span class="menu-text"> Справочники </span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('admin/clear-cache') }}">
                    <i class="menu-icon fa fa-trash-o"></i>
                    <span class="menu-text"> Очистить кеш </span>
                </a>
            </li>
        </ul>
    </li>
</ul><!-- /.nav-list -->
