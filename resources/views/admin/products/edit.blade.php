@extends('admin.body')
@section('title', 'Товар')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{ URL::to('admin/products') }}">Товары</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование товара @isset($data)"{{$data->name}}"@endisset</small>
        </h1>
    </div>

    @if(!isset($data) || isset(request()->copy))
        {{ Form::open(['url' => 'admin/products', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('copy', 1) }}
    @else
        {{ Form::open(['url' => 'admin/products/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <input type="hidden" name="parent_category_id" value="{{ request()->category_id }}">

    @if(isset(request()->copy))
        <div class="alert alert-info">
            <i class="ace-icon fa fa-info-circle"></i> Товар будет создан путем копирования. Внесите необходимые изменения и нажмите "Сохранить".
        </div>
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>

            @if (isset($data))
            <div class="col-sm-2">
                <a href="{{ route('admin.products.edit', [$data->id, 'category_id' => $data->getParentCategoryId(), 'copy' => $data->id]) }}" class="btn btn-info btn-block btn-responsive"><i
                        class="ace-icon fa fa-copy bigger-120"></i> Скопировать
                </a>
            </div>
            @endif
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <span class="label label-xlg label-light">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </span>

                            <span class="label label-xlg label-light">
                                 <a href="{{ $data->getRoute() }}" target="_blank"><i class="ace-icon fa fa-share"></i> Посмотреть на сайте</a>
                            </span>
                        @endif
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="col-sm-6 "><span class="label label-xlg label-light arrowed-right">Опубликован</span></label>
                    <div class="col-sm-6">
                        <label>
                            <input name="enabled"
                                   class="ace ace-switch ace-switch-5"
                                   type="checkbox"
                                   @if (!isset($data->enabled) || (isset($data) && $data->enabled == 1)) checked="checked" @endif>
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Наименование</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="name_ru" class="tab-pane active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab2">
                    <li class="active">
                        <a data-toggle="tab" href="#description_short_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#description_short_ro" aria-expanded="false">RO</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Краткое описание</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="description_short_ru" class="tab-pane active">
                        {{ Form::textarea('description_short[ru]', (isset($data->description_short) ? $data->description_short : old('description_short')), array('class' => 'ckeditor form-control', 'id' => 'editor_desc_short_ru', 'rows' => 6, 'cols' => 50)) }}
                    </div>
                    <div id="description_short_ro" class="tab-pane">
                        {{ Form::textarea('description_short[ro]', (isset($data->description_short_ro) ? $data->description_short_ro : old('description_short_ro')), array('class' => 'ckeditor form-control', 'id' => 'editor_desc_short_ro', 'rows' => 6, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab2">
                    <li class="active">
                        <a data-toggle="tab" href="#params_text_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#params_text_ro" aria-expanded="false">RO</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Параметры (для списка товаров)</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="params_text_ru" class="tab-pane active">
                        {{ Form::textarea('params_text[ru]', (isset($data->params_text) ? $data->params_text : old('params_text')), array('class' => 'ckeditor form-control', 'id' => 'editor_params_text_ru', 'rows' => 6, 'cols' => 50)) }}
                    </div>
                    <div id="params_text_ro" class="tab-pane">
                        {{ Form::textarea('params_text[ro]', (isset($data->params_text_ro) ? $data->params_text_ro : old('params_text_ro')), array('class' => 'ckeditor form-control', 'id' => 'editor_params_text_ro', 'rows' => 6, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('sku', 'Артикул', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sku', (isset($data->sku) ? $data->sku : old('sku')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('in_stock', 'Доступность', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select("in_stock", [1 => 'Есть на складе', 0 => 'Нет на складе'], (isset($data->in_stock) ? $data->in_stock : old('in_stock')), ['class' => 'col-sm-11']) }}
                </div>
            </div>

            <div class="form-group showTip">
                {{ Form::label('category_id', 'Подкатегория', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select("category_id[]", $categories, (isset($data->category) ? $data->category : old('category_id')), ['id' => 'chosencat', 'multiple' => 'true', 'class' => 'tag-input-style col-sm-11 control-label no-padding-right']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('manufacturer_id', 'Бренд', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select("manufacturer_id", ['' => '--- Выберите бренд ---'] + $brands, (isset($data) && $data->manufacturer_id ? $data->manufacturer_id : old('manufacturer_id')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>

            {{--<div class="form-group">
                {{ Form::label('price_eur', 'Цена (MDL)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('price_eur', (isset($data->price) ? $data->price : old('price')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('sale_price_eur', 'Цена со скидкой (MDL)', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sale_price_eur', (isset($data->sale_price) ? $data->sale_price : old('sale_price')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>--}}
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#description" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#productsParameters" data-toggle="tab">Параметры</a>
            </li>
            <li>
                <a href="#productsParameters2" data-toggle="tab">Доп. параметры и цены</a>
            </li>
            <li>
                <a href="#faq" data-toggle="tab">ЧаВо</a>
            </li>
            <li>
                <a href="#recommended" data-toggle="tab">Рекомендуемые</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">

            @include('admin.products.partials.description')

            <div class="tab-pane" id="productsParameters">
                @if ($parameters->isNotEmpty())
                    <ul id="products_sortable">
                        @foreach($parameters as $parameter)
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right"> {{ $parameter->name }} </label>
                                        <div class="col-sm-9">
                                            {{ Form::hidden('parameters[]', $parameter->id) }}
                                            {{ Form::select('value_id['.$parameter->id.'][]',
                                                [0 => 'не выбрано'] + $parameter->values->pluck('value', 'id')->toArray(),
                                                $sel_parameters[$parameter->id] ?? [],
                                                ['class' => 'chosencat col-xs-12 col-sm-4', 'multiple' => true])
                                            }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </ul>
                @else
                    <div class="alert alert-info">
                        Сначала выберите параметры для категории товара и пересохраните товар
                    </div>
                @endif
            </div>

            @include('admin.products.partials.parameters')

            @include('admin.products.partials.faq')

            <div class="tab-pane" id="recommended">
                <div class="row">
                    <div class="col-xs-12 size-container">
                        {{ Form::select('recommended[]', $products, isset($data) ? $data->recommended : [], ['class' => 'form-control chosencat', 'id' => 'products', 'multiple' => 'multiple', 'data-placeholder' => 'Выберите товары']) }}
                    </div>
                </div>
            </div>

            @include('admin.partials.photos', ['table' => 'products', 'table_id' => (isset($data->id) ? $data->id : 0), 'class' => 'showTip L8 active'])

            @include('admin.partials.meta')
        </div>

    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('styles')
    {!! HTML::style('ace/assets/css/datepicker.css') !!}
    {!! HTML::style('ace/assets/css/chosen.css') !!}
@endsection

@section('scripts')
    {!! HTML::script('ace/dist/js/dw_tooltip_c.js') !!}

    <script>
        dw_Tooltip.content_vars = {
            L_Thickness: 'Значения толщины, разделенные запятой.<br><b>Пример</b>: 1000,2000,3000<br>Если необходимо учитывать количество штук, разделите значения через |.<br><b>Пример</b>: 1,1000|2,2000|3,3000',
        }
    </script>


    {!! HTML::script('ace/dist/js/bootstrap-colorpicker.min.js') !!}

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug', ['input_names' => 'name[ru]', 'slugs' => 'slug'])

    @include('admin.partials.datepicker')

    @include('admin.partials.chosen')

    <script>
        $('#faq').on('click', '.add-btn', function() {
            let lang = $(this).data('lang');
            let newRow = $(this).closest('.faq-input-row').clone();
            newRow.find('input').val('');
            newRow.find('textarea').val('');
            newRow.find('[name^="faq_"]').attr('name', 'faq_' + lang + '[]');
            newRow.find('[name^="answer_"]').attr('name', 'answer_' + lang + '[]');
            $(this).closest('.tab-pane').append(newRow);
        });

        $('#faq').on('click', '.remove-btn', function() {
            $(this).closest('.faq-input-row').remove();
        });

        $('.btnAddThicknessRow').click(function() {
            let newRow = $('.thickness-list:visible:last').clone();
            newRow.find('input').val('');
            $('.thickness-list:visible:last').after(newRow);
        });

        $("#product_type_1").on("click", ".btnDeleteThicknessRow", function () {
            if($('.thickness-list').length > 1){
                $(this).parent().remove();
            }
        });

        $("#product_type_4").on("click", ".btnDeleteThicknessRow", function () {
            if($('.thickness-list').length > 1){
                $(this).parent().remove();
            }
        });

        $(function () {
            @if(isset($data))
                $("#product-type").val("{{ $data->product_type }}");

                let sel = "{{ $data->product_type }}";

                if (sel == 1) {
                    $("#product_type_1").find("input[name=measure_type][value={{ $data->measure_type }}]").prop('checked', true);
                    $("#product_type_1").find("input[name=round_by][value={{ $data->round_by }}]").prop('checked', true);

                    $("#product_type_1").removeClass("hide").find("input").attr("disabled", false);
                    $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                } else if (sel == 2) {
                    $("#product_type_2").find("input[name=measure_type][value={{ $data->measure_type }}]").prop('checked', true);
                    $("#product_type_2").find("input[name=round_by][value={{ $data->round_by }}]").prop('checked', true);

                    $("#product_type_2").removeClass("hide").find("input").attr("disabled", false);
                    $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                } else if (sel == 3) {
                    $("#product_type_3").find("input[name=measure_type][value={{ $data->measure_type }}]").prop('checked', true);
                    $("#product_type_3").find("input[name=round_by][value={{ $data->round_by }}]").prop('checked', true);

                    $("#product_type_3").removeClass("hide").find("input").attr("disabled", false);
                    $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                } else if (sel == 4) {
                    $("#product_type_4").find("input[name=measure_type][value={{ $data->measure_type }}]").prop('checked', true);
                    $("#product_type_4").find("input[name=round_by][value={{ $data->round_by }}]").prop('checked', true);

                    $("#product_type_4").removeClass("hide").find("input").attr("disabled", false);
                    $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                    $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                }
            @endif
        });

        $("#product-type").change(function (){
            let sel = $(this).val();

            if (sel == 1) {
                $("#product_type_1").removeClass("hide").find("input").attr("disabled", false);
                $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
            } else if (sel == 2) {
                $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_2").removeClass("hide").find("input").attr("disabled", false);
                $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
            } else if (sel == 3) {
                $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_3").removeClass("hide").find("input").attr("disabled", false);
                $("#product_type_4").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
            } else if (sel == 4) {
                $("#product_type_1").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_2").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_3").addClass("hide").find("input").prop('checked', false).attr("disabled", true);
                $("#product_type_4").removeClass("hide").find("input").attr("disabled", false);
            }
        });

        $(".param-length").keyup(function (){
           let param_length = $(this).val();
           let param_width = $(this).parents().parents().parents().find(".param-width:visible").val();
           let param_thickness = $(this).parents().parents().parents().find(".param-thickness").val();

            param_thickness = param_thickness.split(",")[0];
            if($(this).parents().parents().parents().find(".param-thickness").val().includes('|')){
                param_thickness = $(this).parents().parents().parents().find(".param-thickness").val().split("|")[0].split(",")[1];
            }

           let area = Number(param_length / 1000 * param_width / 1000).toFixed(3);
           $(this).parents().parents().parents().find("input[name=area]").val(area);

           let surface = Number(param_length / 1000 * param_width / 1000 * param_thickness / 1000).toFixed(4);
           $(this).parents().parents().parents().find("input[name=surface]").val(surface);
        });

        $(".param-width").keyup(function (){
            let param_length = $(this).parents().parents().parents().find(".param-length:visible").val();
            let param_width = $(this).val();
            let param_thickness = $(this).parents().parents().parents().find(".param-thickness").val();

            param_thickness = param_thickness.split(",")[0];
            if($(this).parents().parents().parents().find(".param-thickness").val().includes('|')){
                param_thickness = $(this).parents().parents().parents().find(".param-thickness").val().split("|")[0].split(",")[1];
            }

            let area = Number(param_length / 1000 * param_width / 1000).toFixed(3);
            $(this).parents().parents().parents().find("input[name=area]").val(area);

            let surface = Number(param_length / 1000 * param_width / 1000 * param_thickness / 1000).toFixed(4);
            $(this).parents().parents().parents().find("input[name=surface]").val(surface);
        });

        $(".param-thickness").keyup(function (){
            let param_length = $(this).parents().parents().parents().find(".param-length").val();
            let param_width = $(this).parents().parents().parents().find(".param-width").val();

            let param_thickness = $(this).val().split(",")[0];
            if($(this).val().includes('|')){
                param_thickness = $(this).val().split("|")[0].split(",")[1];
            }

            let area = Number(param_length / 1000 * param_width / 1000).toFixed(3);
            $(this).parents().parents().parents().find("input[name=area]").val(area);

            let surface = Number(param_length / 1000 * param_width / 1000 * param_thickness / 1000).toFixed(4);
            $(this).parents().parents().parents().find("input[name=surface]").val(surface);
        });

        $("#product_type_1").on("keyup change", ".thickness-list .thickness", function () {
           let row = $(this).closest(".thickness-list");
           let width_val = $("#product_type_1").find(".param-width").val();
           let length_val = $("#product_type_1").find(".param-length").val();
           let thickness_val = $(this).val();
           let surface = Number(width_val / 1000 * length_val / 1000 * thickness_val / 1000).toFixed(4);

           row.find(".surface").val(surface);
        });

        $("#product_type_4").on("keyup change", ".thickness-list .thickness", function () {
            let row = $(this).closest(".thickness-list");
            let width_val = $("#product_type_4").find(".param-width").val();
            let length_val = $("#product_type_4").find(".param-length").val();
            let thickness_val = $(this).val();
            let surface = Number(width_val / 1000 * length_val / 1000 * thickness_val / 1000).toFixed(4);

            row.find(".surface").val(surface);
        });
    </script>
@append
