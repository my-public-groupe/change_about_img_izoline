<div class="tab-pane" id="description">
    <div class="tabbable tabs-left">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#descRu" data-toggle="tab">Описание RU</a>
            </li>
            <li>
                <a href="#descRo" data-toggle="tab">Описание RO</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane in active" id="descRu">
                {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor_ru')) }}
            </div>
            <div class="tab-pane" id="descRo">
                {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
            </div>
        </div>

    </div>
</div>
