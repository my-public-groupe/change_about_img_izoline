<div class="tab-pane" id="characteristics_old">

    <div class="tabbable tabs-left">

        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#charRu" data-toggle="tab">Характеристики</a>
            </li>
            <li>
                <a href="#charRo" data-toggle="tab" class="lang-ro">Характеристики RO</a>
            </li>
            <li>
                <a href="#charEn" data-toggle="tab" class="lang-en">Характеристики EN</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane in active" id="charRu">
                {{ Form::textarea('characteristics[ru]', (isset($data->characteristics) ? $data->characteristics : old('characteristics')), array('class' => 'ckeditor', 'id' => 'char_editor')) }}
            </div>
            <div class="tab-pane" id="charRo">
                {{ Form::textarea('characteristics[ro]', (isset($data->characteristics_ro) ? $data->characteristics_ro : old('characteristics_ro')), array('class' => 'ckeditor', 'id' => 'char_editor_ro')) }}
            </div>
            <div class="tab-pane" id="charEn">
                {{ Form::textarea('characteristics[en]', (isset($data->characteristics_en) ? $data->characteristics_en : old('characteristics_en')), array('class' => 'ckeditor', 'id' => 'char_editor_en')) }}
            </div>
        </div>

    </div>
</div>