<div class="tab-pane" id="faq">
    <div class="tabbable tabs-left">
        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#faqRu" data-toggle="tab">RU</a>
            </li>
            <li>
                <a href="#faqRo" data-toggle="tab">RO</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane in active" id="faqRu">
                <div class="form-group" style="min-height: 20px;">
                    @if(isset($data) && $data->faq->isNotEmpty())
                        @foreach($data->faq as $item)
                            <div class="faq-input-row">
                                <input type="text" name="question[]" class="form-control faq-question"
                                       placeholder="Вопрос (RU)" value="{{ $item['question'] }}">
                                <textarea name="answer[]" rows="4" class="form-control faq-answer"
                                          placeholder="Ответ (RU)">{{ \Illuminate\Support\Str::replace("<br>", "\n", $item['answer']) }}</textarea>

                                <div class="faq-btns">
                                    <button type="button" class="btn btn-danger btn-sm remove-btn"><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-success btn-sm add-btn" data-lang="ru"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="faq-input-row">
                            <input type="text" name="question[]" class="form-control faq-question"
                                   placeholder="Вопрос (RU)">
                            <textarea name="answer[]" rows="4" class="form-control faq-answer"
                                      placeholder="Ответ (RU)"></textarea>

                            <div class="faq-btns">
                                <button type="button" class="btn btn-danger btn-sm remove-btn"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-success btn-sm add-btn" data-lang="ru"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane" id="faqRo">
                <div class="form-group" style="min-height: 20px;">
                    @if(isset($data) && $data->faq_ro->isNotEmpty())
                        @foreach($data->faq_ro as $item)
                            <div class="faq-input-row">
                                <input type="text" name="question_ro[]" class="form-control faq-question"
                                       placeholder="Вопрос (RO)" value="{{ $item['question'] }}">
                                <textarea name="answer_ro[]" rows="4" class="form-control faq-answer"
                                          placeholder="Ответ (RO)">{{ \Illuminate\Support\Str::replace("<br>", "\n", $item['answer']) }}</textarea>

                                <div class="faq-btns">
                                    <button type="button" class="btn btn-danger btn-sm remove-btn"><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-success btn-sm add-btn" data-lang="ro"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="faq-input-row">
                            <input type="text" name="question_ro[]" class="form-control faq-question"
                                   placeholder="Вопрос (RO)">
                            <textarea name="answer_ro[]" rows="4" class="form-control faq-answer"
                                      placeholder="Ответ (RO)"></textarea>

                            <div class="faq-btns">
                                <button type="button" class="btn btn-danger btn-sm remove-btn"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-success btn-sm add-btn" data-lang="ro"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
