@extends('admin.body')
@section('title', 'Товары')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Товары</small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-success dropdown-toggle">
                    Создать
                    <i class="ace-icon fa fa-angle-down icon-on-right"></i>
                </button>

                <ul class="dropdown-menu dropdown-success dropdown-menu-left">
                    @foreach($categories as $id => $name)
                        <li>
                            <a href="{{ route('admin.products.create', ['category_id' => $id]) }}">{{ $name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div><!-- /.btn-group -->

            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список товаров </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover table-responsive dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">Создан</th>
                    <th align="center">Артикул</th>
                    <th align="center">Изображение</th>
                    <th align="center">Название</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@stop

@section('scripts')

    <script>
        var columns = [
            { data : 'id', className: 'center'},
            {
                data : 'created_at',
                className: 'center',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href="admin/products/${full.id}/edit">${data}</a>`;
                    }
                },
            },
            {
                data : 'sku',
                className: 'center',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href="admin/products/${full.id}/edit">${data}</a>`;
                    }
                },
            },
            {
                data : 'mainphoto',
                "render": {
                    "display": function (data, type, full) {
                        if(full.mainphoto != 'nophoto.png'){
                            return `<img height='100' src="${full.mainphoto}">`;
                        }else{
                            return '-';
                        }
                    }
                },
                className: 'center'
            },
            {
                data : 'name',
                className: 'center',
                "render": {
                    "display": function (data, type, full) {
                        return `<a href="admin/products/${full.id}/edit">${data}</a>`;
                    }
                },
            },
            { data : 'actions', className : 'center' }
        ];

        $("#filter-pending-product").change(function () {
            var params = {};

            if($(this).is(":checked")){
                params['status'] = 0;
            }

            $("#dynamic-table").data("dt_params", params);
            $("#dynamic-table").DataTable().ajax.reload();
        });
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-products'])

@endsection
