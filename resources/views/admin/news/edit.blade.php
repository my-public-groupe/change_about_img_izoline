@extends('admin.body')
@section('title', 'Блог')


@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{ URL::to('admin/news') }}">Блог</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование статьи @isset($data)"{{$data->name}}"@endisset</small>
        </h1>
    </div>

@if(!isset($data))
{{ Form::open(['url' => 'admin/news', 'class' => 'form-horizontal']) }}
@else
{{ Form::open(['url' => 'admin/news/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
@endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <!--
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-yellow btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить и закрыть</button>
            </div>-->
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>

                        <div class="btn btn-link">
                            <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                             Изменен: {{ $data->updated_at }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#name_en" aria-expanded="false">EN</a>
                    </li>
                    <div class="center"> <span class="label label-xlg label-purple">Заголовок</span></div>
                </ul>

                <div class="tab-content">
                    <div id="name_ru" class="tab-pane active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_en" class="tab-pane">
                        {{ Form::textarea('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at) ? date('d-m-Y', strtotime($data->created_at)) : old('date', \Illuminate\Support\Facades\Date::now()->format('d-m-Y'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                 {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right showTip L1']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('category_id', 'Категория', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('category_id[]', $categories, $news_categories, ['class' => 'chosencat col-sm-11 col-xs-12', 'multiple' => true]) }}
                </div>
            </div>

            <div class="form-group showTip L_sort">
                {{ Form::label('sort', 'Сортировка', ['class'=>'showTip L_sort col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::number('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'showTip L_sort col-sm-11 col-xs-12')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#short_ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>

                    <li>
                        <a href="#short_ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <li>
                        <a href="#short_en" data-toggle="tab" aria-expanded="false">EN</a>
                    </li>

                    <div class="center"> <span class="label label-xlg label-purple">Краткий текст</span></div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="short_ru">
                        {{ Form::textarea('description_short[ru]', (isset($data->description_short) ? $data->description_short : old('description_short')), array('style'=>'width:100%', 'rows'=>'3')) }}
                    </div>
                    <div class="tab-pane" id="short_ro">
                        {{ Form::textarea('description_short[ro]', (isset($data->description_short_ro) ? $data->description_short_ro : old('description_short_ro')), array('style'=>'width:100%', 'rows'=>'3')) }}
                    </div>
                    <div class="tab-pane" id="short_en">
                        {{ Form::textarea('description_short[en]', (isset($data->description_short_en) ? $data->description_short_en : old('description_short_en')), array('style'=>'width:100%', 'rows'=>'3')) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#newsCover" data-toggle="tab">Превью (обложка)</a>
            </li>
            <li>
                <a href="#newsPreview" data-toggle="tab">Превью (карточка)</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">
             <div class="tab-pane active" id="ru">
                 <div class="tabbable tabs-left">
                     <ul id="myTab" class="nav nav-tabs">
                         <li class="active">
                             <a href="#descRu" data-toggle="tab">Описание RU</a>
                         </li>
                         <li>
                             <a href="#descRo" data-toggle="tab">Описание RO</a>
                         </li>
                         <li>
                             <a href="#descEn" data-toggle="tab">Описание EN</a>
                         </li>
                     </ul>

                     <div class="tab-content">
                         <div class="tab-pane in active" id="descRu">
                             {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor')) }}
                         </div>
                         <div class="tab-pane" id="descRo">
                             {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
                         </div>
                         <div class="tab-pane" id="descEn">
                             {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
                         </div>
                     </div>

                 </div>
             </div>
            @include('admin.partials.meta')

            @include('admin.partials.photos', ['label_text' => 'Рекомендуемый размер изображения - 2280x900', 'table' => 'news_cover', 'div_id' => 'newsCover', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'showTip L8'])

            @include('admin.partials.photos', ['label_text' => 'Фото будут автоматически обрезаны до пропорций 1:1', 'table' => 'news_preview', 'div_id' => 'newsPreview', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'showTip L8'])
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

{{ Form::close() }}
@endsection

@section('styles')

<style>
    div#tipDiv {
    font-size:12px; line-height:1.2; letter-spacing: .2px;
    color:#000; background-color:white; padding: 2px;

    padding:4px;
    width:320px;
    box-shadow: 0 1px 5px 0 rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12);
}
</style>

@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug', ['input_names' => 'name[ru],name[ro],name[en]', 'slugs' => 'slug'])

    @include('admin.partials.chosen')
<script type="text/javascript">

dw_Tooltip.defaultProps = {
}
dw_Tooltip.content_vars = {
    L1: '<b>URL</b> новости должен быть латинскими маленькими символами и вместо пробела должен быть симол нижего подчеркивания <b>"_"</b>',
    L_sort: 'Порядок отображения товара на сайте. Наименьшие значения отображаются первыми в списке.',
    L10: '<b>Title</b> это название страницы, которое отображается в самом <b>верхнем поле</b> браузера. Также содержание title отображается в выдаче поисковых систем по запросам пользователей',
    L11: '<b>Meta Description</b> должен содержать <b>краткое описание</b> страницы. Достаточно добавить одно-два небольших предложения, в которых указать о чём и для кого эта страница',
    L12: '<b>Meta Keywords</b> - Те слова, которые наиболее полно характеризуют содержимое страницы и будут для нее ключевыми. Это могут быть как <b>отдельные слова</b>, так и <b>словосочетания</b>, но они <b>обязательно</b> должны встречаться в тексте на странице.',

}

</script>

@endsection
