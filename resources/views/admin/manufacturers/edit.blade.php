@extends('admin.body')
@section('title', 'Бренды')


@section('centerbox')
    <div class="page-header">
        <h1><a href="{{ URL::to('admin/manufacturers') }}">Бренды</a> <small><i
                    class="ace-icon fa fa-angle-double-right"></i> Редактирование </small></h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/manufacturers', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/manufacturers/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="false">RU</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Наименование</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="name_ru" class="tab-pane active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" class="form-control"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at)) ? $data->created_at : Date::now()->format('Y-m-d') }}"/>
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right showTip L1']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>
            <div class="form-group hide">
                {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::number('sort', (!empty($data->sort) ? $data->sort : 0), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        @include('admin.partials.photos', ['table' => 'manufacturers', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'active'])

        @include('admin.partials.meta')
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('styles')

    <style>
        div#tipDiv {
            font-size: 12px;
            line-height: 1.2;
            letter-spacing: .2px;
            color: #000;
            background-color: white;
            padding: 2px;

            padding: 4px;
            width: 320px;
            box-shadow: 0 1px 5px 0 rgba(0, 0, 0, .2), 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .12);
        }
    </style>

@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug', ['input_names' => 'name[ru]', 'slugs' => 'slug'])

    @include('admin.partials.datepicker')

    <script type="text/javascript">

        dw_Tooltip.defaultProps = {}
        dw_Tooltip.content_vars = {
            L1: '<b>URL</b> бренда должен быть латинскими маленькими символами и вместо пробела должен быть симол нижего подчеркивания <b>"_"</b>',

            L8: 'Изображение бренда должно быть с минимальной шириной в <b>500 px</b>',

            L10: '<b>Title</b> это название страницы, которое отображается в самом <b>верхнем поле</b> браузера. Также содержание title отображается в выдаче поисковых систем по запросам пользователей',
            L11: '<b>Meta Description</b> должен содержать <b>краткое описание</b> страницы. Достаточно добавить одно-два небольших предложения, в которых указать о чём и для кого эта страница',
            L12: '<b>Meta Keywords</b> - Те слова, которые наиболее полно характеризуют содержимое страницы и будут для нее ключевыми. Это могут быть как <b>отдельные слова</b>, так и <b>словосочетания</b>, но они <b>обязательно</b> должны встречаться в тексте на странице.',

        }

    </script>

@endsection
