@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{route('admin.lists.index')}}">Справочники</a></small>
            @if(isset($data))
                @foreach($data->getPaths() as $path)
                    <small class="@if(count($data->getPaths()) > 1)multiple-paths @endif">
                        @if(isset($path['slug']))
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <a href="{{$path['slug']}}">{{ $path['name'] }}</a>
                        @else
                            <i class="ace-icon fa fa-angle-double-right"></i> {{ $path['name'] }}
                        @endif
                    </small>
                @endforeach
            @else
                <small class="multiple-paths"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование справочника</small>
            @endif
        </h1>
    </div>


    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="false">RU</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">

            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" class="form-control date-picker"
                               data-date-format="yyyy-mm-dd"
                               value="{{ (isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : old('date', Date::now()->format('Y-m-d'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class'=>'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="row">

        <div class="col-xs-12">
            <div class="col-xs-6 col-xs-offset-6">

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="tabbable">
                            <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                                <li class="active">
                                    <a href="#short_en" data-toggle="tab" aria-expanded="false">EN</a>
                                </li>

                                <div class="center"> <span class="label label-xlg label-purple">Краткий текст</span></div>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane in active" id="short_en">
                                    {{ Form::textarea('description_short[en]', (isset($data->description_short_en) ? $data->description_short_en : old('description_short_en')), array('style'=>'width:100%', 'rows'=>'3')) }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>

            <li>
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>

            <li class="hide">
                <a href="#params" data-toggle="tab">Параметры</a>
            </li>

            <li class="hide">
                <a href="#meta" data-toggle="tab">META</a>
            </li>

        </ul>


    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="ru">

            <div class="tabbable  tabs-left">

                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#descEn" data-toggle="tab">EN</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="descEn">
                        {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => 'description_en')) }}
                    </div>

                </div>

            </div>
        </div>

        <div class="tab-pane hide" id="params">
            <div class="tab-content">
                <ul id="sortable">
                    @if (isset($data->params))
                        @foreach($data->params as $item)
                            @include('admin.lists.param_row', ['param' => $item])
                        @endforeach
                    @endif
                        @include('admin.lists.param_row')
                </ul>
                <a href="javascript:addItem();" class="btn btn-sm btn-info showTip L3">
                    <i class="ace-icon fa fa-plus-circle bigger-110"></i>
                    Добавить
                </a>
            </div>
        </div>

        @include('admin.partials.photos', ['table' => 'lists', 'table_id' => isset($data->id) ? $data->id : 0, 'crop' => [1350, 755]] )

        @include('admin.partials.meta')

    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.slug',['input_names' => 'name[ru]', 'slugs' => 'slug'])

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

@endsection
