@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{route('admin.lists.index')}}">Справочники</a></small>
            @if(isset($data))
                @foreach($data->getPaths() as $path)
                    <small class="@if(count($data->getPaths()) > 1)multiple-paths @endif">
                        @if(isset($path['slug']))
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <a href="{{$path['slug']}}">{{ $path['name'] }}</a>
                        @else
                            <i class="ace-icon fa fa-angle-double-right"></i> {{ $path['name'] }}
                        @endif
                    </small>
                @endforeach
            @else
                <small class="multiple-paths"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование видеоматериала</small>
            @endif
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <input type="hidden" name="parent_id" value="9">

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive"><i
                        class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить
                </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#name-ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#name-ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Название</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="name-ru">
                        {{ Form::text('name[ru]', (isset($data) ? $data->name : old('name')), array('style'=>'width:100%')) }}
                    </div>
                    <div class="tab-pane" id="name-ro">
                        {{ Form::text('name[ro]', (isset($data) ? $data->name_ro : old('name_ro')), array('style'=>'width:100%')) }}
                    </div>
                </div>
            </div>

            <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs padding-12 tab-color-blue background-blue">
                    <li class="active">
                        <a href="#description-short-ru" data-toggle="tab" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a href="#description-short-ro" data-toggle="tab" aria-expanded="false">RO</a>
                    </li>

                    <div class="flex justify-content-around slider-link">
                        <div class="center"><span class="label label-xlg label-purple">Описание</span></div>
                    </div>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane in active" id="description-short-ru">
                        {{ Form::textarea('description_short', (isset($data) ? $data->description_short : old('description_short')), array('style'=>'width:100%', 'rows' => 4)) }}
                    </div>
                    <div class="tab-pane" id="description-short-ro">
                        {{ Form::textarea('description_short[ro]', (isset($data) ? $data->description_short_ro : old('description_short_ro')), array('style'=>'width:100%', 'rows' => 4)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('reserve', 'Категория', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('reserve', ['' => '--- Выберите категорию ---'] + $categories, $data->reserve ?? null, ['class' => 'col-sm-11 col-xs-12', 'required' => true]) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('video_link', 'Ссылка на видео YouTube', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[video_link]', (isset($data->params['video_link']) ? $data->params['video_link'] : old('video_link')), array('style'=>'width:100%')) }}
                </div>
            </div>

            <div class="form-group hide">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class'=>'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>
            <div class="form-group hide">
                <div class="tabbable">
                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                        <li class="active">
                            <a data-toggle="tab" href="#slug_ru" aria-expanded="true">RU</a>
                        </li>
                        <div class="center">
                            <span class="label label-xlg label-purple">URL</span>
                        </div>
                    </ul>
                    <div class="tab-content">
                        <div id="slug_ru" class="tab-pane active">
                            {{ Form::textarea('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'form-control', 'rows' => 3, 'cols' => 50)) }}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="row">

        <div class="col-xs-12">
            <div class="col-xs-6 col-xs-offset-6">


            </div>
        </div>
    </div>

    <hr>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        @include('admin.partials.meta', ['active' => true])
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.slug',['type' => 'input', 'input_names'=>'name[ru]','slugs'=>'slug'])
@endsection
