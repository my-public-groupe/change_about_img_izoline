<div class="form-group main-valute-container">
    <label class="control-label bolder blue">Основная валюта</label>
    <div class="radio">
        <label>
            <input id="main_currency_mdl" name="main_currency" type="radio" class="ace" @if(isset($data) && $data->main_currency == 0) checked @endif value="0">
            <span class="lbl"> MDL</span>
        </label>
    </div>

    <div class="radio">
        <label>
            <input id="main_currency_usd" name="main_currency" type="radio" class="ace" @if(isset($data) && $data->main_currency == 1) checked @endif value="1">
            <span class="lbl"> USD</span>
        </label>
    </div>

    <div class="radio">
        <label>
            <input id="main_currency_eur" name="main_currency" type="radio" class="ace" @if(isset($data) && $data->main_currency == 2) checked @endif value="2">
            <span class="lbl"> EUR</span>
        </label>
    </div>
</div>
<script>
    $(function () {
        @if(isset($data) && $data->main_currency == 0)
            $("input[name=price]").attr('disabled', false);

            $("input[name=price_usd]").attr('disabled', true);
            $("input[name=price_eur]").attr('disabled', true);
        @endif
        @if(isset($data) && $data->main_currency == 1)
            $("input[name=price_usd]").attr('disabled', false);

            $("input[name=price]").attr('disabled', true);
            $("input[name=price_eur]").attr('disabled', true);
        @endif
        @if(isset($data) && $data->main_currency == 2)
            $("input[name=price_eur]").attr('disabled', false);

            $("input[name=price]").attr('disabled', true);
            $("input[name=price_usd]").attr('disabled', true);
        @endif
    });

    $("#main_currency_mdl").change(function () {
        $("input[name=price]").attr('disabled', false);

        $("input[name=price_usd]").attr('disabled', true);
        $("input[name=price_eur]").attr('disabled', true);
    });
    $("#main_currency_usd").change(function () {
        $("input[name=price_usd]").attr('disabled', false);

        $("input[name=price]").attr('disabled', true);
        $("input[name=price_eur]").attr('disabled', true);
    });
    $("#main_currency_eur").change(function () {
        $("input[name=price_eur]").attr('disabled', false);

        $("input[name=price]").attr('disabled', true);
        $("input[name=price_usd]").attr('disabled', true);
    });
</script>
<style>
    .main-valute-container .radio {
        display: inline-block;
    }
    .main-valute-container label.control-label{
        margin-left: 65px;
    }
</style>
