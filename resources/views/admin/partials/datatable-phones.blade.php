@if(!empty($phone_1_raw))
    <a href="tel:{{ $phone_1 }}">{{ $phone_1_raw }}</a>
@endif
@if(!empty($phone_2_raw))
    <br>
    <a href="tel:{{ $phone_2 }}">{{ $phone_2_raw['phone'] }}</a>
    @if(!empty($phone_2_raw['comment']))
        ({{ $phone_2_raw['comment'] }})
    @endif
@endif
@if(!empty($item->telegram))
    <br>
    <a target="_blank" href="{{ $item->getTelegramLink() }}"><i class="ace-icon fa fa-telegram blue"></i> Telegram</a>
@endif
@if(!empty($address))
    <br>
    <i class="ace-icon fa fa-map-marker"></i> {{ $address }}
@endif
