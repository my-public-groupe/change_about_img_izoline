<div class="tab-pane" id="category_params">
    <div class="row">
        <div class="col-sm-12">
            {{ Form::select('parameters[]', $parameters, $category_parameters ?? [], ['class'=>'chosencat form-control col-sm-11 col-xs-12', 'multiple' => true]) }}
        </div>
    </div>
</div>
