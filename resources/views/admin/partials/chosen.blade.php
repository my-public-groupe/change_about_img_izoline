{!! HTML::style('ace/assets/css/chosen.css?v=xsort') !!}
{!! HTML::script('ace/assets/js/chosen.jquery.js?v=xsort') !!}
<script>
    if ($("#chosencat").css('display') != 'none') $("#chosencat").chosen({
        placeholder_text_multiple: "Начните вводить название",
        placeholder_text_single: "Начните вводить название",
        no_results_text: "Не найдено ничего"
    });
    $(".chosencat").each(function () {
        if ($(this).css('display') != 'none') {

            var options = {
                width: '100%',
                placeholder_text_multiple: "Начните вводить название",
                placeholder_text_single: "Начните вводить название",
                no_results_text: "Не найдено ничего"
            }

            if($(this).attr('id') == 'desserts'){
                options.allow_duplicates = true;
            }

            $(this).chosen(options);
        }
    });
</script>
