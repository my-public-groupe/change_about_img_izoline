<table>
    <thead>
    <tr>
        <th align="center">Email</th>
        <th align="center">Имя</th>
        <th align="center">Телефон</th>
        <th align="center">Фирма</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $item)
        <tr>
            <td class="center">{{ $item->email }}</td>
            <td class="center">{{ $item->name }}</td>
            <td class="center">{{ $item->getPhone() }}</td>
            <td class="center">{{ $item->params['company'] ?? '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
