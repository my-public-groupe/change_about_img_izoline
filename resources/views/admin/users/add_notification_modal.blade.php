<div class="modal modal-fullscreen fade" id="addNotificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Редактирование оповещений</h4>
            </div>
            <form method="post" action="{{ route('admin.add-user-notification') }}">
                <input type="hidden" name="user_id" value="{{ $data->id }}">
                <input type="hidden" name="celebration_id">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="control-group">
                                    <label class="control-label bolder blue">Напоминать о событии:</label>

                                    @foreach(config('custom.events_remind') as $value => $description)
                                        <div class="checkbox">
                                            <label class="block">
                                                <input name="remind[]" value="{{$value}}" type="checkbox" class="ace input-lg remind-input">
                                                <span class="lbl bigger-120"> {{ $description }}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
