<div class="modal modal-fullscreen fade" id="addEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Новое событие</h4>
            </div>
            <form method="post" action="{{ route('admin.add-user-event') }}">
                <input type="hidden" name="user_id" value="{{ $data->id }}">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label><b>Дата</b></label>
                                    <input type="text" name="date" class="date-picker form-control"
                                           data-date-format="dd.mm.yyyy" required>
                                </div>
                                <div class="form-group">
                                    <label><b>Описание</b></label>
                                    <textarea name="description" class="form-control" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label><b>Повторяется ли событие</b></label>
                                    <select name="repeat" class="form-control">
                                        @foreach(config('custom.events_repeat') as $key => $er)
                                            <option value="{{ $key }}" @if($key == "yearly")selected @endif>{{ $er }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Создать</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>
