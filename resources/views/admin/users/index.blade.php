@extends('admin.body')
@section('title', 'Пользователи')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Пользователи</small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <a class="btn btn-success create-users-mobile" href="{{ URL::to('admin/users/create') }}">
                <i class="ace-icon fa fa-plus-square-o bigger-120"></i>
                Создать
            </a>
            <a class="btn btn-warning hidden-xs hidden-sm" href="{{ URL::to('admin/users/celebrations') }}">
                <i class="ace-icon fa fa-bell bigger-120"></i>
                События всех пользователей
            </a>
            <a class="btn btn-warning celebrations-users-mobile hidden-md hidden-lg" href="{{ URL::to('admin/users/celebrations') }}">
                <i class="ace-icon fa fa-bell bigger-120"></i>
                События
            </a>

            <div class="filters">
                <label class="control-label no-padding-right">Роль:</label>
                <select name="rights" id="filter-role">
                    <option value="">Все</option>
                    <option value="0" @if(request()->rights === '0') selected @endif>Пользователь</option>
                    <option value="2" @if(request()->rights === '2') selected @endif>Партнер</option>
                    <option value="1" @if(request()->rights === '1') selected @endif>Администратор</option>
                </select>
            </div>

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div class="table-header hidden-xs hidden-sm"> Список пользователей</div>

            <table id="dynamic-table" class="table table-users table-striped table-bordered table-responsive table-hover dataTable">
                <thead>
                <tr>
                    <th align="center" class="all">ID</th>
                    <th align="center" class="all">ФИО</th>
                    <th align="center" class="all">Роль</th>
                    <th align="center" class="desktop">Email</th>
                    <th align="center" class="all">Телефон и адрес</th>
                    <th align="center" class="desktop">Создан</th>
                    <th align="center" class="desktop never"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@endsection

@section('styles')
    {{HTML::style('ace/assets/css/dataTables.responsive.css')}}
@endsection

@section('scripts')

    <script>
        var columns = [
            {data: 'id', className: 'center'},
            {data: 'name', className: 'center'},
            {
                data: 'rights',
                "render": {
                    "display": function (data, type, full) {
                        if (data == 0) {
                            return `<i class='ace-icon fa fa-user bigger-120 grey' title="Пользователь"></i>`;
                        } else if (data == 1) {
                            return `<i class='ace-icon fa fa-star bigger-120 red' title="Админ"></i>`;
                        } else if (data == 2) {
                            return `<i class='ace-icon fa fa-handshake-o bigger-120 purple' title="Партнер"></i>`;
                        }
                    }
                },
                className: 'center'
            },
            {
                data: 'email',
                "render":{
                    "display": function (data, type, full){
                        return `${data}<div class='mobile-actions abs'>${full.actions}</div>`;
                    }
                },
                className: 'center'
            },
            {data: 'phones_address', className: 'center'},
            {data: 'created_at', className: 'center'},
            {data: 'actions', className: 'center'}
        ];

        $('#dynamic-table').on('preXhr.dt', function (e, settings, data) {
            data['rights'] = $('select[name=rights]').val();
        });

        $("#filter-role").change(function () {
            let params = {};
            params['rights'] = $('select[name=rights]').val();

            $("#dynamic-table").data("dt_params", params);
            $("#dynamic-table").DataTable().ajax.reload();
        });
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-users'])

    <script>
        $('#dynamic-table').on('click', '.mobile-actions .visibility-btn', function(e) {
            var id      =  $(this).closest('.mobile-actions').find('.action-buttons').data('id');
            var model   =  $(this).closest('.mobile-actions').find('.action-buttons').data('model');
            var $thisdiv = $(this);
            $.get("admin/json/changevisibility", {'id': id, 'model': model}, function(data) {
                if ($thisdiv.hasClass('visible')) {
                    $thisdiv.attr('class', 'visibility-btn unvisible');
                } else {
                    $thisdiv.attr('class', 'visibility-btn visible');
                }
            });
        });

        $('#dynamic-table').on('click', '.mobile-actions .delete-btn', function (e){
            if (!confirm("Удалить?")) {
                return false;
            }
            var id      =  $(this).closest('.action-buttons').data('id');
            var model   =  $(this).closest('.action-buttons').data('model');
            var $thisdiv = $(this);

            $.get("admin/json/delete", {'id': id, 'model': model}, function(data) {
                $thisdiv.closest('tr').prev().remove();
                $thisdiv.closest('tr').remove();
            });
        });
    </script>
@endsection
