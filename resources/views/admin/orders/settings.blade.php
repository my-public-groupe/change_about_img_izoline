@extends('admin.body')
@section('title', 'Настройки заказов')
@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="{{ URL::to('admin/orders') }}">Заказы</a><small><i
                    class="ace-icon fa fa-angle-double-right"></i></small>
            <small>Настройки</small>
        </h1>
    </div>
    <form method="POST" action="{{route('admin.save-orders-settings')}}" id="saveOrderSettingsFrm">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('manager_email', 'Email менеджера', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                    <div class="col-sm-9">
                        {{ Form::text('manager_email', $manager_email, array('class' => 'col-sm-9 col-xs-12')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" class="btn btn-success btnSaveForm">Сохранить</button>
        </div>
    </form>
@endsection
@section('scripts')
    <script>
        $(function () {
            $(".btnSaveForm").click(function (){
                $("#saveOrderSettingsFrm").submit();
            });
        });
    </script>
@endsection
