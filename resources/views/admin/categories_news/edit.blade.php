@extends('admin.body')
@section('title', 'Категория')

@section('centerbox')
    <div class="page-header">
        <h1>
            <a href="/admin"><i class="ace-icon fa fa-home"></i></a>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> <a href="{{ URL::to('admin/categories_news') }}">Категории (блог)</a></small>
            <small class="no-margin"><i class="ace-icon fa fa-angle-double-right"></i> Редактирование категории @isset($data)"{{$data->name}}"@endisset</small>
        </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/categories_news', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/categories_news/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-2 hide">
                <label>
                    {{ Form::checkbox('top',  1, (isset($data) && $data->top == 1 ? true : false), ['class' => 'ace']) }}
                    <span class="lbl showTip L7"> На главную </span>
                </label>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="tabbable">
                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab1">
                    <li class="active">
                        <a data-toggle="tab" href="#name_ru" aria-expanded="true">RU</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#name_ro" aria-expanded="false">RO</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#name_en" aria-expanded="false">EN</a>
                    </li>
                    <div class="center">
                        <span class="label label-xlg label-purple">Название</span>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="name_ru" class="tab-pane active">
                        {{ Form::textarea('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'form-control name_ru', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_ro" class="tab-pane">
                        {{ Form::textarea('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'form-control name_ro', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                    <div id="name_en" class="tab-pane">
                        {{ Form::textarea('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'form-control name_en', 'rows' => 3, 'cols' => 50)) }}
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="yyyy-mm-dd"
                               value="{{ (isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : old('date', date('Y-m-d'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>

            <div class="form-group showTip L13 hide">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}

                <div class="col-sm-9 ">
                    @if(isset($parents))
                        {{ Form::select('parent[]', [0 => 'не выбран'] + $categories, $parents, ['id' => 'chosencat', 'class' => 'tag-input-style col-sm-11 control-label no-padding-right']) }}
                    @else
                        {{ Form::select('parent[]', [0 => 'не выбран'] + $categories, '', ['id' => 'chosencat', 'class' => 'tag-input-style col-sm-11 control-label no-padding-right']) }}
                    @endif
                </div>
            </div>
            <div class="form-group hide">
                {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'col-sm-11 col-xs-12 showTip L1')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="space"></div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('styles')
    {!! HTML::style('ace/assets/css/datepicker.css') !!}
    {!! HTML::style('ace/assets/css/chosen.css') !!}
@append

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug', ['input_names' => 'name[ru],name[ro],name[en]', 'slugs' => 'slug'])

    @include('admin.partials.datepicker')

    @include('admin.partials.chosen')

@append
