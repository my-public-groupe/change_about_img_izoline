@extends('admin.body')
@section('title', 'Главная')

@section('centerbox')
    <div class="page-header">
        <h1> Главная </h1>
    </div>

    <div class="alert alert-success">
        <button class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
        Здравствуйте, {{ Auth::user()->name }}! Управление сайтом в панели слева.
    </div>

    <div class="row main-page">
        <div class="col-xs-12">
            <h3 class="header smaller lighter green"></h3>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/lists?id=1') }}" class="btn btn-purple btn-app radius-4">
                        <i class="ace-icon fa fa-image bigger-230"></i>
                        Слайдер
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/lists?id=9') }}" class="btn btn-purple btn-app radius-4">
                        <i class="ace-icon fa fa-video-camera bigger-230"></i>
                        Видеоматериалы
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/lists') }}" class="btn btn-purple btn-app radius-4">
                        <i class="ace-icon fa fa-list-alt bigger-230"></i>
                        Справочники
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/translations') }}" class="btn btn-purple btn-app radius-4">
                        <i class="ace-icon fa fa-globe bigger-230"></i>
                        Переводы
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/products') }}" class="btn btn-info btn-app radius-4">
                        <i class="ace-icon fa fa-gift bigger-230"></i>
                        Товары
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/categories') }}" class="btn btn-info btn-app radius-4">
                        <i class="ace-icon fa fa-list-alt bigger-230"></i>
                        Категории
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/manufacturers') }}" class="btn btn-info btn-app radius-4">
                        <i class="ace-icon fa fa-list-alt bigger-230"></i>
                        Бренды
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/parameters') }}" class="btn btn-info btn-app radius-4">
                        <i class="ace-icon fa fa-cubes bigger-230"></i>
                        Параметры
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/content') }}" class="btn btn-warning btn-app radius-4">
                        <i class="ace-icon fa fa-file-text-o bigger-230"></i>
                        Страницы
                    </a>
                </div>
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 center">
                    <a href="{{ url('admin/orders') }}" class="btn btn-danger btn-app radius-4">
                        <i class="ace-icon fa fa-shopping-cart bigger-230"></i>
                        Заказы
                    </a>
                </div>
            </div>
            <div class="row">

            </div>
            <div class="space"></div>
        </div><!-- /.col-xs-12 -->
    </div>
@stop
