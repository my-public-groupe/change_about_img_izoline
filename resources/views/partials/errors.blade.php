@if (count($errors) > 0)
    <div class="alert alert-danger container top-buffer" style="margin: 0;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
