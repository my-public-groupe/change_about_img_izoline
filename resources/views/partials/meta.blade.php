@if($data?->meta?->title != "")
    @section('title', $data->meta->title)
@endif
@if($data?->meta?->meta_keywords != "")
    @section('meta_keywords', $data->meta->meta_keywords)
@endif
@if($data?->meta?->meta_description != "")
    @section('meta_description', $data->meta->meta_description)
@endif
