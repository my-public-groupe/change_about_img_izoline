<!-- Ask form -->
<div class="modal fade" id="askFrmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('common.success')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('common.ask_form_sent')</p>
            </div>
        </div>
    </div>
</div>

<!-- Contact form -->
<div class="modal fade" id="contactFrmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">@lang('common.success')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('common.contact_form_sent')</p>
            </div>
        </div>
    </div>
</div>

<!-- Cheap form -->
@if(isset($product) && Route::is('catalog.get-product'))
    <div class="modal fade" id="cheapFrmModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog custom-modal">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="text-center border-color-1 border-bottom mb-3 row">
                        <div class="col-md-3"></div>
                        <div class="col-9 col-md-6">
                            <h4 class="text-left text-md-center">@lang('common.found_cheaper')</h4>
                        </div>
                        <div class="col-3 d-flex justify-content-end align-items-center">
                            <button type="button" class="close mb-2" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="font-size-14">@lang('common.close')</span>
                            </button>
                        </div>
                    </div>
                    <div class="text-justify font-size-15 pb-3 border-color-1 border-bottom mb-3">
                        @lang('common.found_cheaper_text')
                    </div>
                    <div class="row pb-3 border-color-1 border-bottom mb-3">
                        <div class="col-3 max-height-130">
                            <img class="img-fluid"
                                 src="{{ $product->mainphoto() }}" alt="{{ $product->name }}">
                        </div>
                        <div class="col-9 d-flex flex-column justify-content-between">
                            <div class="text-blue">{{ $product->name }}</div>
                        </div>
                    </div>
                    <form id="cheapFrm">
                        <input type="hidden" name="referer" value="{{ url()->current() }}">

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="name" class="col-form-label">@lang('common.name')<span
                                        class="text-danger">*</span></label>
                                <input type="text" name="name" class="form-control height-35" id="name" required>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="phone" class="col-form-label">@lang('common.phone')<span
                                        class="text-danger">*</span></label>
                                <input type="text" name="phone" class="form-control height-35" id="phone" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-9 ">
                                <label for="shop" class="col-form-label">@lang('common.product_shop_link') <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="shop_link" class="form-control height-35" id="shop" required>
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="price" class="col-form-label">@lang('common.price') <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="price" class="form-control height-35" id="price">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-12 mb-2">
                                <label for="message" class="col-form-label">@lang('common.comment')</label>
                                <textarea class="form-control" name="comment" id="message" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-dark">@lang('common.send')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif


<!-- Consultation form -->
<div class="modal fade" id="consultationFrmModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog custom-modal">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="text-center border-color-1 border-bottom mb-3">
                    <h4>@lang('common.consultation')</h4>
                </div>
                <form id="consultationFrm">
                    <input type="hidden" name="referer" value="{{ url()->current() }}">

                    <div class="row">
                        <div class="col-6">
                            <label for="name" class="col-form-label">@lang('common.name')<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control height-35" id="name" required>
                        </div>
                        <div class="col-6">
                            <label for="phone" class="col-form-label">@lang('common.phone')<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="phone" class="form-control height-35" id="phone" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <label for="price" class="col-form-label">Email</label>
                            <input type="text" name="email" class="form-control height-35" id="price">
                        </div>
                    </div>
                    <div class="row m-3">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-dark">@lang('common.send')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Checkout managere form -->
<div class="modal fade" id="checkoutManagerFrm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog custom-modal">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="text-center border-color-1 border-bottom mb-3">
                    <h4>@lang('common.mail_checkout_manager')</h4>
                </div>
                <form id="consultationFrm" method="post" action="{{ route('cart.checkout-manager') }}">
                    <div class="row">
                        <div class="col-12">
                            <label for="name" class="col-form-label">@lang('common.name')<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control height-35" id="name" required>
                        </div>
                        <div class="col-12">
                            <label for="phone" class="col-form-label">@lang('common.phone')<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="phone" class="form-control height-35" id="phone" required>
                        </div>
                    </div>

                    <div class="row m-3">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-dark">@lang('common.send')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
