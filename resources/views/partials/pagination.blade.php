@if ($paginator->hasPages())
    <div class="product-pagination d-flex justify-content-center">
        <div class="theme-paggination-block">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                @if ($paginator->onFirstPage())
                                    <li aria-disabled="true" aria-label="« @lang('pagination.previous')"
                                        class="page-item disabled">
                                        <a aria-hidden="true" class="page-link">‹</a>
                                    </li>
                                @else
                                    <li aria-disabled="true" aria-label="« @lang('pagination.previous')"
                                        class="page-item disabled">
                                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev">
                                            <span aria-hidden="true" class="page-link">‹</span>
                                        </a>
                                    </li>
                                @endif

                                {{-- Pagination Elements --}}
                                @foreach ($elements as $element)
                                    {{-- "Three Dots" Separator --}}
                                    @if (is_string($element))
                                        <li aria-disabled="true" class="page-item disabled">
                                            <span class="page-link">...</span>
                                        </li>
                                    @endif

                                    {{-- Array Of Links --}}
                                    @if (is_array($element))
                                        @foreach ($element as $page => $url)
                                            @if ($page == $paginator->currentPage())
                                                <li aria-current="page" class="page-item active">
                                                    <a class="page-link">{{ $page }}</a>
                                                </li>
                                            @else
                                                <li class="page-item">
                                                    <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach

                                {{-- Next Page Link --}}
                                @if ($paginator->hasMorePages())
                                    <li class="page-item">
                                        <a href="{{ $paginator->nextPageUrl() }}" rel="next"
                                           aria-label="@lang('pagination.next')"
                                           class="page-link">›</a>
                                    </li>
                                @else
                                    <li aria-disabled="true" class="page-item disabled">
                                        <span class="page-link">...</span>
                                    </li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
