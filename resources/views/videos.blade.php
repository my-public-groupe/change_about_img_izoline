@extends('body')
@if(isset($video_category))
    @include('partials.meta', ['data' => $video_category])
@endif
@section('centerbox')
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a
                                href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">@lang('common.helpful_videos')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="row">
            <div class="col-xl-9">
                @if($videos->isNotEmpty())
                    <div class="max-width-1100-wd">
                        @foreach($videos as $video)
                            @if(empty($video->getVideoId())) @continue @endif

                            <article class="card mb-13 border-0">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video->getVideoId() }}"
                                            allow="autoplay; fullscreen" allowfullscreen></iframe>
                                </div>
                                <div class="card-body pt-5 pb-0 px-0">
                                    <h4 class="mb-3">{{ $video->name }}</h4>
                                    <p>{{ $video->description_short }}</p>
                                </div>
                            </article>
                        @endforeach
                    </div>
                @endif
            </div>
            <div class="col-xl-3">
                <aside class="mb-7">
                    <div class="border-bottom border-color-1 mb-5">
                        <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">@lang('common.about_videos')</h3>
                    </div>
                    <p class="text-gray-90 mb-0">@lang('common.about_videos_text')</p>
                </aside>

                <aside class="mb-7">
                    @if($video_categories->isNotEmpty())
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">@lang('common.categories')</h3>
                        </div>
                        <div class="list-group video-categories">
                            @foreach($video_categories as $name => $slug)
                                <a href="{{ route('get-videos', $slug) }}"
                                   class="@if($slug == $category_slug)selected @endif font-bold-on-hover px-3 py-2 list-group-item list-group-item-action border-0">
                                    <i class="mr-2 fas fa-angle-right"></i>
                                    {{ $name }}
                                </a>
                            @endforeach
                        </div>
                    @endif
                </aside>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        .video-categories a.selected{
            font-weight: 700;
        }
    </style>
@endsection
