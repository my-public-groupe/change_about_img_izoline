@extends('body')
@section('centerbox')
    <div class="page-banner-section section">
        <div class="container">
            <div class="row">
                <div class="page-banner-content col">
                    <h1 class="search-result-heading">@lang('common.search_result', ['searchword' => session('searchword')])</h1>

                    @if($products->isNotEmpty())
                        <p>@lang('common.search_total', ['search_total' => $totalProducts])</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 mb-40 search-container">
                @if($products->isNotEmpty())
                    <ul class="row list-unstyled products-group no-gutters mb-6">
                        @foreach($products as $product)
                            <li class="col-12 col-sm-6 col-md-4 product-item">
                                @include('partials.product-box-grid')
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="row list-unstyled products-group no-gutters mb-3">
                        <h6>@lang('common.no_products')</h6>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
