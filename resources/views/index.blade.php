@extends('body')
@section('centerbox')
    <!-- Slider Section -->
    @if(!empty($slider))
        <div class="mb-4">
            <div class="bg-img-hero" style="background-image: url({{ asset('img/slider-background.jpg') }});">
                <div class="container overflow-hidden">
                    <div class="js-slick-carousel u-slick"
                    data-pagi-classes="text-center position-absolute right-0 bottom-0 left-0 u-slick__pagination u-slick__pagination--long justify-content-center mb-3 mb-md-4">
                        @foreach($slider as $slide)
                            <div class="js-slide">
                                <div class="row pt-7 py-md-0">
                                    <div class="d-none d-wd-block offset-1"></div>
                                    <div class="col-12 col-md-6 mt-md-8 mt-lg-10">
                                        <div class="ml-xl-4">
                                            @if(!empty($slide->getHeading1()))
                                                <h1 class="font-size-46 text-lh-50 font-weight-light text-uppercase sm-font-size-36"
                                                data-scs-animation-in="fadeInUp"
                                                data-scs-animation-delay="200">
                                                    <strong class="font-weight-bold">{{ $slide->getHeading1() }}</strong>
                                                </h1>
                                            @endif

                                            @if(!empty($slide->getHeading2()))
                                                <h1 class="font-size-46 text-lh-50 font-weight-light mb-8 text-uppercase sm-font-size-36"
                                                    data-scs-animation-in="fadeInUp"
                                                    data-scs-animation-delay="200">
                                                    {{ $slide->getHeading2() }}
                                                </h1>
                                            @endif

                                            @if(!empty($slide->getBtnName()) && !empty($slide->getBtnLink()))
                                                <a href="{{ $slide->getBtnLink() }}" class="btn btn-primary transition-3d-hover rounded-lg font-weight-normal py-2 px-md-7 px-3 font-size-16"
                                                    data-scs-animation-in="fadeInUp"
                                                    data-scs-animation-delay="300">
                                                    {{ $slide->getBtnName() }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-wd-5 d-flex align-items-end ml-auto ml-md-0"
                                        data-scs-animation-in="fadeInRight"
                                        data-scs-animation-delay="500">
                                        <img class="img-fluid ml-auto mr-10 mr-wd-auto" src="{{ $slide->mainphoto() }}" alt="Slide Image">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!-- End Slider Section -->

    <div class="container">
        <div class="mb-6 d-flex flex-wrap justify-content-between">
            <div class="p-2 mx-auto">
                <div class="border w-250 p-2 text-center br-17">
                    <div class="d-flex justify-content-center">
                        <i class="text-primary ec ec-tag font-size-50"></i>
                    </div>
                    <div class="font-weight-bold text-dark font-size-16 mx-6 height-50">
                        @lang('common.features_title_1')
                    </div>
                    <div class="text-secondary font-size-12 height-75">
                        @lang('common.features_text_1')
                    </div>
                </div>
            </div>

            <div class="p-2 mx-auto">
                <div class="border w-250 p-2 text-center br-17">
                    <div class="d-flex justify-content-center">
                        <img src="/svg/components/competitive-price-1.svg" alt="Price">
                    </div>
                    <div class="font-weight-bold text-dark font-size-16 mx-6 height-50">
                        @lang('common.features_title_2')
                    </div>
                    <div class="text-secondary font-size-12 height-75">
                        @lang('common.features_text_2')
                    </div>
                </div>
            </div>

            <div class="p-2 mx-auto">
                <div class="border w-250 p-2 text-center br-17">
                    <div class="d-flex justify-content-center">
                        <img src="/svg/components/payment-delay-1.svg" alt="Payment">
                    </div>
                    <div class="font-weight-bold text-dark font-size-16 mx-6 height-50">
                        @lang('common.features_title_3')
                    </div>
                    <div class="text-secondary font-size-12 height-75">
                        @lang('common.features_text_3')
                    </div>
                </div>
            </div>

            <div class="p-2 mx-auto">
                <div class="border w-250 p-2 text-center br-17">
                    <div class="d-flex justify-content-center">
                        <i class="text-primary ec ec-transport font-size-50"></i>
                    </div>
                    <div class="font-weight-bold text-dark font-size-16 mx-6 height-50">
                        @lang('common.features_title_4')
                    </div>
                    <div class="text-secondary font-size-12 height-75">
                        @lang('common.features_text_4')
                    </div>
                </div>
            </div>

            <div class="p-2 mx-auto">
                <div class="border w-250 p-2 text-center br-17">
                    <div class="d-flex justify-content-center">
                        <i class="text-primary ec ec-returning font-size-50"></i>
                    </div>
                    <div class="font-weight-bold text-dark font-size-16 mx-6 height-50">
                        @lang('common.features_title_5')
                    </div>
                    <div class="text-secondary font-size-12 height-75">
                        @lang('common.features_text_5')
                    </div>
                </div>
            </div>
        </div>

        <!-- About Section -->
        <div class="bg-gray-1 px-8 py-2 mb-6">
            <div class="row">
                <div class="col-12 col-xl-8 pr-xl-5">
                    <h1 class="h1 font-weight-bold text-center">@lang('common.about_us')</h1>
                    <p>@lang('common.about-part-1')</p>
                    <p>@lang('common.about-part-2')</p>
                    <a class="" href=""></a>
                </div>
                <div class="d-none d-xl-block col-xl-4">
                    <div class="d-flex align-items-center h-100 w-100">
                        <div class="overflow-hidden br-17">
                            <img class="img-fluid" src="{{ asset('img/about.jpg') }}" alt="About">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Section end -->

        <!-- Categories Card -->
        @if($site_categories->isNotEmpty())
        <div class="mb-4">
            <div class="row d-block d-md-flex flex-nowrap flex-md-wrap overflow-auto overflow-md-visble justify-content-md-center">
                @foreach($site_categories as $category)
                    <div class="col-md-6 col-xl-4 col-12 mb-5 flex-shrink-0 flex-md-shrink-1">
                        <div class="bg-gray-1 overflow-hidden h-100 d-flex align-items-center">
                            <div class="pr-3 w-100">
                                <div class="d-flex align-items-center">
                                    <div class="width-122 height-122 flex-shrink-0">
                                        <a href="{{ route('catalog.get-parent-category', $category->slug) }}">
                                            <img alt="{{ $category->name }}" src="{{ $category->mainphoto() }}" style="width: 100%;">
                                        </a>
                                    </div>
                                    <div class="rounded w-60 ml-6">
                                        <a href="{{ route('catalog.get-parent-category', $category->slug) }}">
                                            <p class="font-size-18 font-weight-light mb-0"><strong>{{ $category->name }}</strong></p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endif
        <!-- End Categories Card -->

        <!-- FAQ Section -->

        @if($faq->isNotEmpty())
            <div class="mb-4 text-center">
                <h1 class="h2">@lang('common.answers')</h1>
            </div>
            <!-- Basics Accordion -->
            <div id="basicsAccordion" class="mb-12">
                @foreach($faq as $item)
                    <!-- Card -->
                    <div class="card mb-3 border-top-0 border-left-0 border-right-0 border-color-1 rounded-0">
                        <div class="card-header card-collapse bg-transparent-on-hover border-0" id="faq-{{$item->id}}">
                            <h5 class="mb-0">
                                <button type="button"
                                        class="px-0 btn btn-link btn-block d-flex justify-content-between card-btn collapsed py-3 font-size-18 border-0"
                                        data-toggle="collapse"
                                        data-target="#item-{{ $item->id }}">

                                    {{ $item->name }}

                                    <span class="card-btn-arrow">
                                <i class="fas fa-chevron-down text-gray-90 font-size-18"></i>
                            </span>
                                </button>
                            </h5>
                        </div>
                        <div id="item-{{ $item->id }}" class="collapse"
                             data-parent="#basicsAccordion">
                            <div class="card-body pl-0 pb-8">
                                <p class="mb-0">{!! $item->description !!}</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Card -->
                @endforeach
            </div>
            <!-- FAQ Section end -->
        @endif

        <!-- Brand Carousel -->
        @if($brands->isNotEmpty())
            <div class="container mb-8">
            <div class="py-2 border-top border-bottom">
                <div class="js-slick-carousel u-slick my-1"
                    data-slides-show="5"
                    data-slides-scroll="1"
                    data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                    data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                    data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                    data-responsive='[{
                        "breakpoint": 992,
                        "settings": {
                            "slidesToShow": 2
                        }
                    }, {
                        "breakpoint": 768,
                        "settings": {
                            "slidesToShow": 1
                        }
                    }, {
                        "breakpoint": 554,
                        "settings": {
                            "slidesToShow": 1
                        }
                    }]'>

                    @foreach($brands as $brand)
                        <div class="js-slide">
                            <a href="javascript:void(0);" class="link-hover__brand">
                                <img class="img-fluid m-auto max-height-50" src="{{ $brand->mainphoto() }}" alt="{{ $brand->name }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <!-- End Brand Carousel -->

        <!-- Tab Prodcut Section -->
        <div class="mb-6">
            <!-- Nav Classic -->
            <div class="position-relative bg-white text-center z-index-2">
                <ul class="nav nav-classic nav-tab justify-content-center" id="recommended-tab" role="tablist">

                    @if($recommended->isNotEmpty())
                        <li class="nav-item">
                            <a class="nav-link active js-animation-link" id="pills-one-example1-tab" data-toggle="pill"
                               href="#recommended" role="tab"
                               data-target="#recommended"
                               data-link-group="groups"
                               data-animation-in="slideInUp">
                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                    @lang('common.recommended')
                                </div>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- End Nav Classic -->
            <!-- Tab Content -->
            <div class="tab-content" id="pills-tabContent">
                @if($recommended->isNotEmpty())
                    <div class="tab-pane fade pt-2 show active" id="recommended" role="tabpanel"
                         aria-labelledby="pills-one-example1-tab" data-target-group="groups">
                        <ul class="row list-unstyled products-group no-gutters">
                            @foreach($recommended as $product)
                                <li class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2 product-item h-100 remove-divider-xs-down">
                                    @include('partials.product-box-recommended')
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <!-- End Tab Content -->
        </div>
        <!-- End Tab Prodcut Section -->
    </div>
@endsection
