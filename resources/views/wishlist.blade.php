@extends('body')
@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{ route('index') }}">@lang('common.home')</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">@lang('common.wishlist')</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="row mb-8">
            <div class="col-xl-12 col-wd-12gdot5">
                <!-- Shop-control-bar Title -->
                <div class="flex-center-between mb-3">
                    <h3 class="font-size-25 mb-0">@lang('common.wishlist')</h3>
                </div>
                <!-- End shop-control-bar Title -->
                <!-- Shop Body -->
                <!-- Tab Content -->

                @if($products->isNotEmpty())
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade pt-2 active show" id="pills-two-example1" role="tabpanel" aria-labelledby="pills-two-example1-tab" data-target-group="groups">
                            <ul class="row list-unstyled products-group no-gutters">
                                @foreach($products as $product)
                                    <li class="col-12 col-sm-3 product-item remove-divider-xs-down">
                                        @include('partials.product-box-grid')
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    {{ $products->links() }}
                @endif
                <!-- End Tab Content -->
                <!-- End Shop Body -->
            </div>
        </div>
    </div>
@endsection
