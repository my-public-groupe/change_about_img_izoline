@extends('body')

@section('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />
@endsection

@include('partials.meta', ['data' => $product])

@if ($product->photos->isNotEmpty())
    @section('og_image', URL::to('/') . $product->originalphoto())
@endif

@switch(true)
    @case($product->price_piece !== null)
        @section('og_price_amount', number_format($product->price_piece, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.measure_type_1'))
    @break

    @case($product->price_cubic !== null)
        @section('og_price_amount', number_format($product->price_cubic, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.price_cubic'))
    @break

    @case($product->price_square !== null)
        @section('og_price_amount', number_format($product->price_square, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.price_square'))
    @break

    @case($product->price_roll !== null)
        @section('og_price_amount', number_format($product->price_roll, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.measure_type_3'))
    @break

    @case($product->price_box !== null)
        @section('og_price_amount', number_format($product->price_box, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.measure_type_4'))
    @break

    @case($product->price_list !== null)
        @section('og_price_amount', number_format($product->price_list, 2, '.', ''))
        @section('og_price_currency', trans('common.lei') . '/' . trans('common.measure_type_2'))
    @break
@endswitch

@section('og_product_brand', $product->brand->name ?? '')
@section('og_product_availability', 'in stock')
@section('og_product_condition', 'new')

@section('centerbox')
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                            <a href="{{ route('index') }}">@lang('common.home')</a>
                        </li>

                        @if(!empty($product->category[0]) && !empty($product->category[0]->parents->count()))
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                                <a href="{{ route('catalog.get-parent-category', $product->category[0]->parents[0]->slug) }}">{{ $product->category[0]->parents[0]->name }}</a>
                            </li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                                <a href="{{ route('catalog.get-child-category', [$product->category[0]->parents[0]->slug, $product->category[0]->slug]) }}">{{ $product->category[0]->name }}</a>
                            </li>
                        @endif

                        @if(!empty($product->category[0]) && empty($product->category[0]->parents->count()))
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                                <a href="{{ route('catalog.get-parent-category', $product->category[0]->slug) }}">{{ $product->category[0]->name }}</a>
                            </li>
                        @endif

                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                            aria-current="page">{{ $product->name }}</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <!-- Single Product Body -->
        <div class="mb-6">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-4 mb-4 mb-md-0">
                    <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                        data-infinite="true"
                        data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                        data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                        data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                        data-nav-for="#sliderSyncingThumb">

                        @if($product->photos->isNotEmpty())
                            @foreach($product->photos as $k => $photo)
                                <a href="{{ $product->mainphoto($k) }}" data-fancybox="gallery">
                                    <div class="js-slide">
                                        <img class="img-fluid" src="{{ $product->mainphoto($k) }}" alt="{{ $product->name }}">
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>

                    <div id="sliderSyncingThumb"
                         class="js-slick-carousel u-slick u-slick--slider-syncing u-slick--gutters-1 u-slick--transform-off"
                         data-infinite="true"
                         data-slides-show="5"
                         data-is-thumbs="true"
                         data-nav-for="#sliderSyncingNav">

                        @if($product->photos->isNotEmpty())
                            @foreach($product->photos as $k => $photo)
                                <div class="js-slide" style="cursor: pointer;">
                                    <img class="img-fluid" src="{{ $product->mainphotothumb($k) }}"
                                         alt="{{ $product->name }}">
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xl-4 mb-md-6 mb-lg-0">
                    <div class="mb-2">
                        @if(!empty($product->sku))
                            <small><strong>SKU</strong>: {{ $product->sku }}</small>
                        @endif

                        <h1 class="font-size-25 text-lh-1dot2">{{ $product->name }}</h1>

                        @if($product->brand)
                            <div class="product-brand">
                                <img src="{{ $product->brand->mainphoto() }}" alt="{{ $product->brand->name }}">
                            </div>
                        @endif

                        <p>{!! $product->description_short !!}</p>
                    </div>
                </div>
                <div class="mx-md-auto col-md-8 col-lg-6 col-xl-4">
                    <div class="mb-2">
                        <qty-calculator
                            :product='@json($product)'
                            :thickness_list='@json($thickness_list)'>
                        </qty-calculator>
                    </div>

                    <div class="mb-2 mt-4">
                        <div class="d-flex justify-content-center align-items-center mb-3">
                            <div class="img-infographic-box">
                                <img class="img-infographic" src="img/delivery.png" alt="Delivery">
                            </div>

                            <div class="d-block text-infographic-box">
                                <h5 class="title-infographic">@lang('common.tile-delivery-infographic')</h5>
                                <p class="text-infographic">@lang('common.delivery-infographic')</p>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center align-items-center mb-3">
                            <div class="img-infographic-box">
                                <img class="img-infographic" src="img/return.png" alt="Return">
                            </div>

                            <div class="d-block text-infographic-box">
                                <h5 class="title-infographic">@lang('common.tile-return-infographic')</h5>
                                <p class="text-infographic">@lang('common.return-infographic')</p>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center align-items-center">
                            <div class="img-infographic-box">
                                <img class="img-infographic" src="img/certificate.png" alt="Certificate">
                            </div>

                            <div class="d-block text-infographic-box">
                                <h5 class="title-infographic">@lang('common.tile-certificate-infographic')</h5>
                                <p class="text-infographic">@lang('common.certificate-infographic')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Product Body -->
        <!-- Single Product Tab -->
        <div class="mb-8">
            <div class="position-relative position-md-static px-md-6">
                <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-0 pb-1 pb-xl-0 mb-n1 mb-xl-0"
                    id="pills-tab-8" role="tablist">

                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link active" id="description-tab" data-toggle="pill" href="#description"
                           role="tab">@lang('common.description')</a>
                    </li>

                    @if($product->params()->isNotEmpty())
                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a class="nav-link" id="params-tab" data-toggle="pill"
                               href="#parameters" role="tab">@lang('common.specification')</a>
                        </li>
                    @endif

                    @if($product->faq->isNotEmpty())
                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a class="nav-link" id="faq-tab" data-toggle="pill"
                               href="#faq" role="tab">@lang('common.faq')</a>
                        </li>
                    @endif
                </ul>
            </div>

            <!-- Tab Content -->
            <div class="borders-radius-17 border p-4 mt-4 mt-md-0 px-lg-10 py-lg-9 mb-6">
                <div class="tab-content" id="Jpills-tabContent">

                    <div class="last-p-margin-0 tab-pane fade active show" id="description" role="tabpanel">
                        {!! $product->description !!}
                    </div>

                    @if($product->params()->isNotEmpty())
                        <div class="tab-pane fade" id="parameters" role="tabpanel">
                            <div class="mx-md-5 pt-1">
                                <div class="table-responsive mb-4">
                                    <table class="table table-hover ">
                                        <tbody>
                                        <tr>
                                            <th class="px-4 px-xl-5 border-bottom border-top-0">@lang('common.width')</th>
                                            <td class="border-bottom border-top-0">{{ $product->width }}</td>
                                        </tr>
                                        <tr>
                                            <th class="px-4 px-xl-5 border-bottom">@lang('common.length')</th>
                                            <td class="border-bottom">{{ $product->length }}</td>
                                        </tr>
                                        <tr>
                                            <th class="px-4 px-xl-5 border-bottom">@lang('common.thickness')</th>
                                            <td class="border-bottom">{{ $product->thickness }}</td>
                                        </tr>
                                        @foreach($product->params() as $name => $values)
                                            <tr>
                                                <th class="px-4 px-xl-5 border-top">{{ $name }}</th>
                                                <td class="border-top">{{ implode(", ", $values) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($product->faq->isNotEmpty())
                        <div class="tab-pane fade" id="faq" role="tabpanel">
                            <div id="basicsAccordion" class="mb-4">
                                @foreach($product->faq as $k => $data)
                                    @if(empty($data['question']) || empty($data['answer']))
                                        @continue
                                    @endif

                                    <div
                                        class="card mb-3 border-top-0 border-left-0 border-right-0 border-color-1 rounded-0">
                                        <div class="card-header card-collapse bg-transparent-on-hover border-0"
                                             id="basicsHeadingOne">
                                            <h5 class="mb-0">
                                                <button type="button"
                                                        class="px-0 btn btn-link btn-block d-flex justify-content-between card-btn collapsed py-3 font-size-25 border-0"
                                                        data-toggle="collapse"
                                                        data-target="#answer-{{$k}}">
                                                    {{ $data['question'] }}

                                                    <span class="card-btn-arrow">
                                        <i class="fas fa-chevron-down text-gray-90 font-size-18"></i>
                                    </span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="answer-{{$k}}" class="collapse"
                                             data-parent="#basicsAccordion">
                                            <div class="card-body pl-0 pb-8">
                                                <p class="mb-0">{!! $data['answer'] !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>
            </div>
            <!-- End Tab Content -->

            @if($related->isNotEmpty())
                    <!-- Recommended Products -->
                    <div class="mb-6 d-block">
                        <div class="position-relative">
                            <div class="border-bottom border-color-1 mb-2">
                                <h3 class="d-inline-block section-title section-title__full mb-0 pb-2 font-size-22">@lang('common.recommended_goods')</h3>
                            </div>
                            <div
                                class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                                data-slides-show="5"
                                data-slides-scroll="1"
                                data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                                data-arrow-left-classes="fa fa-angle-left right-1"
                                data-arrow-right-classes="fa fa-angle-right right-0"
                                data-responsive='[{
                                    "breakpoint": 1400,
                                    "settings": {
                                        "slidesToShow": 4
                                    }
                                    }, {
                                        "breakpoint": 1200,
                                        "settings": {
                                        "slidesToShow": 4
                                        }
                                    }, {
                                    "breakpoint": 992,
                                    "settings": {
                                        "slidesToShow": 3
                                    }
                                    }, {
                                    "breakpoint": 768,
                                    "settings": {
                                        "slidesToShow": 2
                                    }
                                    }, {
                                    "breakpoint": 554,
                                    "settings": {
                                        "slidesToShow": 2
                                    }
                                    }]'>

                                    @foreach($related as $related_product)
                                    <div class="js-slide products-group">
                                        <div class="product-item h-100">
                                            @include('partials.product-box-recommended', ['product' => $related_product])
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- End Recommended Products -->
                @endif


            </div>
        </div>
    </div>
@endsection

@section('schema')
    <script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "{{ $product->name }}",
        "image": [
            @foreach ($product->photos as $photo)
            "{{ url("uploaded/$photo->source") }}"@if(!$loop->last),
            @endif
        @endforeach
        ],
    "description": "@if(!empty($product->description))
            {!! strip_tags($product->description) !!}
        @else
            {!! $product->name !!}
        @endif",
        "sku": "{{ $product->id }}",
        "mpn": "{{ $product->id }}",
        @if($product->brand)
            "brand": {
              "@type": "Brand",
              "name": "{{ $product->brand->name }}"
            },
        @endif
        "offers": {
            "@type": "Offer",
            "url": "{{ url()->current() }}",
            "priceCurrency": "MDL",
            "price": {{ $product->getRealPrice() }},
            "priceValidUntil": "{{ \Carbon\Carbon::parse($product->created_at)->addYears(10)->format('Y-m-d') }}",
            "itemCondition": "https://schema.org/NewCondition",
            "availability": "https://schema.org/InStock",
            "seller": {
                "@type": "Organization",
                "name": "{{ env('APP_NAME') }}"
            },
            "hasMerchantReturnPolicy": {
              "@type": "MerchantReturnPolicy",
              "applicableCountry": "MD",
              "returnPolicyCategory": "https://schema.org/MerchantReturnFiniteReturnWindow",
              "merchantReturnDays": 14,
              "returnMethod": "https://schema.org/ReturnByMail",
              "returnFees": "https://schema.org/FreeReturn"
            },
            "shippingDetails": {
              "@type": "OfferShippingDetails",
              "shippingDestination": {
                "@type": "DefinedRegion",
                "addressCountry": "MD"
              }
            }
        }
    }

    </script>
@endsection

@section('scripts')
    <script>
        $('#myDiv').click(function () {
            $("#cheapFrmModal").modal('show');
        });
        $('#consultationBtn').click(function () {
            $("#consultationFrmModal").modal('show');
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>

    <script>
        $(document).ready(function() {
            $('[data-fancybox="gallery"]').fancybox({
                buttons: ["zoom", "close"],
                touch: {
                    vertical: true,
                },
                loop: true,
            });
        });
    </script>
@endsection
