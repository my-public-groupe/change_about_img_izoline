@extends('body')

@section('centerbox')
    <div id="content" class="site-content" tabindex="-1">
        <div class="container">
            <div id="primary" class="content-area page-404">
                <main id="main" class="site-main">
                    <div class="center-block text-center">
                        <div class="info-404">
                            <div class="text-xs-center inner-bottom-xs">
                                <h2 class="display-3">404</h2>
                                <p class="lead">@lang('common.404')</p>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>
        .page-404{
            margin-top: 50px;
            margin-bottom: 50px;
        }
    </style>
@endsection
