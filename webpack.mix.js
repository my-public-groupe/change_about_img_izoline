const mix = require('laravel-mix');

/*mix.browserSync({
    proxy: 'eclairaffair.local'
});*/

mix.js('resources/js/app.js', 'public/js').vue({
    extractStyles: true,
    globalStyles: false
}).webpackConfig((webpack) => {
    return {
        plugins: [
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
            }),
        ],
    };
})
    .sass('resources/scss/app.scss', 'public/css')
    .options({
        processCssUrls: false,
    })
    .extract()
    .version()
