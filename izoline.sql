-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 06, 2023 at 10:52 AM
-- Server version: 10.3.36-MariaDB
-- PHP Version: 8.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `izoline`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `show_menu` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `enabled`, `show_menu`, `views`, `sort`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Гидроизюляция', 'Hidroizolație', '', '', '', '', '', '', '', 1, 1, 0, 0, 'gidroizyulyacziya', '2023-04-28 10:45:20', '2023-05-10 09:52:05'),
(2, 'Теплоизоляция', 'Izolație termică', '', '', '', '', '', '', '', 1, 1, 0, 0, 'teploizolyacziya', '2023-04-28 10:45:20', '2023-05-10 09:56:50'),
(3, 'Защитные пленки, мембраны, геотекстиль', 'Pelicule de protecție, membrane, geotextile', '', '', '', '', '', '', '', 1, 1, 0, 0, 'zashhitnye-plenki-membrany-geotekstili', '2023-04-28 10:45:20', '2023-05-10 09:57:33'),
(4, 'Аксессуары для плоских кровель', 'Accesorii pentru acoperișuri plate', '', '', '', '', '', '', '', 1, 1, 0, 0, 'aksessuary-dlya-ploskix-kroveli', '2023-04-28 10:45:20', '2023-05-10 09:57:53'),
(5, 'Крепёж', 'Fixare', '', '', '', '', '', '', '', 1, 0, 0, 0, 'krepyozh', '2023-04-28 10:45:20', '2023-05-10 09:58:01'),
(6, 'Рулонные битумно-полимерные материалы', 'Materiale cu rulouri de bitum-polimer', '', '', '', '', '', '', '', 1, 0, 0, 0, 'rulonnye-bitumnopolimernye-materialy', '2023-04-28 10:45:20', '2023-04-28 10:45:20'),
(7, 'ПВХ мембраны', 'Membrane din PVC', '', '', '', '', '', '', '', 1, 0, 0, 0, 'pvx-membrany', '2023-04-28 10:45:20', '2023-04-28 10:45:20'),
(8, 'Праймеры и мастики', 'Primeruri și masticuri', '', '', '', '', '', '', '', 1, 0, 0, 0, 'prajmery-i-mastiki', '2023-04-28 10:45:20', '2023-04-28 10:45:20'),
(9, 'Экструдированный пенополистирол (XPS)', 'Spumă de polistiren extrudat (XPS)', '', '', '', '', '', '', '', 1, 0, 0, 0, 'ekstrudirovannyj-penopolistirol-xps', '2023-04-28 10:45:20', '2023-04-28 10:45:20'),
(10, 'Каменная вата', 'Vata minerala', '', '', '', '', '', '', '', 1, 0, 0, 0, 'kamennaya-vata', '2023-04-28 10:45:20', '2023-04-28 10:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `categories_materials`
--

CREATE TABLE `categories_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_ro` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_short_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_materials`
--

INSERT INTO `categories_materials` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `enabled`, `sort`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Группа 1', 'Grupa 1', '', '', '', '', '', '', '', 1, 0, 'gruppa-1', '2023-05-12 09:49:40', '2023-05-12 12:29:57'),
(2, 'Группа 2', 'Grupa 2', '', '', '', '', '', '', '', 1, 0, 'gruppa-2', '2023-05-12 12:29:49', '2023-05-12 12:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `categories_news`
--

CREATE TABLE `categories_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories_xref`
--

CREATE TABLE `categories_xref` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `child_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_xref`
--

INSERT INTO `categories_xref` (`id`, `parent_id`, `child_id`) VALUES
(1, 1, 6),
(2, 1, 7),
(3, 1, 8),
(4, 2, 9),
(5, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_en` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ro` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `views` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `name`, `name_en`, `name_ro`, `description`, `description_en`, `description_ro`, `enabled`, `views`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'О нас', '', 'Despre noi', '<p>Строительный магазин&nbsp;<strong>&quot;IZOLINE&quot;&nbsp;</strong>сфокусирован на оптовой и розничной продаже термо и гидроизоляционных материалов. Наша цель обеспечить строительный рынок современными, экологичными и качественными товарами. Специализируясь на прямых поставках таких брэндов как &quot;<strong>PENOPLEX</strong>&quot;, &quot;<strong>ROCKWOOL</strong>&quot;, &quot;<strong>BELTEP</strong>&quot;, &quot;<strong>PLASTFOIL</strong>&quot;, &quot;<strong>KRZ&quot;</strong>,&nbsp;<strong>&quot;HEMIX&quot;&nbsp;</strong>&nbsp;- мы способны предоставить потребителю в сфере изоляционных материалов превосходное качество товара по разумным ценам.</p>\r\n\r\n<p>Нет плохих материалов - Есть соответствие заявленным характеристикам.&nbsp;Клиент имеет право знать, за что он платит деньги (за бренд, за качество, за стабильность характеристик приобретаемой продукции).<br />\r\n- Необходим определенный бренд - предложим!<br />\r\n- Интересуют характеристики (по критерию цена-качество) - подберем!</p>', '', '<p>Compania&nbsp;<strong>&rdquo;IZOLINE&rdquo;</strong>&nbsp;este specializată &icirc;n domeniul comercializării en-gross și cu amănuntul a materialelor pentru termoizolare și hidroizolare. Noi avem drept scop livrarea materialelor de construcție moderne, ecologic pure și de cea mai bună calitate. Import&acirc;nd direct de la producătorii cu branduri cunoscute ca:&nbsp;<strong>&quot;ROCKWOOL&quot;, &quot;PENOPLEX&quot;, &quot;BELTEP&quot;, &quot;PLASTFOIL&quot;, &quot;KRZ&quot;, &quot;HEMIX&quot; -</strong>&nbsp;compania garantează consumatorului produse de calitate și la preț rezonabil.&nbsp;</p>', 1, 0, 'about-us', '2023-05-22 08:17:11', '2023-05-22 08:17:24'),
(2, 'Политика конфиденциальности', '', 'Politica de confidențialitate', '<p>Мы, компания &quot;<strong>Izoline</strong>&quot;, заботимся о вашей конфиденциальности и стремимся обеспечить защиту ваших персональных данных. Эта политика конфиденциальности поясняет, какие данные мы собираем, зачем они используются и как мы обеспечиваем их безопасность.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Мы собираем информацию о вас, когда вы регистрируетесь на нашем сайте, заполняете форму заказа или связываетесь с нами по электронной почте. Мы можем использовать эту информацию для улучшения наших услуг и продуктов, а также для более эффективного обслуживания наших клиентов.</p>\r\n	</li>\r\n	<li>\r\n	<p>Мы не передаем вашу личную информацию третьим лицам без вашего явного согласия, за исключением случаев, предусмотренных законодательством.</p>\r\n	</li>\r\n	<li>\r\n	<p>Мы прилагаем все усилия для обеспечения безопасности вашей информации. Для защиты ваших персональных данных мы используем различные технические средства и процедуры.</p>\r\n	</li>\r\n	<li>\r\n	<p>Наш сайт может содержать ссылки на другие сайты, которые не контролируются нами. Мы не несем ответственности за содержание и политику конфиденциальности этих сайтов.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Мы рекомендуем вам ознакомиться с нашей политикой конфиденциальности и обращаться к ней при необходимости. Если у вас есть какие-либо вопросы или предложения по улучшению нашей политики конфиденциальности, пожалуйста, свяжитесь с нами.</p>', '', '<p>Noi, compania &quot;<strong>Izoline</strong>&quot;, ne preocupăm de confidențialitatea dvs. și ne străduim să protejăm datele dvs. personale. Această politică de confidențialitate explică ce date colectăm, &icirc;n ce scopuri le utilizăm și cum asigurăm securitatea lor.</p>\r\n\r\n<ol>\r\n	<li>Colectăm informații despre dvs. atunci c&acirc;nd vă &icirc;nregistrați pe site-ul nostru, completați un formular de comandă sau ne contactați prin e-mail. Putem utiliza aceste informații pentru &icirc;mbunătățirea serviciilor și produselor noastre și pentru a servi mai eficient clienții noștri.</li>\r\n	<li>Nu divulgăm informațiile personale ale dvs. către terți fără consimțăm&acirc;ntul dvs. expres, cu excepția cazurilor prevăzute de lege.</li>\r\n	<li>Facem toate eforturile pentru a ne asigura că informațiile dvs. sunt &icirc;n siguranță. Pentru a proteja datele dvs. personale, folosim diferite instrumente și proceduri tehnice.</li>\r\n	<li>Site-ul nostru poate conține link-uri către alte site-uri care nu sunt controlate de noi. Nu suntem responsabili pentru conținutul și politica de confidențialitate a acestor site-uri.</li>\r\n</ol>\r\n\r\n<p>Vă recomandăm să citiți politica noastră de confidențialitate și să o utilizați &icirc;n caz de nevoie. Dacă aveți &icirc;ntrebări sau sugestii pentru &icirc;mbunătățirea politicii noastre de confidențialitate, vă rugăm să ne contactați.</p>', 1, 0, 'privacy-policy', '2023-05-22 08:31:01', '2023-05-22 08:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(34, 'default', '{\"uuid\":\"9ed6f3c2-652e-4ae3-bdfe-ffc07527e42c\",\"displayName\":\"App\\\\Notifications\\\\OrderCreated\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":3:{s:11:\\\"notifiables\\\";O:29:\\\"Illuminate\\\\Support\\\\Collection\\\":2:{s:8:\\\"\\u0000*\\u0000items\\\";a:1:{i:0;O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:16:\\\"admin@izoline.md\\\";}}}s:28:\\\"\\u0000*\\u0000escapeWhenCastingToString\\\";b:0;}s:12:\\\"notification\\\";O:30:\\\"App\\\\Notifications\\\\OrderCreated\\\":3:{s:6:\\\"locale\\\";s:2:\\\"ro\\\";s:8:\\\"\\u0000*\\u0000order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":5:{s:5:\\\"class\\\";s:16:\\\"App\\\\Models\\\\Order\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";s:15:\\\"collectionClass\\\";N;}s:2:\\\"id\\\";s:36:\\\"f2819cf7-ac26-4a7d-8981-2b7db3b170ae\\\";}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}}\"}}', 0, NULL, 1686031248, 1686031248),
(35, 'default', '{\"uuid\":\"46722b53-9150-43d6-87d5-1a06a07aa317\",\"displayName\":\"App\\\\Notifications\\\\OrderCreated\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":3:{s:11:\\\"notifiables\\\";O:29:\\\"Illuminate\\\\Support\\\\Collection\\\":2:{s:8:\\\"\\u0000*\\u0000items\\\";a:1:{i:0;O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:17:\\\"johndoe@gmail.com\\\";}}}s:28:\\\"\\u0000*\\u0000escapeWhenCastingToString\\\";b:0;}s:12:\\\"notification\\\";O:30:\\\"App\\\\Notifications\\\\OrderCreated\\\":3:{s:6:\\\"locale\\\";s:2:\\\"ro\\\";s:8:\\\"\\u0000*\\u0000order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":5:{s:5:\\\"class\\\";s:16:\\\"App\\\\Models\\\\Order\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";s:15:\\\"collectionClass\\\";N;}s:2:\\\"id\\\";s:36:\\\"6c466731-b4b7-4459-a410-ceea71553ef4\\\";}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}}\"}}', 0, NULL, 1686031248, 1686031248);

-- --------------------------------------------------------

--
-- Table structure for table `lists`
--

CREATE TABLE `lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `views` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reserve` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lists`
--

INSERT INTO `lists` (`id`, `name`, `name_ro`, `name_en`, `description`, `description_ro`, `description_en`, `description_short`, `description_short_ro`, `description_short_en`, `parent_id`, `enabled`, `views`, `sort`, `slug`, `params`, `reserve`, `created_at`, `updated_at`) VALUES
(1, 'Слайдер', 'Slider', 'Слайдер', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 'slider', NULL, NULL, '2023-04-10 10:49:37', NULL),
(2, '#1', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, '1', '{\"btn_name_ru\":\"\\u041f\\u0435\\u0440\\u0435\\u0439\\u0442\\u0438 \\u0432 \\u043a\\u0430\\u0442\\u0430\\u043b\\u043e\\u0433\",\"btn_name_ro\":\"Vezi catalogul\",\"btn_link_ru\":\"\\/\",\"btn_link_ro\":\"\\/\"}', NULL, '2023-04-10 10:57:16', NULL),
(3, 'Техническая документация', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 'texniceskaia-dokumentaciia', NULL, NULL, '2023-04-28 13:16:12', NULL),
(4, 'Гидроизоляция', 'Hidroizolație', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 0, 0, 'gidroizoliaciia', NULL, NULL, '2023-04-28 13:29:31', NULL),
(5, 'Битумная', 'Bitum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 0, 0, 'bitumnaia', '[{\"pdf_name\":\"1\",\"pdf_file\":\"test.pdf\"}]', NULL, '2023-04-28 14:07:37', NULL),
(6, 'Мембраны ПВХ', 'Membrane PVC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, 0, 0, 'membrany-pvx', NULL, NULL, '2023-05-11 16:20:03', NULL),
(7, 'Технониколь', 'Tehnonikol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 1, 0, 0, 'texnonikol', NULL, NULL, '2023-05-11 16:20:49', NULL),
(8, 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 1, 0, 0, 'test', '[{\"pdf_name\":\"\\u041f\\u0440\\u0438\\u043c\\u0435\\u0440 \\u0434\\u043e\\u043a\\u0443\\u043c\\u0435\\u043d\\u0442\\u0430\",\"pdf_file\":\"test.pdf\"}]', NULL, '2023-05-11 16:21:24', NULL),
(9, 'Видеоматериалы', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, 'videos', '{\"key\":[\"\",\"\",\"\"],\"value\":[\"\",\"\",\"\"]}', NULL, '2023-05-12 09:53:19', NULL),
(10, 'Изолента ПВХ. Применение и технические характеристики', 'Bandă adezivă din PVC pentru conducte. Aplicație și date tehnice', NULL, NULL, NULL, NULL, 'Изолента изготавливается на основе поливинил хлоридного изоляционного пластика. Клеевой слой нанесён на 1 сторону. Материал, из которого изготовлена лента, нетоксичен. В процессе производства изоленты использованы химические добавки, которые препятствуют плавлению материала под действием высоких температур. Допустимый рабочий диапазон температур составляет от -50° С до +70° С.', 'Banda are la bază plastic izolator din policlorură de vinil. Stratul adeziv este aplicat pe o singură față. Materialul utilizat pentru fabricarea benzii este netoxic. În procesul de producție se folosesc aditivi chimici pentru a preveni topirea materialului la temperaturi ridicate. Intervalul de temperatură de funcționare admisibil este de la -50°C la +70°C.', NULL, 9, 1, 0, 0, 'izolenta-pvx-primenenie-i-texniceskie-xarakteristiki', '{\"video_link\":\"https:\\/\\/www.youtube.com\\/watch?v=-rlQN6XvMnE\"}', '1', '2023-05-12 10:37:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aggregate_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `variant_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_media_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mediables`
--

CREATE TABLE `mediables` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `mediable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meta`
--

CREATE TABLE `meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description_ro` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description_en` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords_ro` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords_en` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ro` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL,
  `table_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta`
--

INSERT INTO `meta` (`id`, `meta_description`, `meta_description_ro`, `meta_description_en`, `meta_keywords`, `meta_keywords_ro`, `meta_keywords_en`, `title`, `title_ro`, `title_en`, `table_id`, `table_type`) VALUES
(1, '', '', '', '', '', '', '', '', '', 1, 'App\\Models\\Categories'),
(2, '', '', '', '', '', '', '', '', '', 2, 'App\\Models\\Categories'),
(3, '', '', '', '', '', '', '', '', '', 3, 'App\\Models\\Categories'),
(4, '', '', '', '', '', '', '', '', '', 4, 'App\\Models\\Categories'),
(5, '', '', '', '', '', '', '', '', '', 5, 'App\\Models\\Categories'),
(6, '', '', '', '', '', '', '', '', '', 6, 'App\\Models\\Categories'),
(7, '', '', '', '', '', '', '', '', '', 7, 'App\\Models\\Categories'),
(8, '', '', '', '', '', '', '', '', '', 8, 'App\\Models\\Categories'),
(9, '', '', '', '', '', '', '', '', '', 9, 'App\\Models\\Categories'),
(10, '', '', '', '', '', '', '', '', '', 10, 'App\\Models\\Categories'),
(11, '', '', '', '', '', '', '', '', '', 1, 'App\\Models\\CategoriesMaterials'),
(12, '', '', '', '', '', '', '', '', '', 2, 'App\\Models\\CategoriesMaterials'),
(13, '', '', '', '', '', '', '', '', '', 1, 'App\\Models\\Content'),
(14, '', '', '', '', '', '', '', '', '', 2, 'App\\Models\\Content');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short_en` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `top` tinyint(1) NOT NULL DEFAULT 0,
  `views` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE `news_categories` (
  `news_id` int(11) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `data`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '{\"products\":[{\"id\":1,\"name\":\"Elastoizol business EKP 5,0\",\"product_number\":\"#2\",\"price\":6,\"currency_price_valute\":\" lei \\/ folie\",\"currency_type_price\":\"6 lei \\/ m<sup>3<\\/sup>\",\"has_sale\":false,\"quantity\":1,\"photo\":\"\\/uploaded\\/test_5.png\",\"slug\":\"http:\\/\\/izoline.local\\/catalog\\/product\\/elastoizol-business-ekp-50\",\"weight\":1,\"surface\":0.11},{\"id\":2,\"name\":\"Penoplex Comfort\",\"product_number\":\"#1\",\"price\":40,\"currency_price_valute\":\" lei \\/ rola\",\"currency_type_price\":\"40 lei \\/ m<sup>2<\\/sup>\",\"has_sale\":false,\"quantity\":1,\"photo\":\"\\/uploaded\\/penoplex-comfort_13.png\",\"slug\":\"http:\\/\\/izoline.local\\/catalog\\/product\\/penoplex-comfort\",\"weight\":1,\"surface\":16}],\"ip\":\"127.0.0.1\",\"user_agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/114.0.0.0 Safari\\/537.36\",\"orderInfo\":{\"payment\":\"cash\",\"name\":\"John Doe\",\"phone\":\"+37368123456\",\"email\":\"johndoe@gmail.com\",\"address\":\"test 123\\/ABC\",\"comment\":\"-\"},\"amount\":46,\"lang\":\"ro\",\"currency\":{\"name\":\"MDL\",\"label\":\"MDL\",\"label_short\":\"lei\"}}', 1, 0, '2023-06-06 06:00:48', '2023-06-06 06:00:48');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_filter` tinyint(1) NOT NULL DEFAULT 1,
  `filter_type` tinyint(4) DEFAULT NULL,
  `is_characteristic` tinyint(1) NOT NULL DEFAULT 1,
  `type` tinyint(4) DEFAULT NULL COMMENT '0-input, 1-select'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `name`, `name_ro`, `name_en`, `params`, `is_filter`, `filter_type`, `is_characteristic`, `type`) VALUES
(1, 'Предназначение', 'Aplicație', NULL, NULL, 1, NULL, 1, 1),
(2, 'Материал', 'Material', NULL, NULL, 1, NULL, 1, 1),
(3, 'Срок службы', 'Perioada de exploatare', NULL, NULL, 1, NULL, 1, 1),
(4, 'Основа', 'Baza', NULL, NULL, 1, NULL, 1, 1),
(5, 'Верхняя сторона', 'Partea superioară', NULL, NULL, 1, NULL, 1, 1),
(6, 'Толщина', 'Grosime', NULL, NULL, 1, NULL, 1, 1),
(7, 'Горючесть', 'Inflamabilitate', NULL, NULL, 1, NULL, 1, 1),
(8, 'Армирование', 'Armătură', NULL, NULL, 1, NULL, 1, 1),
(9, 'Тип продукции', 'Tipul de produs', NULL, NULL, 1, NULL, 1, 1),
(10, 'Вес', 'Greutate', NULL, NULL, 1, NULL, 1, 1),
(11, 'Прочность на сжатие при 10% линейной деформации', 'Rezistența la compresiune la o deformație liniară de 10%.', NULL, NULL, 1, NULL, 1, 1),
(12, 'Толщина панели', 'Grosimea panoului', NULL, NULL, 1, NULL, 1, 1),
(13, 'Тип', 'Tip', NULL, NULL, 1, NULL, 1, 1),
(14, 'Плотность', 'Densitate', NULL, NULL, 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `parameters_categories`
--

CREATE TABLE `parameters_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parameter_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters_categories`
--

INSERT INTO `parameters_categories` (`id`, `parameter_id`, `category_id`, `sort`) VALUES
(1, 1, 1, 0),
(2, 8, 1, 0),
(3, 5, 1, 0),
(4, 10, 1, 0),
(5, 7, 1, 0),
(6, 2, 1, 0),
(7, 4, 1, 0),
(8, 3, 1, 0),
(9, 6, 1, 0),
(10, 9, 1, 0),
(11, 7, 2, 0),
(12, 1, 2, 0),
(13, 11, 2, 0),
(14, 12, 2, 0),
(15, 14, 2, 0),
(16, 7, 3, 0),
(17, 1, 3, 0),
(18, 13, 3, 0),
(19, 6, 3, 0),
(20, 13, 4, 0),
(21, 13, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameters_products`
--

CREATE TABLE `parameters_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `parameter_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters_products`
--

INSERT INTO `parameters_products` (`id`, `parameter_id`, `product_id`, `value`, `value_ro`, `value_en`, `value_id`, `sort`) VALUES
(509, 1, 1, '', '', '', 1, 0),
(510, 6, 1, '', '', '', 20, 0),
(511, 3, 1, '', '', '', 9, 0),
(512, 4, 1, '', '', '', 13, 0),
(513, 2, 1, '', '', '', 7, 0),
(514, 7, 1, '', '', '', 42, 0),
(515, 10, 1, '', '', '', 33, 0),
(516, 5, 1, '', '', '', 16, 0),
(517, 8, 1, '', '', '', 28, 0),
(518, 9, 1, '', '', '', 31, 0),
(519, 7, 2, '', '', '', 42, 0),
(520, 1, 2, '', '', '', 1, 0),
(521, 11, 2, '', '', '', 36, 0),
(522, 12, 2, '', '', '', 39, 0),
(523, 14, 2, '', '', '', 56, 0),
(524, 13, 4, '', '', '', 47, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameters_values`
--

CREATE TABLE `parameters_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_ro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reserve` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `parameters_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters_values`
--

INSERT INTO `parameters_values` (`id`, `value`, `value_ro`, `value_en`, `reserve`, `sort`, `parameters_id`) VALUES
(1, 'Плоская кровля', 'Acoperiș plat', NULL, '', 0, 1),
(2, 'Скатная кровля', 'Acoperiș înclinat', NULL, '', 1, 1),
(3, 'Стены, Фасад', 'Pereți, fațadă', NULL, '', 2, 1),
(4, 'Полы', 'Podele', NULL, '', 3, 1),
(5, 'Фундамент', 'Fundație', NULL, '', 4, 1),
(6, 'Универсальный', 'Universal', NULL, '', 5, 1),
(7, 'Битумные и битумно-полимерные материалы', 'Materiale bitum și bitum-polimer', NULL, '', 0, 2),
(8, 'Рулонный битумно-полимерный', 'Role bitum-polimere', NULL, '', 1, 2),
(9, '3-5 лет', '3-5 ani', NULL, '', 0, 3),
(10, '7-10 лет', '7-10 ani', NULL, '', 1, 3),
(11, '10-15 лет', '10-15 ani', NULL, '', 2, 3),
(12, 'до 25 лет', 'pina la 25 ani', NULL, '', 3, 3),
(13, 'Безосновный', 'Fără suport', NULL, '', 0, 4),
(14, 'Полиэстер', 'Poliester', NULL, '', 1, 4),
(15, 'Стеклохолст', 'Fibră de sticlă', NULL, '', 2, 4),
(16, 'Базальт', 'Basalt', NULL, '', 0, 5),
(17, 'Сланец', 'Ardezie', NULL, '', 1, 5),
(18, 'Плёнка с логотипом', 'Peliculă cu logo', NULL, '', 2, 5),
(19, 'ПВХ', 'PVC', NULL, '', 2, 2),
(20, '1.2 мм', '1.2 mm', NULL, '', 0, 6),
(21, '1.5 мм', '1.5 mm', NULL, '', 1, 6),
(22, '1.8 мм', '1.8 mm', NULL, '', 2, 6),
(23, '2 мм', '2 mm', NULL, '', 3, 6),
(24, 'Г1', 'G1', NULL, '', 1, 7),
(25, 'Г2', 'G2', NULL, '', 2, 7),
(26, 'Г3', 'G3', NULL, '', 3, 7),
(27, 'Г4', 'G4', NULL, '', 4, 7),
(28, 'Нет', 'Nu', NULL, '', 0, 8),
(29, 'Стеклохолст', 'Fibră de sticlă', NULL, '', 1, 8),
(30, 'Полиэстерная сетка', 'Plasa de poliester', NULL, '', 2, 8),
(31, 'Мастик', 'Mastic', NULL, '', 0, 9),
(32, 'Праймеры', 'Primer', NULL, '', 1, 9),
(33, '7 кг', '7 kg', NULL, '', 0, 10),
(34, '10 кг', '10 kg', NULL, '', 1, 10),
(35, '15 кг', '15 kg', NULL, '', 2, 10),
(36, '100 кПа', '100 kPa', NULL, '', 0, 11),
(37, '150 кПа', '150 kPa', NULL, '', 1, 11),
(38, '180 кПа', '180 kPa', NULL, '', 2, 11),
(39, '20 мм', '20 mm', NULL, '', 0, 12),
(40, '30 мм', '30 mm', NULL, '', 1, 12),
(41, '50 мм', '50 mm', NULL, '', 2, 12),
(42, 'Нет', 'Nu', NULL, '', 0, 7),
(43, 'Геотекстиль', 'Geotextil', NULL, '', 0, 13),
(44, 'Мембрана шиповидная', 'Membrană cu țepi', NULL, '', 1, 13),
(45, 'Пароизоляция', 'Barieră de vapori', NULL, '', 2, 13),
(46, 'Воронки', 'Lacune', NULL, '', 3, 13),
(47, 'Кровельные проходки', 'Drenaje de acoperiș', NULL, '', 4, 13),
(48, 'Листоуловител', 'Dispozitiv de colectare a frunzelor', NULL, '', 5, 13),
(49, 'Аэраторы', 'Aeratoare', NULL, '', 6, 13),
(50, 'Опоры под оборудование', 'Suporturi pentru echipamente', NULL, '', 7, 13),
(51, 'Герметик', 'Etanșant', NULL, '', 8, 13),
(52, 'Анкер-втулки', 'Manșoane de ancorare', NULL, '', 9, 13),
(53, 'Саморезы', 'Șuruburi autofiletante', NULL, '', 10, 13),
(54, 'Шайбы, Рейки', 'Șaibe', NULL, '', 11, 13),
(55, 'Клей-пена', 'Adeziv din spumă', NULL, '', 12, 13),
(56, '100 кг/м.куб', '100 kg/m3', NULL, '', 0, 14),
(57, '120 кг/м.куб', '150 kg/m3', NULL, '', 1, 14);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_id` int(11) NOT NULL DEFAULT 0,
  `table` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `source`, `table_id`, `table`, `token`, `sort`) VALUES
(3, '1_3.jpg', 2, 'lists', '', 3),
(4, '1_4.jpg', 2, 'lists', '', 4),
(5, 'test_5.png', 1, 'products', '', 5),
(10, 'test_10.png', 1, 'products', '', 10),
(11, 'test_11.png', 1, 'products', '', 11),
(12, 'test_12.png', 1, 'products', '', 12),
(13, 'penoplex-comfort_13.png', 2, 'products', '', 13),
(14, 'penoplex-comfort_14.png', 2, 'products', '', 14),
(15, 'penoplex-comfort_15.png', 2, 'products', '', 15),
(16, 'penoplex-comfort_16.png', 2, 'products', '', 16),
(17, 'voronka-vmo-110x450-obogrevaemaya-s-obzhimnym_17.png', 4, 'products', '', 17),
(18, 'voronka-vmo-110x450-obogrevaemaya-s-obzhimnym_18.png', 4, 'products', '', 18),
(19, 'voronka-vmo-110x450-obogrevaemaya-s-obzhimnym_19.jpg', 4, 'products', '', 19);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_1c` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type` tinyint(1) NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `measure_type` tinyint(1) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `thickness` int(11) DEFAULT NULL,
  `area` double(15,2) DEFAULT NULL,
  `weight` double(15,2) DEFAULT NULL,
  `surface` double(15,2) DEFAULT NULL,
  `num_in_box` int(11) DEFAULT NULL,
  `price_piece` double(15,2) DEFAULT NULL,
  `price_cubic` double(15,2) DEFAULT NULL,
  `price_square` double(15,2) NOT NULL,
  `price_roll` double(15,2) NOT NULL,
  `price_box` double(15,2) DEFAULT NULL,
  `placeholder_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder_text_ro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_ro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `in_stock` tinyint(1) NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `id_1c`, `product_type`, `sku`, `manufacturer_id`, `user_id`, `measure_type`, `length`, `width`, `thickness`, `area`, `weight`, `surface`, `num_in_box`, `price_piece`, `price_cubic`, `price_square`, `price_roll`, `price_box`, `placeholder_text`, `placeholder_text_ro`, `name`, `name_ro`, `description`, `description_ro`, `description_short`, `description_short_ro`, `faq`, `faq_ro`, `params`, `recommended`, `slug`, `enabled`, `in_stock`, `views`, `sort`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '#2', NULL, 1, 2, 1500, 1500, 50, 2.25, 1.00, 0.11, 3, 7.00, 6.00, 100.00, 200.00, 0.00, 'примечание', 'text', 'Elastoizol business EKP 5,0', 'Elastoizol business EKP 5,0', '<p><strong>Основные преимущества</strong>:</p>\r\n\r\n<p>Устройство кровли с помощью Эластоизола БИЗНЕС ЭКП-5,0 может производиться на протяжении всего года, за исключением дождливой и снежной погоды. Учитывая, что данный кровельный материал является направляемым, то отпадает необходимость в покупке и использовании приклеивающих мастик, что позволяет снизить стоимость выполнения строительных работ.&nbsp;<br />\r\n<br />\r\n<strong>Способ применения:</strong></p>\r\n\r\n<p>Эластоизол БИЗНЕС ЭКП-5,0 позволяет применять метод сваривания внахлест свободно лежащего материала. Чтобы приклеить данный кровельный материал на предварительно подготовленное основание, покровной слой с нижней стороны оплавляется. Оплавление производится газовыми или другими горелками.&nbsp;<br />\r\n<br />\r\n<strong>Тип покрытия:</strong></p>\r\n\r\n<p>К &mdash; крупнозернистая минеральная посыпка&nbsp;<br />\r\nП &mdash; пленка</p>\r\n\r\n<p><strong>Основа:</strong>&nbsp;Полиэстер</p>', '<p><strong>Principalele avantaje</strong>:</p>\r\n\r\n<p>Acoperișurile cu Elastoisol BIZNESS EKP-5.0 pot fi realizate pe tot parcursul anului, cu excepția vremii ploioase și a zăpezii. Av&acirc;nd &icirc;n vedere că acest material pentru acoperișuri este ghidat, nu este nevoie de achiziționarea și utilizarea masticurilor adezive, ceea ce reduce costurile de realizare a lucrărilor de construcție.&nbsp;</p>\r\n\r\n<p><strong>Metoda de aplicare</strong>:</p>\r\n\r\n<p>Elastoisol BUSINESS EKP-5.0 permite aplicarea metodei de sudare prin suprapunere a materialului liber. Pentru a lipi acest material pentru acoperișuri pe substratul pregătit anterior, stratul de acoperire este topit din partea inferioară. Topirea se face cu gaz sau cu alte arzătoare.&nbsp;</p>\r\n\r\n<p><strong>Tipul de acoperire</strong>:</p>\r\n\r\n<p>K - praf mineral cu granulație grosieră.&nbsp;<br />\r\nP - peliculă</p>\r\n\r\n<p><strong>Substrat</strong>: poliester</p>', 'Elastoizol business EKP 5,0', 'Elastoizol business EKP 5,0', '[{\"question\":\"\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441\",\"answer\":\"\\u041e\\u0442\\u0432\\u0435\\u0442\"}]', '[{\"question\":\"Intrebare\",\"answer\":\"Raspuns\"}]', NULL, '2', 'elastoizol-business-ekp-50', 1, 1, 302, 0, '2023-05-10 13:35:41', '2023-06-06 07:07:09'),
(2, NULL, 2, '#1', NULL, 1, 3, 1500, 1500, 1500, 10.00, 1.00, 16.00, 4, 7.00, 6.00, 40.00, 50.00, NULL, '5', 'text de exemplu', 'Penoplex Comfort', 'Penoplex Comfort', '<p>ПЕНОПЛЭКС КОМФОРТ &mdash; высокоэффективный теплоизоляционный материал (XPS последнего поколения), изготавливаемый методом экструзии из полистирола общего назначения.</p>\r\n\r\n<h3>Применение&nbsp;ПЕНОПЛЭКС&nbsp;КОМФОРТ</h3>\r\n\r\n<p>Универсальный тип, предназначенный для частного домостроения. Рекомендуется для применения в строительных конструкциях, не подверженных высоким нагрузкам: для теплоизоляции цоколей, стен, скатных крыш, плоских неэксплуатируемых кровель, балконов и лоджий. Г-образная кромка плит позволяет удобно их стыковывать и обеспечивать непрерывный слой теплоизоляции.</p>\r\n\r\n<h3>Характеристики ПЕНОПЛЭКС&nbsp;КОМФОРТ</h3>\r\n\r\n<ul type=\"disc\">\r\n	<li>неизменно низкая теплопроводность;</li>\r\n	<li>практически нулевое водопоглощение (однородная структура из закрытых независимых ячеек);</li>\r\n	<li>высокая прочность на сжатие;</li>\r\n	<li>подтвержденная биостойкость;</li>\r\n	<li>доказанная долговечность;</li>\r\n	<li>экологичность.</li>\r\n</ul>', '<p>PENOPLEX COMFORT este un material termoizolant de &icirc;naltă performanță (XPS de ultimă generație) fabricat din polistiren de uz general prin extrudare.</p>\r\n\r\n<p>Aplicarea PENOPLEX COMFORT<br />\r\nTipul universal destinat construcțiilor de locuințe private. Recomandat pentru aplicarea &icirc;n structurile de construcție care nu sunt expuse la sarcini mari: pentru izolarea termică a soclurilor, pereților, acoperișurilor &icirc;nclinate, acoperișurilor plate neexploatabile, balcoanelor și logiilor. Marginea &icirc;n formă de L a plăcilor permite conectarea ușoară a acestora și asigură un strat continuu de izolație termică.</p>\r\n\r\n<p>Caracteristicile PENOPLEX COMFORT</p>\r\n\r\n<ul>\r\n	<li>conductivitate termică permanent scăzută;</li>\r\n	<li>absorbție de apă practic zero (structură omogenă de celule independente &icirc;nchise);</li>\r\n	<li>rezistență ridicată la compresiune;</li>\r\n	<li>biorezistență dovedită;</li>\r\n	<li>durabilitate dovedită;</li>\r\n	<li>respectarea mediului.</li>\r\n</ul>', 'Высокоэффективный теплоизоляционный материал (XPS последнего поколения), изготавливаемый методом экструзии из полистирола общего назначения.', 'Material izolant de înaltă performanță (XPS de ultimă generație) fabricat prin extrudare din polistiren de uz general.', '[{\"question\":\"\",\"answer\":\"\"}]', '[{\"question\":\"\",\"answer\":\"\"}]', NULL, '1', 'penoplex-comfort', 1, 1, 83, 0, '2023-05-22 10:42:16', '2023-06-06 06:40:44'),
(4, NULL, 3, '#3', NULL, 1, 1, NULL, NULL, NULL, NULL, 1.00, 1.00, NULL, 2600.00, NULL, 0.00, 0.00, 2600.00, NULL, NULL, 'Воронка VMO 110x450 обогреваемая с обжимным', 'Pâlnie VMO 110x450 încălzită cu flanșă metalică sertizată și prindere', '<p><strong>Обогреваемая кровельная воронка с листвоуловителем и металлическим обжимным фланцем с вертикальным выпуском</strong>&nbsp;используется при устройстве водоотвода c кровли.<br />\r\nЗа счет встроенного саморегулирующегося кабеля обеспечивается гарантированный обогрев в диапазоне температур от +5&deg;С до &ndash;7&deg;С в точке сбора воды. Благодаря коррозионностойкому металлическому прижимному фланцу с зоной прижатия шириной 45 мм, воронка выдерживает большие нагрузки, а механический способ соединения кровельного полотна применим для всех типов гидроизоляционных материалов.<br />\r\nКомплектуется и гайками и барашками для удобства монтажа и крышкой-заглушкой.<br />\r\nВоронка изготовлена из высокопрочного полимера, устойчивого к ультрафиолетовому воздействию и атмосферному воздействию в диапазоне температур от -50 до +90&deg;С. Материал воронок обладает коэффициентом теплового расширения, близким по величине к линейному расширению гидроизоляционных материалов.</p>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: 1148px; top: -6px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', '<p>Intrarea &icirc;ncălzită a acoperișului cu captare a frunzelor și flanșă de prindere metalică cu ieșire verticală este utilizată pentru drenarea acoperișului.<br />\r\nCablul autoreglabil integrat asigură o &icirc;ncălzire garantată &icirc;ntr-un interval de temperatură cuprins &icirc;ntre +5&deg;C și -7&deg;C la punctul de colectare a apei. Datorită flanșei de prindere metalice rezistente la coroziune, cu o lățime a zonei de presiune de 45 mm, p&acirc;lnia rezistă la sarcini grele, iar metoda de conectare mecanică a foliei de acoperiș este potrivită pentru toate tipurile de materiale de impermeabilizare.<br />\r\nLivrat cu piulițe și piulițe cu aripioare pentru o instalare ușoară și un capac de acoperire.<br />\r\nP&acirc;lnie fabricată din polimer foarte durabil, rezistent la radiațiile UV și la condițiile meteorologice &icirc;n intervalul de temperatură de la -50 la +90&deg;C. Materialul p&acirc;lniei are un coeficient de dilatare termică apropiat de dilatarea liniară a materialelor de impermeabilizare.</p>\r\n\r\n<div id=\"gtx-trans\" style=\"position: absolute; left: -89px; top: -6px;\">\r\n<div class=\"gtx-trans-icon\">&nbsp;</div>\r\n</div>', 'Обогреваемая кровельная воронка с листвоуловителем и металлическим обжимным фланцем с вертикальным выпуском используется при устройстве водоотвода c кровли.', 'Intrarea încălzită a acoperișului cu captare a frunzelor și flanșă de prindere metalică cu ieșire verticală este utilizată pentru drenarea acoperișului.', '[{\"question\":\"\",\"answer\":\"\"}]', '[{\"question\":\"\",\"answer\":\"\"}]', NULL, '', 'voronka-vmo-110x450-obogrevaemaya-s-obzhimnym', 1, 1, 14, 0, '2023-06-06 06:54:44', '2023-06-06 07:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `products_categories`
--

CREATE TABLE `products_categories` (
  `product_id` int(11) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_categories`
--

INSERT INTO `products_categories` (`product_id`, `categories_id`) VALUES
(2, 9),
(1, 6),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_collection`
--

CREATE TABLE `product_collection` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `collection_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `open_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_short` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_phones` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telegram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `viber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `rights` tinyint(4) NOT NULL DEFAULT 0,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addresses` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `masterclass_agree` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo` tinyint(1) NOT NULL,
  `wordpress` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `token`, `new_email`, `password`, `open_password`, `description_short`, `description`, `phone`, `other_phones`, `telegram`, `viber`, `facebook`, `instagram`, `enabled`, `rights`, `params`, `addresses`, `masterclass_agree`, `remember_token`, `promo`, `wordpress`, `created_at`, `updated_at`) VALUES
(1, 'Xsort', 'Web Studio', 'support@xsort.md', '', '', '$2y$10$MbyQdGbhn3IKw3C9WTPN0.PsP6gT0UUkoUw4IxZ3cy/cs0W3Gfsi6', NULL, NULL, NULL, NULL, '', '', '', '', '', 1, 1, NULL, '', 0, NULL, 0, 0, '2021-03-11 15:03:59', '2021-07-23 08:09:43'),
(2, 'Izoline', '', 'admin@izoline.md', '', '', '$2a$12$vrr/W48TXsgXrT5M59qHtOYBAF.DHQGCU.h2w9ByAYWW8fnqWTg3a', 'izoline2023', NULL, NULL, '', '', '', '', '', '', 1, 1, NULL, '', 0, NULL, 0, 0, '2023-04-09 21:00:00', '2021-05-28 11:50:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `categories_materials`
--
ALTER TABLE `categories_materials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `categories_news`
--
ALTER TABLE `categories_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `categories_xref`
--
ALTER TABLE `categories_xref`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_xref_parent_id_index` (`parent_id`),
  ADD KEY `categories_xref_child_id_index` (`child_id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `lists`
--
ALTER TABLE `lists`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lists_slug_unique` (`slug`),
  ADD KEY `lists_parent_id_index` (`parent_id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_slug_unique` (`slug`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `media_disk_directory_filename_extension_unique` (`disk`,`directory`,`filename`,`extension`),
  ADD KEY `media_aggregate_type_index` (`aggregate_type`),
  ADD KEY `original_media_id` (`original_media_id`);

--
-- Indexes for table `mediables`
--
ALTER TABLE `mediables`
  ADD PRIMARY KEY (`media_id`,`mediable_type`,`mediable_id`,`tag`),
  ADD KEY `mediables_mediable_id_mediable_type_index` (`mediable_id`,`mediable_type`),
  ADD KEY `mediables_tag_index` (`tag`),
  ADD KEY `mediables_order_index` (`order`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_table_id_index` (`table_id`),
  ADD KEY `meta_table_index` (`table_type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_slug_unique` (`slug`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_index` (`user_id`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameters_categories`
--
ALTER TABLE `parameters_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parameters_categories_parameter_id_foreign` (`parameter_id`),
  ADD KEY `parameters_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `parameters_products`
--
ALTER TABLE `parameters_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parameters_products_parameter_id_foreign` (`parameter_id`),
  ADD KEY `parameters_products_product_id_foreign` (`product_id`),
  ADD KEY `parameters_products_value_index` (`value`),
  ADD KEY `parameters_products_value_ro_index` (`value_ro`),
  ADD KEY `parameters_products_value_en_index` (`value_en`),
  ADD KEY `parameters_products_value_id_index` (`value_id`);

--
-- Indexes for table `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parameter_values_parameter_id_foreign` (`parameters_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_table_id_index` (`table_id`),
  ADD KEY `photos_table_index` (`table`),
  ADD KEY `photos_token_index` (`token`),
  ADD KEY `idx_sort` (`sort`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `product_collection`
--
ALTER TABLE `product_collection`
  ADD KEY `product_collection_product_id_index` (`product_id`),
  ADD KEY `product_collection_collection_id_index` (`collection_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories_materials`
--
ALTER TABLE `categories_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories_news`
--
ALTER TABLE `categories_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories_xref`
--
ALTER TABLE `categories_xref`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `lists`
--
ALTER TABLE `lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meta`
--
ALTER TABLE `meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `parameters_categories`
--
ALTER TABLE `parameters_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `parameters_products`
--
ALTER TABLE `parameters_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=525;

--
-- AUTO_INCREMENT for table `parameters_values`
--
ALTER TABLE `parameters_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `original_media_id` FOREIGN KEY (`original_media_id`) REFERENCES `media` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `mediables`
--
ALTER TABLE `mediables`
  ADD CONSTRAINT `mediables_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parameters_categories`
--
ALTER TABLE `parameters_categories`
  ADD CONSTRAINT `parameters_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parameters_categories_parameter_id_foreign` FOREIGN KEY (`parameter_id`) REFERENCES `parameters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parameters_products`
--
ALTER TABLE `parameters_products`
  ADD CONSTRAINT `parameters_products_parameter_id_foreign` FOREIGN KEY (`parameter_id`) REFERENCES `parameters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parameters_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parameters_values`
--
ALTER TABLE `parameters_values`
  ADD CONSTRAINT `parameter_values_parameter_id_foreign` FOREIGN KEY (`parameters_id`) REFERENCES `parameters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_collection`
--
ALTER TABLE `product_collection`
  ADD CONSTRAINT `product_collection_collection_id_foreign` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_collection_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
