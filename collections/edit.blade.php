@extends('admin.body')
@section('title', 'Коллекции')

@section('centerbox')

    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/collections') }}">Коллекции</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
    </div>

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/collections', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/collections/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Наименование (ru)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name[ru]')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('name[ro]', 'Наименование (ro)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name[ro]')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('in_menu', 'Отображать в меню', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('in_menu', ['1' => 'Да', '0' => 'Нет'], (isset($data->in_menu) ? $data->in_menu : old('in_menu')), ['class' => 'col-sm-9']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('sort', 'Сортировка', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort', 0)), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>
                        Категории для набора
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $selected = (isset($data) ? $data->categories->pluck('id')->toArray() : []); ?>

                                <select name="categories[]" class='form-control chosencat' id='categories' multiple data-placeholder='Выберите категории'>
                                    @foreach ($categories as $category)
                                        @include('admin.collections.partials.list-item')
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>
                        Товары для набора
                    </h4>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php $selected = (isset($data) ? $data->products->pluck('id')->toArray() : []); ?>
                                {{ Form::select('products[]', $products, $selected, ['class' => 'form-control chosencat', 'id' => 'products', 'multiple' => 'multiple', 'data-placeholder' => 'Выберите товары']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

{{ Form::close() }}

@endsection

@section('scripts')
    @include('admin.partials.slug', ['input_name' => 'name[ru]'])
    @include('admin.partials.chosen')
@append
